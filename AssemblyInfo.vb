Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly


' TODO: Review the values of the assembly attributes


<Assembly: AssemblyTitle("BuildCda")> 
<Assembly: AssemblyDescription("Build index for .NET 4.0")> 
<Assembly: AssemblyCompany("ParcelQuest")>
<Assembly: AssemblyProduct("BuildCda.NET")> 
<Assembly: AssemblyCopyright("CD-DATA Copyright � 2004-2022")> 
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Revision
'	Build Number

' You can specify all the values or you can default the Revision and Build Numbers
' by using the '*' as shown below


<Assembly: AssemblyVersion("21.1.0.1")> 



<Assembly: AssemblyFileVersionAttribute("21.1.0.1")> 
<Assembly: ComVisibleAttribute(False)> 