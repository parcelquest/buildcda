Option Strict Off
Option Explicit On
Module Cddata

   Public Structure DPFREC
      Dim iID As Short
      Dim iType As Short
      Dim iOffSet As Short
      Dim iLength As Short
      Dim bIndexed As Short 'For CDAssr, currently not used
      Dim bSorted As Short 'For CDAssr, currently not used
      Dim iRecordArrayLength As Short 'For CDAssr, currently not used
      Dim bGroup As Short
      Dim bIndex As Short
      Dim bDisplay As Short
      Dim lTerms As Integer
      Dim lUniqueTerms As Integer
      <VBFixedString(40), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=40)> Public acTag As String
      <VBFixedString(16), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=16)> Public acTermFile As String
      <VBFixedString(16), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=16)> Public acIndexFile As String
      <VBFixedString(16), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=16)> Public acRecordFile As String
      <VBFixedString(16), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=16)> Public acWordFile As String
      <VBFixedString(40), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=40)> Public acMemberOf As String
   End Structure

   Public Structure Field
      Dim ID As Integer
      Dim Type As Integer
      Dim FieldOffSet As Integer
      Dim FieldLength As Integer
      Dim Indexed As Boolean
      Dim Sorted As Boolean
      Dim RecordArrayLength As Integer
      Dim Group As Boolean
      Dim Index As Boolean
      Dim Display As Boolean
      Dim Search As Boolean
      Dim Terms As Integer
      Dim UniqueTerms As Integer
      Dim Tag As String
      Dim TermFile As String
      Dim IndexFile As String
      Dim RecordFile As String
      Dim WordFile As String
      Dim MemberOfGroup As String
   End Structure

   Public g_aFields() As Field
   Public iMaxRecs As Integer
   Public g_Field As DPFREC
   Private iRecNum As Integer
   Private iNumFlds As Integer

   Declare Function CddNew Lib "prodlib2.dll" (ByVal lpFileName As String) As Integer
   Declare Function CddOpen Lib "prodlib2.dll" (ByVal lpFileName As String) As Integer
   Declare Function CddGetRec Lib "prodlib2.dll" (ByRef pField As DPFREC, ByVal iID As Integer, ByVal strTag As String) As Integer
   Declare Function CddPutRec Lib "prodlib2.dll" (ByRef pField As DPFREC) As Integer
   Declare Function CddRecLen Lib "prodlib2.dll" () As Integer
   Declare Function CddNumFlds Lib "prodlib2.dll" () As Integer
   Declare Sub CddClose Lib "prodlib2.dll" ()
   Declare Sub CddUpdate Lib "prodlib2.dll" ()
   Declare Function CddNewFldList Lib "prodlib2.dll" (ByVal iNumFlds As Integer, ByVal lpFileName As String) As Integer

   Public Function DpfParse() As Integer
      Dim iRet, iCnt As Integer
      Dim strTemp As String
      Dim objINI As New clsINIWrapper

      DpfParse = -1
      LogMsg("Parsing DPF file")

      Try
         iRet = CddOpen(g_sProjectFile)
      Catch ex As Exception
         LogMsg("***** Error opening: " & g_sProjectFile)
         LogMsg("ErrMsg: " & ex.Message())
         Exit Function
      End Try

      If iRet >= 0 Then
         iRecNum = 0
         Try
            iRet = CddGetRec(g_Field, iRecNum, "")
         Catch ex As Exception
            LogMsg("***** Error reading: " & g_sProjectFile & ".  Please check for comma or invalid delimiter in the RecordDef section")
            LogMsg("ErrMsg: " & ex.Message())
            Exit Function
         End Try

         If iRet >= 0 Then
            iNumFlds = CddNumFlds

            'Initialize globals
            strTemp = ""
            Call objINI.valueRead("Data", "RecordLength", strTemp, g_sProjectFile)
            If strTemp <> "" Then
               g_iRecLen = strTemp
            Else
               g_iRecLen = CddRecLen
            End If

            g_iFields = iNumFlds
            g_iDisplayFields = 0
            g_iSearchFields = 0
            g_iExtractFields = 0

            'Redefine buffer
            ReDim g_aFields(iNumFlds)

            'Fill up field array
            For iCnt = 0 To iNumFlds - 1
               iRet = CddGetRec(g_Field, iCnt, "")
               g_aFields(iCnt).IndexFile = Trim(Replace(g_Field.acIndexFile, Chr(0), Chr(32)))
               g_aFields(iCnt).RecordFile = Trim(Replace(g_Field.acRecordFile, Chr(0), Chr(32)))
               g_aFields(iCnt).TermFile = Trim(Replace(g_Field.acTermFile, Chr(0), Chr(32)))
               g_aFields(iCnt).WordFile = Trim(Replace(g_Field.acWordFile, Chr(0), Chr(32)))
               g_aFields(iCnt).Tag = Trim(Replace(g_Field.acTag, Chr(0), Chr(32)))
               g_aFields(iCnt).MemberOfGroup = Trim(Replace(g_Field.acMemberOf, Chr(0), Chr(32)))
               g_aFields(iCnt).Display = g_Field.bDisplay
               g_aFields(iCnt).Group = g_Field.bGroup
               g_aFields(iCnt).Index = g_Field.bIndex
               g_aFields(iCnt).Indexed = g_Field.bIndexed
               g_aFields(iCnt).Sorted = g_Field.bSorted
               g_aFields(iCnt).ID = g_Field.iID
               g_aFields(iCnt).FieldLength = g_Field.iLength
               g_aFields(iCnt).FieldOffSet = g_Field.iOffSet
               g_aFields(iCnt).RecordArrayLength = g_Field.iRecordArrayLength
               g_aFields(iCnt).Type = g_Field.iType
               g_aFields(iCnt).Terms = g_Field.lTerms
               g_aFields(iCnt).UniqueTerms = g_Field.lUniqueTerms

               If g_aFields(iCnt).Group = True Then
                  g_aFields(iCnt).Display = False
               ElseIf g_aFields(iCnt).Display = True Then
                  g_iDisplayFields = g_iDisplayFields + 1
               End If
               'If g_aFields(iCnt).MemberOfGroup = "" And g_aFields(iCnt).Index = True Then
               If g_aFields(iCnt).Index = True Then
                  g_iSearchFields = g_iSearchFields + 1
                  If g_aFields(iCnt).Group = False Then g_iExtractFields = g_iExtractFields + 1
               Else
                  If g_aFields(iCnt).MemberOfGroup <> "" Then g_iExtractFields = g_iExtractFields + 1
                  'System.Diagnostics.Debug.WriteLine(VB6.TabLayout(iCnt, g_aFields(iCnt).MemberOfGroup))
               End If
            Next
            'g_iExtractFields = g_iSearchFields
         End If
      Else
         'Fail to open DPF file
         LogMsg("Error reading: " & g_sProjectFile & ".  Please check for comma or invalid delimiter in the RecordDef section")
         GoTo Parse_Err
      End If

      DpfParse = 0
      GoTo Parse_Exit

Parse_Err:
      DpfParse = -1
      'MsgBox Err.Description
Parse_Exit:
      objINI = Nothing
   End Function

   Public Function DpfUpdate(ByRef iRec As Integer) As Integer
      Dim iRet As Integer
      Dim sTemp As String

      On Error GoTo Update_Error
      g_Field.iID = g_aFields(iRec).ID
      g_Field.iType = g_aFields(iRec).Type
      g_Field.acTag = g_aFields(iRec).Tag
      g_Field.iOffSet = g_aFields(iRec).FieldOffSet
      g_Field.iLength = g_aFields(iRec).FieldLength
      g_Field.bIndex = g_aFields(iRec).Index
      g_Field.bDisplay = g_aFields(iRec).Display
      g_Field.bGroup = g_aFields(iRec).Group
      g_Field.bIndexed = g_aFields(iRec).Indexed
      g_Field.bSorted = g_aFields(iRec).Sorted
      g_Field.lTerms = g_aFields(iRec).Terms
      g_Field.lUniqueTerms = g_aFields(iRec).UniqueTerms
      g_Field.iRecordArrayLength = g_aFields(iRec).RecordArrayLength

      g_Field.acTermFile = g_aFields(iRec).TermFile
      If Left(g_Field.acTermFile, 1) <= " " Then
         sTemp = g_sPrefix & g_Field.iID
         g_Field.acTermFile = sTemp & ".TRM"
         g_Field.acIndexFile = sTemp & ".IND"
         g_Field.acRecordFile = sTemp & ".RCD"
      Else
         g_Field.acIndexFile = g_aFields(iRec).IndexFile
         g_Field.acRecordFile = g_aFields(iRec).RecordFile
      End If
      g_Field.acMemberOf = g_aFields(iRec).MemberOfGroup

      DpfUpdate = CddPutRec(g_Field)
      Exit Function
Update_Error:
      DpfUpdate = 1
      MsgBox("DpfUpdate() " & Err.Description)
   End Function
End Module