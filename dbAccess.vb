Option Strict Off
Option Explicit On
Module dbAccess

   Public Structure CountyInfo
      Dim CountyCode As String
      Dim CountyName As String
      Dim ProductCode As String
      Dim Prefix As String
      Dim Autosend As String 'Copy zip file to FTP folder
      Dim NumFiles As Integer
      Dim LastRecsCount As Integer
   End Structure

   Public dbConn As ADODB.Connection
   Public dbRs As ADODB.Recordset

   Public Function openDB(ByRef dbProvider As String) As Boolean
      Dim strError As String = ""

      If dbConn Is Nothing Then
         dbConn = New ADODB.Connection
      End If

      openDB = True
      Try
         If dbConn.State <> ADODB.ObjectStateEnum.adStateOpen Then
            dbConn.ConnectionString = "Provider=" & dbProvider
            dbConn.Open()
         End If

      Catch ex As Exception
         LogMsg("***** Error connecting to DB: " & ex.Message())
         dbConn = Nothing
         openDB = False
      End Try
   End Function

   Public Sub closeDB()
      If dbConn.State = ADODB.ObjectStateEnum.adStateOpen Then
         dbConn.Close()
      End If

      dbConn = Nothing
   End Sub

   'Set Products!State="B"
   Public Sub updProductState(ByRef sPrefix As String, ByRef sFlag As String)
      Dim sql As String
      Dim strError As String = ""

      If Not openDB(g_sProvider) Then
         LogMsg("Unable to connect to SQL server " & g_sProvider)
         Exit Sub
      End If

      Try
         sql = "SELECT * FROM Products WHERE (ProdCode='CDA') AND (Prefix='" & sPrefix & "')"
         dbRs = New ADODB.Recordset
         dbRs.CursorType = ADODB.CursorTypeEnum.adOpenKeyset
         dbRs.LockType = ADODB.LockTypeEnum.adLockOptimistic
         dbRs.Open(sql, dbConn, , , ADODB.CommandTypeEnum.adCmdText)

         'Loop through record set and process everyone
         Do While Not dbRs.EOF
            dbRs.Fields("State").Value = sFlag
            If dbRs.Fields("AutoExtract").Value = "E" Then
               dbRs.Fields("AutoExtract").Value = "Y"
            End If
            dbRs.MoveNext()
         Loop
         dbRs.UpdateBatch()
      Catch ex As Exception
         LogMsg("***** Error updating Products table flags: " & ex.Message())
         'Close recordset
         If dbRs.State = ADODB.ObjectStateEnum.adStateOpen Then
            dbRs.Close()
         End If
         dbRs = Nothing
         Call closeDB()
      End Try
   End Sub

   'Return >=0 if OK, <0 if error
   Public Function getCDA(ByRef atCntyInfo() As CountyInfo) As Integer
      Dim iRet As Integer
      Dim sql As String
      Dim strError As String = ""

      getCDA = -1
      If Not openDB(g_sProvider) Then
         LogMsg("Unable to connect to SQL server " & g_sProvider)
         Exit Function
      End If

      If g_sPrefix = "" Then
         sql = "SELECT prefix, products.countycode, county.countyname, products.numfiles,lastrecscount, autosend " & _
               "FROM Products INNER JOIN County on county.countycode=products.countycode " & _
               "WHERE (State='P') AND (ProdCode='CDA') AND (Doit='Y') ORDER BY prefix"
      Else
         sql = "SELECT prefix, products.countycode, county.countyname, products.numfiles,lastrecscount, autosend " & "FROM Products INNER JOIN County on county.countycode=products.countycode " & "WHERE (ProdCode='CDA') AND (Prefix='" & g_sPrefix & "')"
      End If

      Try
         dbRs = New ADODB.Recordset
         dbRs.CursorType = ADODB.CursorTypeEnum.adOpenKeyset
         dbRs.LockType = ADODB.LockTypeEnum.adLockOptimistic
         dbRs.Open(sql, dbConn, , , ADODB.CommandTypeEnum.adCmdText)
      Catch ex As Exception
         LogMsg("***** Error open record set in GetCDA: " & ex.Message())
         Exit Function
      End Try

      'Loop through record set and process everyone
      iRet = 0
      Try
         Do While Not dbRs.EOF
            atCntyInfo(iRet).Prefix = dbRs.Fields("Prefix").Value.ToString()
            atCntyInfo(iRet).CountyCode = dbRs.Fields("CountyCode").Value.ToString()
            atCntyInfo(iRet).CountyName = dbRs.Fields("CountyName").Value.ToString()
            atCntyInfo(iRet).NumFiles = dbRs.Fields("NumFiles").Value.ToString()
            atCntyInfo(iRet).LastRecsCount = dbRs.Fields("LastRecsCount").Value.ToString()
            atCntyInfo(iRet).Autosend = dbRs.Fields("Autosend").Value.ToString()
            iRet = iRet + 1
            dbRs.MoveNext()
         Loop
         atCntyInfo(iRet).Prefix = ""
      Catch ex As Exception
         LogMsg("***** Error populating county info in GetCDA: " & ex.Message())
         iRet = -1
      End Try
      getCDA = iRet

      'Close recordset
      If dbRs.State = ADODB.ObjectStateEnum.adStateOpen Then
         dbRs.Close()
      End If
      dbRs = Nothing
      Call closeDB()
   End Function
End Module