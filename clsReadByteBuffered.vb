Option Strict Off
Option Explicit On
Friend Class clsReadByteBuffered

   Dim m_lCurrentPos As Integer 'current position in the buffer - ZERO BASED!!
   Dim ByteBuffer() As Byte
   Dim m_zHndl As Integer

   Const m_zDATA_LENGTH As Integer = 1
   Dim m_zReturnArrayLength As Integer
   Dim m_aReturnArray() As Byte

   Public BufferSize As Integer
   Public lLogicalSeek As Integer
   Public lLOF As Integer

   Public Sub GotoPos(ByRef lPos As Integer)

      ReDim ByteBuffer(BufferSize - 1)
      m_lCurrentPos = BufferSize

      Seek(m_zHndl, lPos)
      Call FillBuffer()


   End Sub

   '   Public Function ArrayGet() As Object

   '      On Error GoTo lblErr

   '      Dim zNewPos As Integer
   '      Dim iX As Integer

   '      zNewPos = m_lCurrentPos + m_zReturnArrayLength

   '      If zNewPos > UBound(ByteBuffer) + 1 Then

   '         If Not FillBuffer Then
   '            ArrayGet = System.DBNull.Value
   '            Exit Function
   '         End If

   '         zNewPos = m_lCurrentPos + m_zReturnArrayLength

   '      End If

   '      If zNewPos <= UBound(ByteBuffer) + 1 Then 'not minus one as the sign is reversed ( equivalent to <=)

   '         For iX = 0 To m_zReturnArrayLength - 1
   '            m_aReturnArray(iX) = ByteBuffer(m_lCurrentPos + iX)
   '         Next

   '         m_lCurrentPos = zNewPos
   '         lLogicalSeek = lLogicalSeek + ((m_zReturnArrayLength) * m_zDATA_LENGTH)
   '         ArrayGet = VB6.CopyArray(m_aReturnArray)
   '      Else
   '         ArrayGet = System.DBNull.Value
   '      End If

   '      Exit Function

   'lblErr:
   '      LogMsg("clsReadByteBuffered!ArrayGet: " & Err.Description)
   '      'MsgBox Err.Description, , "StringGet"
   '   End Function

   Public Function ByteGet(ByRef aLongArray() As Byte) As Boolean

      On Error GoTo lblErr

      Dim zNewPos As Integer
      Dim iX As Integer

      zNewPos = m_lCurrentPos + m_zReturnArrayLength

      If zNewPos > UBound(ByteBuffer) + 1 Then

         If Not FillBuffer Then
            ByteGet = False
            Exit Function
         End If

         zNewPos = m_lCurrentPos + m_zReturnArrayLength

      End If

      If zNewPos <= UBound(ByteBuffer) + 1 Then 'not minus one as the sign is reversed ( equivalent to <=)

         For iX = 0 To m_zReturnArrayLength - 1
            aLongArray(iX) = ByteBuffer(m_lCurrentPos + iX)
         Next

         m_lCurrentPos = zNewPos
         lLogicalSeek = lLogicalSeek + ((m_zReturnArrayLength) * m_zDATA_LENGTH)
         ByteGet = True
      Else
         ByteGet = False
      End If

      Exit Function

lblErr:
      LogMsg("clsReadByteBuffered!ByteGet: " & Err.Description)
      '   MsgBox Err.Description, , "StringGet"
   End Function

   Private Function FillBuffer() As Boolean

      On Error GoTo lblErr

      Dim lCurrentSeek As Integer
      Dim lSeekOffset As Integer
      Dim zNewDim As Integer

      'get the number of characters not yet accessed in the buffer
      lSeekOffset = ((UBound(ByteBuffer) + 1) - m_lCurrentPos) * m_zDATA_LENGTH
      'set the corrected position in the file
      lCurrentSeek = Seek(m_zHndl) - lSeekOffset
      'if this position is not passed the end of the file.....
      If lCurrentSeek <= lLOF Then

         If lCurrentSeek - 1 + ((UBound(ByteBuffer) + 1) * m_zDATA_LENGTH) > lLOF Then
            zNewDim = (CInt(lLOF - (lCurrentSeek - 1)) / 4) - 1
            If zNewDim >= 0 Then
               ReDim ByteBuffer(zNewDim)
            Else
               FillBuffer = False
            End If
         End If

         If UBound(ByteBuffer) >= 0 Then

            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
            FileGet(m_zHndl, ByteBuffer, lCurrentSeek)
            lLogicalSeek = lCurrentSeek
            m_lCurrentPos = 0
            FillBuffer = True

         Else
            FillBuffer = False
         End If

      Else
         FillBuffer = False
      End If

      Exit Function

lblErr:
      LogMsg("clsReadByteBuffered!FillBuffer: " & Err.Description)
      '    MsgBox Err.Description, , "FillBuffer"
   End Function

   Public WriteOnly Property ReturnArrayLength() As Integer
      Set(ByVal Value As Integer)
         m_zReturnArrayLength = Value

      End Set
   End Property

   Public Sub OpenFile(ByRef szFileName As String)
      m_zHndl = FreeFile()

      Try
         FileOpen(m_zHndl, szFileName, OpenMode.Binary)
      Catch ex As Exception
         LogMsg("clsReadByteBuffered!OpenFile: " & ex.Message())

      End Try

      ReDim ByteBuffer(BufferSize - 1)
      m_lCurrentPos = BufferSize
      lLOF = LOF(m_zHndl)

   End Sub

   Private Sub Class_Initialize_Renamed()
      BufferSize = IN_ARRAY_BUFFER_SIZE
   End Sub

   Public Sub New()
      MyBase.New()
      Class_Initialize_Renamed()
   End Sub

   Public Sub CloseFile()
      FileClose(m_zHndl)
   End Sub

   Protected Overrides Sub Finalize()
      CloseFile()
      MyBase.Finalize()
   End Sub

   Private Function GetChunk() As Boolean

      On Error GoTo lblErr

      Dim lCurrentSeek As Integer
      Dim lSeekOffset As Integer
      Dim zNewDim As Integer

      'get the number of characters not yet accessed in the buffer
      lSeekOffset = ((UBound(ByteBuffer) + 1) - m_lCurrentPos) * m_zDATA_LENGTH
      'set the corrected position in the file
      lCurrentSeek = Seek(m_zHndl) - lSeekOffset
      'if this position is not passed the end of the file.....
      If lCurrentSeek <= lLOF Then
         If lCurrentSeek - 1 + ((UBound(ByteBuffer) + 1) * m_zDATA_LENGTH) > lLOF Then
            zNewDim = (CInt(lLOF - (lCurrentSeek - 1))) - 1
            If zNewDim >= 0 Then
               ReDim ByteBuffer(zNewDim)
            Else
               GetChunk = False
            End If
         End If

         If UBound(ByteBuffer) >= 0 Then
            FileGet(m_zHndl, ByteBuffer, lCurrentSeek)
            lLogicalSeek = lCurrentSeek
            m_lCurrentPos = UBound(ByteBuffer) + 1
            GetChunk = True
         Else
            GetChunk = False
         End If
      Else
         GetChunk = False
      End If

      Exit Function

lblErr:
      LogMsg("clsReadByteBuffered!GetChunk: " & Err.Description)
      '    MsgBox Err.Description, , "FillBuffer"
   End Function

   Public Sub appendFiles(ByRef sFilename1 As String, ByRef sFilename2 As String)

      On Error GoTo lblErr

      Dim zHndlGroup As Integer

      zHndlGroup = FreeFile

      FileOpen(zHndlGroup, sFilename1, OpenMode.Binary)
      If LOF(zHndlGroup) > 1 Then
         Seek(zHndlGroup, LOF(zHndlGroup) + 1)
      End If

      OpenFile(sFilename2)

      Do Until GetChunk = False
         FilePut(zHndlGroup, ByteBuffer)
      Loop
      FileClose(zHndlGroup)
      Exit Sub

lblErr:
      LogMsg("clsReadByteBuffered!appendFile: " & Err.Description)
      'MsgBox Err.Description, , "clsAppend : FillBuffer"
      FileClose(zHndlGroup)
   End Sub
End Class