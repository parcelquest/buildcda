Option Strict Off
Option Explicit On
Friend Class clsWriteLong
   Dim m_aBuffer() As Integer
   Dim m_zHndl As Integer
   Dim m_zCurrentPos As Integer 'current position in the buffer - ZERO BASED!!
   Const m_zDATA_LENGTH As Integer = 4
   Public lLogicalSeek As Integer
   Public ArrayBufferSize As Integer

   Public Sub variantArrayWrite(ByRef aNew As Object)
      On Error GoTo lblErr1

      Dim iX As Integer
      Dim zUbound As Integer
      Dim zNextPos As Integer


      zUbound = UBound(aNew)
      zNextPos = m_zCurrentPos + zUbound + 1

      If zNextPos > ArrayBufferSize Then
         ReDim Preserve m_aBuffer(m_zCurrentPos - 1)

         FilePut(m_zHndl, m_aBuffer)
         ReDim m_aBuffer(ArrayBufferSize - 1)
         m_zCurrentPos = 0
      End If

      For iX = 0 To zUbound
         'UPGRADE_WARNING: Couldn't resolve default property of object aNew(). Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
         m_aBuffer(m_zCurrentPos + iX) = aNew(iX)
      Next

      m_zCurrentPos = m_zCurrentPos + zUbound + 1
      Exit Sub

lblErr1:
      LogMsg("clsWriteLong!variantArrayWrite: " & Err.Description)
      'MsgBox Err.Description, vbExclamation, "clsWriteLong : variantArrayWrite"
   End Sub

   Public Sub arrayWrite(ByRef aNew() As Integer)
      On Error GoTo lblErr2

      Dim iX As Integer
      Dim zUbound As Integer
      Dim zNextPos As Integer

      zUbound = UBound(aNew)
      zNextPos = m_zCurrentPos + zUbound + 1

      If zNextPos > ArrayBufferSize Then
         ReDim Preserve m_aBuffer(m_zCurrentPos - 1)

         FilePut(m_zHndl, m_aBuffer)
         ReDim m_aBuffer(ArrayBufferSize - 1)
         m_zCurrentPos = 0
      End If

      For iX = 0 To zUbound
         m_aBuffer(m_zCurrentPos + iX) = aNew(iX)
      Next

      m_zCurrentPos = m_zCurrentPos + zUbound + 1
      lLogicalSeek = lLogicalSeek + (m_zDATA_LENGTH * (zUbound + 1))

      Exit Sub

lblErr2:
      LogMsg("clsWriteLong!arrayWrite: " & Err.Description)
      'MsgBox Err.Description, vbExclamation, "clsWriteLong : arrayWrite"
   End Sub
   'Public Function termWrite(sTerm As String, lRecordNumber As Long) As Boolean
   '   On Error GoTo lblErr3
   '
   '   Dim iX As Integer
   '   Dim zNextPos As Integer
   '
   '   zNextPos = m_zCurrentPos + 2
   '
   '   If zNextPos > ArrayBufferSize Then
   '      ReDim Preserve m_aBuffer(m_zCurrentPos - 1)
   '      Put #m_zHndl, , m_aBuffer
   '      ReDim m_aBuffer(ArrayBufferSize - 1)
   '      m_zCurrentPos = 0
   '   End If
   '
   '   m_aBuffer(m_zCurrentPos) = CLng(sTerm)
   '   m_aBuffer(m_zCurrentPos + 1) = lRecordNumber
   '   m_zCurrentPos = m_zCurrentPos + 2
   '   termWrite = True
   '   Exit Function
   '
   'lblErr3:
   '   MsgBox Err.Description, vbExclamation, "clsWriteLong : termWrite"
   '   termWrite = False
   'End Function

   Public Sub longWrite(ByRef lTerm As Integer)
      On Error GoTo lblErr4

      If m_zCurrentPos + 1 > ArrayBufferSize Then

         FilePut(m_zHndl, m_aBuffer)
         m_zCurrentPos = 0
      End If

      m_aBuffer(m_zCurrentPos) = lTerm
      m_zCurrentPos = m_zCurrentPos + 1
      lLogicalSeek = lLogicalSeek + m_zDATA_LENGTH
      Exit Sub

lblErr4:
      LogMsg("clsWriteLong!longWrite: " & Err.Description)
      'MsgBox Err.Description, vbExclamation, "clsWriteLong : longWrite"
   End Sub

   Public Function OpenFile(ByRef szFileName As String) As Boolean
      On Error GoTo lblErr5


      If Dir(szFileName) <> "" Then
         Kill((szFileName))
      End If

      'open the file #4
      m_zHndl = FreeFile()
      FileOpen(m_zHndl, szFileName, OpenMode.Binary)

      'empty the buffer
      ReDim m_aBuffer(ArrayBufferSize - 1)
      m_zCurrentPos = 0

      lLogicalSeek = Seek(m_zHndl)

      OpenFile = True
      Exit Function

lblErr5:
      LogMsg("clsWriteLong!OpenFile: " & Err.Description)
      '   MsgBox Err.Description, vbExclamation, "clsWriteLong : OpenFile"
   End Function

   'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Private Sub Class_Initialize_Renamed()
      ArrayBufferSize = OUT_BUFFER_SIZE_LONG_EXTRACTION
   End Sub

   'Public Sub Dispose() Implements IDisposable.Dispose
   '   CloseFile()
   '   GC.SuppressFinalize(Me)
   'End Sub

   Public Sub New()
      MyBase.New()
      Class_Initialize_Renamed()
   End Sub

   Public Sub CloseFile()
      Try
         If m_zCurrentPos > 0 Then
            'Resize output buffer then write to output file
            ReDim Preserve m_aBuffer(m_zCurrentPos - 1)
            FilePut(m_zHndl, m_aBuffer)
            m_zCurrentPos = 0
         End If
         FileClose(m_zHndl)
      Catch ex As Exception
         'Ignore this error
      End Try
   End Sub

   Protected Overrides Sub Finalize()
      CloseFile()
      MyBase.Finalize()
   End Sub

   Public Function termCheck(ByRef sTerm As String) As Boolean
      If IsNumeric(sTerm) Then
         termCheck = True
      End If
   End Function
End Class