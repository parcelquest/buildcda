Option Strict Off
Option Explicit On
Imports System.IO

Friend Class clsRecordExtraction
   Public lRecords As Integer
   Public lBadRecords As Integer

   Dim lApproxRecords As Integer
   Dim lInterval As Integer

   'Extract data for one field - output to TRM file
   Public Sub ExtractField(ByRef iX As Integer)
      On Error GoTo lblErr

      Dim zUbound As Integer
      Dim objFieldExt As New clsFieldExtraction
      Dim ObjWriter As New clsWriteBuffer
      Dim sField As String
      Dim bFinished As Boolean
      Dim bGoodData As Boolean
      Dim lRecordsDone As Integer
      Dim sFileName As String
      Dim lLastRecordPos As Integer
      Dim iCurFile As Integer

      sFileName = UTFileName(TERM, iX)
      g_aFields(iX).TermFile = sFileName
      Call ObjWriter.OpenFile(g_sWorkDir & sFileName, g_aFields(iX).Type, iX)
      iCurFile = 1
      lApproxRecords = 0
      lRecords = 0

      'Save raw file name
      sFileName = g_sRawFile

ReExtractField:
      objFieldExt.OpenFile(g_sRawFile)
      lApproxRecords = lApproxRecords + objFieldExt.lEOF / g_iRecLen
      lLastRecordPos = objFieldExt.lLogicalSeek

      Do Until bFinished
         sField = objFieldExt.fieldGetFixed(g_aFields(iX).FieldOffSet, g_aFields(iX).FieldLength)
         bGoodData = termCheck(sField, g_aFields(iX).Type)
         lRecords = lRecords + 1
         If Not bGoodData Then
            lBadRecords = lBadRecords + 1
            LogMsg("*** Bad record(" & lBadRecords & "): " & lRecords & " = " & sField)
         End If

         If bGoodData Then
            Call ObjWriter.termWrite(sField, lRecords, iX)
         End If

         'If lRecords Mod 100 = 0 Then
         '   setProgress((Cinteger(lRecords * 100 / lApproxRecords)))
         '   setStatus("Extracting Record :  " & lRecords)
         'End If

         bFinished = Not objFieldExt.nextRecord
         If bFinished Then
            objFieldExt.CloseFile()
            If g_iRawFile > iCurFile Then
               bFinished = False
               iCurFile = iCurFile + 1
               g_sRawFile = Left(g_sRawFile, Len(g_sRawFile) - 2) & Format(iCurFile, "00")
               GoTo ReExtractField
            End If
         End If

         lRecordsDone = lRecordsDone + 1
         lLastRecordPos = objFieldExt.lLogicalSeek
      Loop

      'Restore raw filename
      g_sRawFile = sFileName
      ObjWriter.CloseFile()

      Exit Sub

lblErr:
      LogMsg("clsRecordExtraction!ExtractField: " & Err.Description)
      '   MsgBox Err.Description, vbExclamation, "clsRecrdExt : ExtractField"
   End Sub

   'CHeck to make sure data is in valid type
   Public Function termCheck(ByRef sTerm As String, ByRef zType As Integer) As Boolean
      termCheck = True
      Select Case zType
         Case iFIELD_TYPE_LONG, iFIELD_TYPE_LJNUM
            If Trim(sTerm) <> "" Then
               Try
                  Dim vTemp As Long
                  vTemp = CLng(sTerm)
               Catch ex As Exception
                  'Error
                  termCheck = False
               End Try
            End If

         Case iFIELD_TYPE_FLOAT
            If Trim(sTerm) <> "" Then
               Try
                  Dim vTemp As Single
                  vTemp = CSng(sTerm)
               Catch ex As Exception
                  'Error
                  termCheck = False
               End Try
            End If
      End Select

   End Function

   Public Function recordsExtract() As Integer
      Dim objFieldExt As New clsFieldExtraction
      Dim ObjWriter As New clsWriteBuffer
      Dim sFileName As String
      Dim iX, iNumFiles, iRet As Integer

      'Setup progress window
      g_bStopProcess = False
      iRet = -1

      'Create filename and remove existing file for extract fields
      For iX = 0 To g_iFields - 1
         sFileName = UTFileName(TERM, iX)
         If Dir(g_sWorkDir & sFileName) <> "" Then
            Kill((g_sWorkDir & sFileName))
         End If
         g_aFields(iX).TermFile = sFileName
         g_aFields(iX).Terms = 0
      Next

      'VB can only open maximum 512 files at a time, we want to limit it down do
      '500 and leave room for other things. So we extract 500 fields at a time
      iNumFiles = g_iExtractFields
      iX = 0
      Do While iNumFiles > 0
         Try
            If iNumFiles > 500 Then
               iRet = recordsExtract1(iX, 500)
            Else
               iRet = recordsExtract1(iX, iNumFiles)
            End If
         Catch ex As Exception
            LogMsg("**** clsRecordExtraction!recordsExtract: " & ex.Message)
            iRet = 0
         End Try

         If iRet <> 0 Then Exit Do

         iNumFiles = iNumFiles - 500
         iX = iX + 500
      Loop

      If iRet <> 0 Then GoTo lblExitRE

      iRet = CombineFields()
lblExitRE:
      recordsExtract = iRet
   End Function

   Public Function recordsExtract1(ByVal iStart As Integer, ByVal iNumFile As Integer) As Integer
      Dim objFieldExt As New clsFieldExtraction
      Dim ObjWriter As New clsWriteBuffer
      Dim iX As Integer
      Dim iY As Integer
      Dim aFields() As String
      Dim bFinished As Boolean
      Dim bGoodData As Boolean
      Dim lRecordsDone As Integer
      Dim sFileName As String
      Dim lLastRecordPos As Integer
      Dim iCurFile As Integer
      Dim iRet As Integer

      iRet = -1
      ReDim aFields(g_iExtractFields)

      LogMsg("Extract raw file to create TRM files")
      For iX = 0 To iNumFile - 1
         'Open individual extract field file
         iY = iX + iStart
         Call ObjWriter.OpenFile(g_sWorkDir & g_aFields(g_aExtractFields(iY)).TermFile, g_aFields(g_aExtractFields(iY)).Type, iX)
      Next

      iCurFile = 1

      'Save original Raw filename
      sFileName = g_sRawFile

ReExtractData:
      'Open RAW file
      LogMsg("Opening raw file: " & g_sRawFile)
      objFieldExt.OpenFile(g_sRawFile)
      lApproxRecords = lApproxRecords + (objFieldExt.lEOF / g_iRecLen)
      LogMsg("Expected number of records: " & lApproxRecords)
      lLastRecordPos = objFieldExt.lLogicalSeek

      Do Until bFinished
         lRecords = lRecords + 1
         For iX = 0 To iNumFile - 1
            iY = iX + iStart
            aFields(iX) = objFieldExt.fieldGetFixed(g_aFields(g_aExtractFields(iY)).FieldOffSet, g_aFields(g_aExtractFields(iY)).FieldLength)
            bGoodData = termCheck(aFields(iX), g_aFields(g_aExtractFields(iY)).Type)
            If Not bGoodData Then
               lBadRecords = lBadRecords + 1
               LogMsg("Bad record(" & lBadRecords & "): " & lRecords & " at field " & iX & " " & g_aFields(g_aExtractFields(iY)).Tag & " = " & aFields(iX))
               aFields(iX) = ""
            End If
         Next

         For iX = 0 To iNumFile - 1
            'Output data and record number to .TRM files
            Try
               Call ObjWriter.termWrite(aFields(iX), lRecords, iX)
            Catch ex As Exception
               LogMsg("***** Error in termWrite: " & ex.Message & vbCrLf & " at field " & iX & " Value=" & aFields(iX))
            End Try
         Next

         System.Windows.Forms.Application.DoEvents()
         If g_bStopProcess Then
            Exit Do
         End If

         bFinished = Not objFieldExt.nextRecord
         'If EOF then close input file
         If bFinished Then
            objFieldExt.closeFile()
            'If more input files available, setup filename for next one
            If g_iRawFile > iCurFile Then
               bFinished = False
               iCurFile = iCurFile + 1
               g_sRawFile = Left(g_sRawFile, Len(g_sRawFile) - 2) & Format(iCurFile, "00")
               GoTo ReExtractData
            End If
         End If
         lRecordsDone = lRecordsDone + 1
         lLastRecordPos = objFieldExt.lLogicalSeek
      Loop
      g_lRecords = lRecords

      ObjWriter.CloseFile()
      ObjWriter = Nothing
      objFieldExt = Nothing

      iRet = 0
      LogMsg("Extract data complete!")

      g_sRawFile = sFileName
      recordsExtract1 = iRet
   End Function

   'Combine TRM files for group fields
   Public Function CombineFields() As Integer
      Dim iInner As Integer
      Dim iOuter As Integer
      Dim iRet As Integer
      Dim objByteRead As New clsReadByteBuffered

      On Error GoTo lblErr
      iRet = -1

      For iInner = 0 To g_iFields - 1
         If g_aFields(iInner).Group = True Then
            For iOuter = 0 To g_iFields - 1
               If g_aFields(iOuter).MemberOfGroup = g_aFields(iInner).Tag Then
                  Call objByteRead.appendFiles(g_sWorkDir & g_aFields(iInner).TermFile, g_sWorkDir & g_aFields(iOuter).TermFile)
                  objByteRead = Nothing
                  objByteRead = New clsReadByteBuffered
               End If
            Next
         End If
      Next
      iRet = 0

      GoTo CombineFields_Exit
lblErr:
      LogMsg("***** clsRecordExtraction!CombineFields: " & Err.Description)

CombineFields_Exit:
      CombineFields = iRet
   End Function

   Public Sub ReCombineFields(ByRef iX As Integer)
      On Error GoTo lblErr
      Dim iNdx As Integer
      Dim objByteRead As New clsReadByteBuffered

      If g_aFields(iX).Group = True Then
         If g_aFields(iX).TermFile = "" Then
            g_aFields(iX).TermFile = UTFileName(TERM, iX)
         End If
         If File.Exists(g_sWorkDir & g_aFields(iX).TermFile) Then
            Kill((g_sWorkDir & g_aFields(iX).TermFile))
         End If

         For iNdx = 0 To g_iFields - 1
            If g_aFields(iNdx).MemberOfGroup = g_aFields(iX).Tag Then

               Call objByteRead.appendFiles(g_sWorkDir & g_aFields(iX).TermFile, g_sWorkDir & g_aFields(iNdx).TermFile)
               objByteRead = Nothing
               objByteRead = New clsReadByteBuffered
            End If
         Next
      End If

      Exit Sub
lblErr:
      LogMsg("clsRecordExtraction!ReCombineFields: " & Err.Description)
      '   MsgBox Err.Description, vbExclamation, "clsRecrdExt : ReCombineFields"
   End Sub

   Public Sub New()
      MyBase.New()
      lInterval = getInterval()
   End Sub

   Public Sub ExtractGroup(ByRef iX As Integer)
      On Error GoTo lblErr
      Dim iInner As Integer
      Dim iNdx As Integer
      Dim objByteRead As New clsReadByteBuffered

      For iNdx = 0 To g_iFields - 1
         If g_aFields(iNdx).MemberOfGroup = g_aFields(iX).Tag Then
            Call ExtractField(iNdx)
            Call objByteRead.appendFiles(g_sWorkDir & g_aFields(iX).TermFile, g_sWorkDir & g_aFields(iNdx).TermFile)
            objByteRead = Nothing
            objByteRead = New clsReadByteBuffered
         End If
      Next
      objByteRead = Nothing
      Exit Sub
lblErr:
      'MsgBox Err.Description, vbExclamation, "clsRecrdExt : ExtractGroup"
      LogMsg("clsRecordExtraction!ExtractGroup: " & Err.Description)
   End Sub
End Class