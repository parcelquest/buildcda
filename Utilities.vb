Option Strict Off
Option Explicit On
Option Compare Binary
Imports System.IO
Module Utilities

   Public m_zLastFreeFile As Integer

   Public Sub relink(ByRef sInFilename As String, ByRef sOutfilename As String)

      On Error GoTo lblErr

      Dim objReadOps As New clsReadBuffered
      Dim zOutHndl As Integer
      Dim sInTemp As String
      Dim lindex As Integer
      Dim lxCount As Integer
      Dim lFilePos As Integer

      'UPGRADE_WARNING: Lower bound of array arrIndex was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1033"'
      ReDim arrIndex(g_lRecords)

      objReadOps.OpenFile(sInFilename)

      zOutHndl = FreeFile()
      If Dir(sOutfilename) <> "" Then
         Kill((sOutfilename))
      End If

      FileOpen(zOutHndl, sOutfilename, OpenMode.Binary)

      lFilePos = objReadOps.lLogicalSeek
      sInTemp = objReadOps.stringGet

      Do Until sInTemp = ""
         lxCount = lxCount + 1
         lindex = CInt(UTSubStringTail(sInTemp))
         arrIndex(lindex) = lFilePos
         lFilePos = objReadOps.lLogicalSeek
         sInTemp = objReadOps.stringGet
      Loop

      FilePut(zOutHndl, arrIndex, 1)
      FileClose(zOutHndl)
      Exit Sub

lblErr:
      FileClose(zOutHndl)
   End Sub

   Private Function numericExtension(ByRef zNumber As Integer) As String
      Dim sTemp As String
      sTemp = CStr(zNumber)
      numericExtension = New String("0", 3 - Len(sTemp)) & sTemp
   End Function

   Public Function UTFileName(ByRef sType As String, ByRef zNumber As Integer) As String
      UTFileName = g_sPrefix & numericExtension(zNumber) & "." & sType
   End Function

   Public Function GetMinorIndexCount(ByRef szString As String) As Integer
      On Error GoTo lblErr

      Dim zPos As Integer
      Dim zPos2 As Integer

      zPos = InStr(szString, sINDEX_DELIMITER)
      zPos2 = InStr(zPos + 1, szString, sINDEX_DELIMITER)

      GetMinorIndexCount = CInt(Mid(szString, zPos + 1, zPos2 - (zPos + 1)))
      Exit Function
lblErr:
      MsgBox("GetMinorIndexCount : " & Err.Description)

   End Function

   Public Function GetMinorIndexes(ByRef szString As String) As Integer

      On Error GoTo lblErr

      Dim zPos As Integer

      zPos = InStr(szString, sINDEX_DELIMITER)
      zPos = InStr(zPos + 1, szString, sINDEX_DELIMITER)

      GetMinorIndexes = CInt(Mid(szString, zPos + 1, Len(szString) - zPos))
      Exit Function
lblErr:
      MsgBox("GetMinorIndexes : " & Err.Description)

   End Function

   Public Function GetGetMinorString(ByRef szString As String) As String
      On Error GoTo lblErr

      Dim zPos As Integer

      zPos = InStr(szString, sINDEX_DELIMITER)

      GetGetMinorString = Mid(szString, 1, zPos)
      Exit Function

lblErr:
      MsgBox("GetgetMinorString : " & Err.Description)
   End Function

   Public Function UTSubStringHead(ByRef szString As String) As String
      On Error GoTo lblUTSSH

      Dim zPos As Integer

      UTSubStringHead = ""
      If szString <> "" Then
         zPos = InStr(szString, sINDEX_DELIMITER)
         If zPos > 1 Then UTSubStringHead = Mid(szString, 1, zPos - 1)
      End If

      Exit Function
lblUTSSH:
      MsgBox("UTSubStringHead : " & Err.Description)
   End Function

   Public Function UTHeadTail(ByRef szString As String, ByRef sHead As String, ByRef lTail As Integer) As Boolean
      Dim zPos As Integer
      Dim sTmp As String

      UTHeadTail = False
      If szString = "" Then
         Exit Function
      End If

      zPos = InStr(szString, sINDEX_DELIMITER)
      If zPos = 0 Then
         Exit Function
      End If

      Try
         sHead = Mid(szString, 1, zPos - 1)
         sTmp = Mid(szString, zPos + 1, Len(szString))
         lTail = Val(sTmp)
         UTHeadTail = True
      Catch ex As Exception
         LogMsg("UTHeadTail: " & ex.Message())
      End Try
    End Function

   Public Function UTSubStringTail(ByRef szString As String) As String
      Dim zPos As Integer

      zPos = InStr(szString, sINDEX_DELIMITER)
      UTSubStringTail = Mid(szString, zPos + 1, Len(szString))
   End Function

   Public Function GetPartMinorString(ByRef szString As String, ByRef zLength As Integer) As String
      Dim zPos As Integer

      zPos = InStr(szString, sINDEX_DELIMITER)
      If zLength > zPos Then zLength = zPos
      GetPartMinorString = Mid(szString, 1, zLength)
   End Function

   Public Function UTCheckFileExists(ByRef sFileName As String) As Boolean

      UTCheckFileExists = False
      Try
         If File.Exists(sFileName) Then
            UTCheckFileExists = True
         End If
      Catch ex As Exception
         'Ignore error
      End Try
   End Function


   Public Function UTCheckDirExists(ByRef sFileName As String) As Boolean
      On Error GoTo lblErr

      If Dir(sFileName, FileAttribute.Directory) <> "" And sFileName <> "" Then UTCheckDirExists = True
      Exit Function

lblErr:

      UTCheckDirExists = False
   End Function

   Public Function UTTempFile(ByRef sDir As String) As String
      Dim sFileName As String

      If Right(sDir, 1) <> "\" Then
         sFileName = sDir & "\tempFile.tmp"
      Else
         sFileName = sDir & "tempFile.tmp"
      End If

      UTTempFile = sFileName
   End Function

   Public Function UTStr1Str2Str3(ByRef szString As String, ByRef sStr1 As String, ByRef sStr2 As Integer, ByRef sStr3 As Integer) As Boolean
      On Error GoTo lblErr

      Dim zPos As Integer
      Dim zPos2 As Integer

      If szString <> "" Then
         zPos = InStr(szString, sINDEX_DELIMITER)
         zPos2 = InStr(zPos + 1, szString, sINDEX_DELIMITER)
         If zPos <> 0 Then
            sStr1 = Mid(szString, 1, zPos - 1)
            sStr2 = CInt(Mid(szString, zPos + 1, zPos2 - 1))
            sStr3 = CInt(Mid(szString, zPos2 + 1, Len(szString)))
         Else
            UTStr1Str2Str3 = False
            Exit Function
         End If
      Else
         UTStr1Str2Str3 = False
         Exit Function
      End If
      UTStr1Str2Str3 = True
      Exit Function
lblErr:

      UTStr1Str2Str3 = False
   End Function

   Public Function UTStr1Str2(ByRef szString As String, ByRef sStr1 As String, ByRef sStr2 As Integer) As Boolean
      On Error GoTo lblErr

      Dim zPos As Integer
      Dim zPos2 As Integer

      If szString <> "" Then

         zPos = InStr(szString, sINDEX_DELIMITER)
         zPos2 = InStr(zPos + 1, szString, sINDEX_DELIMITER)

         If zPos <> 0 Then
            sStr1 = Mid(szString, 1, zPos - 1)
            sStr2 = CInt(Mid(szString, zPos + 1, (zPos2 - 1) - (zPos)))
         Else
            UTStr1Str2 = False
            sStr1 = ""
            sStr2 = CInt("")
            Exit Function
         End If
      Else
         UTStr1Str2 = False
         sStr1 = ""
         sStr2 = CInt("")
         Exit Function
      End If
      UTStr1Str2 = True
      Exit Function
lblErr:

      UTStr1Str2 = False
   End Function

   Public Function UTStr2(ByRef szString As String) As Integer
      On Error GoTo lblErr

      Dim zPos As Integer
      Dim zPos2 As Integer

      If szString <> "" Then
         zPos = InStr(szString, sINDEX_DELIMITER)
         zPos2 = InStr(zPos + 1, szString, sINDEX_DELIMITER)

         If zPos <> 0 Then
            UTStr2 = CInt(Mid(szString, zPos + 1, zPos2 - 1 - zPos))
            If UTStr2 = 0 Then MsgBox("What?")
         Else
            Exit Function
         End If
      Else
         Exit Function
      End If

      Exit Function
lblErr:

      UTStr2 = False
   End Function

   Public Function UTStr3(ByRef szString As String) As Integer
      On Error GoTo lblErr

      Dim zPos As Integer
      Dim zPos2 As Integer

      If szString <> "" Then
         zPos = InStr(szString, sINDEX_DELIMITER)
         zPos2 = InStr(zPos + 1, szString, sINDEX_DELIMITER)

         If zPos <> 0 Then
            UTStr3 = CInt(Mid(szString, zPos2 + 1, Len(szString)))
         Else
            Exit Function
         End If
      Else
         Exit Function
      End If

      Exit Function
lblErr:

      UTStr3 = False
   End Function

   Public Sub utSaveAll()
      Dim objFileAction As New clsFileAction
      objFileAction.saveAll()
   End Sub

   Public Sub UTCopyMemory(ByRef arr1() As Integer, ByRef arr2() As Integer)
      Dim iX As Integer
      ReDim arr1(UBound(arr2))

      For iX = 0 To UBound(arr2)
         arr1(iX) = arr2(iX)
      Next
   End Sub

   Public Sub addLogEntry(ByRef sLogEntry As String)

      Dim zHndl As Integer
      zHndl = FreeFile()
      sLogEntry = "**************************************************************************" & vbCrLf & Today & "             " & sLogEntry
      'FileOpen(zHndl, VB6.GetPath & "\cd-data.log", OpenMode.Append)
      FileOpen(zHndl, Application.StartupPath & "\cd-data.log", OpenMode.Append)
      PrintLine(zHndl, sLogEntry)
      FileClose(zHndl)
      Exit Sub
lblErr:
      'MsgBox Err.Description
      FileClose(zHndl)
   End Sub

   'Input string format:
   '    Ex: "John Smith" OR "Carol Smith"
   'Output:
   '    aTerm(0) = "John Smith"
   '    aTerm(1) = "Carol Smith"
   '    UTSplitToken = 2

   Public Function UTSplitToken(ByRef sString As String, ByRef aTerms() As String, ByRef strDelim As String) As Integer
      Dim sTemp As String
      Dim sNewTerm As String
      Dim iLen As Integer
      Dim iCurrentPos As Integer
      Dim iLastPos As Integer

      sTemp = Trim(sString)
      iLen = Len(sTemp)
      UTSplitToken = 0

      If sTemp = "" Then
         Exit Function
      End If

      ReDim aTerms(0)

      If Left(sTemp, 1) = strDelim Then
         iCurrentPos = InStr(2, sTemp, strDelim)
         If iCurrentPos = iLen Then
            iLastPos = iLen
         Else
            iLastPos = 2
         End If
      Else
         iCurrentPos = InStr(1, sTemp, strDelim)
         iLastPos = 1
      End If

      If iCurrentPos <= iLastPos Then
         aTerms(0) = sTemp
         UTSplitToken = 1
         Exit Function
      End If

      Do Until iCurrentPos >= iLen
         Do Until Mid(sTemp, iCurrentPos, 1) = strDelim
            iCurrentPos = iCurrentPos + 1
            If iCurrentPos > iLen Then Exit Do
         Loop

         sNewTerm = Mid(sTemp, iLastPos, iCurrentPos - iLastPos)
         If Not (UBound(aTerms) = 0 And aTerms(UBound(aTerms)) = "") Then
            ReDim Preserve aTerms(UBound(aTerms) + 1)
         End If

         aTerms(UBound(aTerms)) = sNewTerm
         iLastPos = iCurrentPos + 1
         UTSplitToken = UTSplitToken + 1

         'Looking for token delimeter
         '      Do Until mid(sTemp, iLastPos, 1) <> strDelim
         '         iLastPos = iLastPos + 1
         '         If iLastPos > iLen Then GoTo lblExit
         '      Loop

         iCurrentPos = iLastPos
      Loop

lblExit:
      Exit Function

lblErr:
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Utilities : UTSplitToken")
      UTSplitToken = 0
   End Function

   Public Function UTGetToken(ByRef strInput As String, Optional ByRef strDelimiter As String = vbNullChar) As String

      'take a string separated by nulls, split off 1 item, and shorten the string
      'so the next item is ready for removal.

      'The passed string must have a terminating null for this function to work correctly.
      'If you remain in a loop, check this first!

      Dim pos As Integer
      Dim item As String

      pos = InStr(1, strInput, strDelimiter)
      If pos Then
         item = Mid(strInput, 1, pos - 1)
         strInput = Mid(strInput, pos + 1, Len(strInput))
         UTGetToken = item
      Else
         UTGetToken = strInput
         strInput = ""
      End If

   End Function

   Public Function UTSplitWords(ByRef sString As String, ByRef aTerms() As String) As Integer
      Dim strTmp As String
      Dim strKey As String
      Dim iCnt As Integer

      strTmp = sString
      iCnt = 0
      Do Until strTmp = ""
         strKey = UTGetToken(strTmp)
         iCnt = iCnt + 1
         ReDim Preserve aTerms(iCnt)
         aTerms(iCnt - 1) = strKey
      Loop

      UTSplitWords = iCnt
   End Function

   Public Function UTSplitString(ByRef sString As String, ByRef aTerms() As String) As Integer
      Dim sTemp As String
      Dim sNewTerm As String
      Dim iLen As Integer
      Dim iCurrentPos As Integer
      Dim iLastPos As Integer

      sTemp = Trim(sString)
      iLen = Len(sTemp)

      UTSplitString = 0
      If sTemp = "" Then
         Exit Function
      End If

      ReDim aTerms(0)
      iCurrentPos = InStr(1, sTemp, " ")
      iLastPos = 1

      If iCurrentPos <= 0 Then
         aTerms(0) = sTemp
         UTSplitString = 1
         Exit Function
      End If

      Do Until iCurrentPos >= iLen
         Do Until Mid(sTemp, iLastPos, 1) <> " "
            iLastPos = iLastPos + 1
            If iLastPos > iLen Then GoTo lblExit
         Loop

         iCurrentPos = iLastPos
         Do Until Mid(sTemp, iCurrentPos, 1) = " "
            iCurrentPos = iCurrentPos + 1
            If iCurrentPos > iLen Then Exit Do
         Loop

         sNewTerm = Mid(sTemp, iLastPos, iCurrentPos - iLastPos)
         If Not (UBound(aTerms) = 0 And aTerms(UBound(aTerms)) = "") Then
            ReDim Preserve aTerms(UBound(aTerms) + 1)
         End If

         aTerms(UBound(aTerms)) = sNewTerm
         iLastPos = iCurrentPos
         UTSplitString = UTSplitString + 1
      Loop

lblExit:
      Exit Function

lblErr:
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Utilities : UTSplitString")
      UTSplitString = 0
   End Function

   Public Function UTClng(ByRef sTerm As String) As Integer
      On Error GoTo lblErr
      UTClng = CInt(sTerm)
      Exit Function

lblErr:
      UTClng = 0
   End Function

   Public Function UTCSng(ByRef sTerm As String) As Single
      On Error GoTo lblErr

      UTCSng = CSng(sTerm)
      Exit Function

lblErr:
      UTCSng = 0
   End Function

   Public Function UTCheckFile(ByRef sFilePath As String) As Boolean
      If UTCheckFileExists(sFilePath) = False Then
         MsgBox("The following file can not be found : & vbcrlf & sfilepath", MsgBoxStyle.Exclamation, "Alert")
         Exit Function
      End If

      UTCheckFile = True
   End Function

   'Public Sub UTDeleteColumn(ByRef lCol As Integer)

   '	Dim tempListCols() As ListColumns
   '	Dim i As integer
   '	Dim iTempIndex As integer

   '	ReDim tempListCols(UBound(aListColumns) - 1)

   '	For i = 0 To UBound(aListColumns)
   '		If i <> lCol Then
   '			tempListCols(iTempIndex).Field = aListColumns(i).Field
   '			tempListCols(iTempIndex).Header = aListColumns(i).Header
   '			iTempIndex = iTempIndex + 1
   '		End If
   '	Next 

   '	ReDim aListColumns(UBound(aListColumns) - 1)
   '	For i = 0 To UBound(tempListCols)
   '		aListColumns(i).Field = tempListCols(i).Field
   '		aListColumns(i).Header = tempListCols(i).Header
   '	Next 

   'End Sub

   'Public Sub UTInsertColumn(ByRef lCol As Integer)

   '	Dim tempListCols() As ListColumns
   '	Dim i As integer
   '	Dim iTempIndex As integer

   '	ReDim tempListCols(UBound(aListColumns) + 1)

   '	For i = 0 To UBound(aListColumns)
   '		If i <> lCol Then
   '			tempListCols(i).Field = aListColumns(iTempIndex).Field
   '			tempListCols(i).Header = aListColumns(iTempIndex).Header
   '			iTempIndex = iTempIndex + 1
   '		End If
   '	Next 

   '	ReDim aListColumns(UBound(aListColumns) + 1)
   '	For i = 0 To UBound(tempListCols)
   '		aListColumns(i).Field = tempListCols(i).Field
   '		aListColumns(i).Header = tempListCols(i).Header
   '	Next 

   'End Sub

   Public Function UTFreeFile() As Integer
      Try
         UTFreeFile = FreeFile()
      Catch ex As Exception
         UTFreeFile = 0
         LogMsg("Error in UTFreeFile.  Last free file is " & m_zLastFreeFile & " - Error: " & ex.Message())
      End Try

      If UTFreeFile > m_zLastFreeFile Then
         m_zLastFreeFile = UTFreeFile
      End If
   End Function

   Public Sub ParsePathname(ByRef strString As String, ByRef strPath As String, ByRef strFilename As String)
      On Error GoTo lblParseErr

      Dim iPos As Integer
      strPath = ""
      strFilename = ""
      If strString <> "" Then
         iPos = InStrRev(strString, "\")
         If iPos > 1 Then
            strPath = Left(strString, iPos)
            strFilename = Mid(strString, iPos + 1, Len(strString) - iPos)
         End If
      End If

      Exit Sub
lblParseErr:

   End Sub

   Public Function CheckDataCD() As Boolean
      On Error GoTo lblCDCErr

      Dim strTmp As String
      Dim iRet As Integer

      strTmp = g_sWorkDir & g_sMasterRecordFile
      Do
         If File.Exists(strTmp) Then
            CheckDataCD = True
            Exit Do
         Else
            iRet = MsgBox("Please insert the Datadisc into the CD-ROM drive (" & Left(g_sWorkDir, 2) & ") then try again!", MsgBoxStyle.RetryCancel, "Datadisc not found!")
            If iRet = MsgBoxResult.Cancel Then CheckDataCD = False
         End If
      Loop While iRet = MsgBoxResult.Retry
      Exit Function
lblCDCErr:
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Utilities : CheckDataCD")
      CheckDataCD = False
   End Function

   Public Function copyCdaFiles() As Boolean
      Dim sSrc, sTmp, sDst As String

      On Error GoTo copyCdaFiles_Error

      'Copy county files to working folder
      sDst = g_sWorkDir
      sSrc = g_sProdRoot & "cdassr\*.*"
      sTmp = Dir(sSrc)
      Do While sTmp <> ""
         FileCopy(g_sProdRoot & "cdassr\" & sTmp, sDst & sTmp)
         sTmp = Dir()
      Loop

      copyCdaFiles = True
      Exit Function

copyCdaFiles_Error:
      LogMsg("Missing CDA file: " & sSrc & ".  Please check and retry!")
      copyCdaFiles = False
   End Function

   Public Function copyProdFiles(ByRef sProduct As String, ByRef sCntyName As String) As Boolean
      Dim sSrc, sTmp, sDst As String
      Dim sUseName, sProdDir As String
      Dim iTmp As Integer

      On Error GoTo copyProdFiles_Error

      'Copy City table to working folder
      sSrc = g_sCityTbl & "\" & Right(sProduct, 3) & "CITY.DAT"
      FileCopy(sSrc, g_sWorkDir & Right(sProduct, 3) & "CITY.DAT")

      'Copy county files to working folder
      'sProdDir = GetIniData(Application.ExecutablePath, sProduct, "CDProd")
      sProdDir = My.Settings.CDProd
      If g_bNoChars And sProdDir <> "" Then
         sSrc = g_sProdRoot & sProdDir & "\*.*"
      Else
         sSrc = g_sProdRoot & "cdassr\*.*"
      End If

      sDst = g_sWorkDir
      sTmp = Dir(sSrc)
      Do While sTmp <> ""
         If g_bNoChars And sProdDir <> "" Then
            FileCopy(g_sProdRoot & sProdDir & "\" & sTmp, sDst & sTmp)
         Else
            FileCopy(g_sProdRoot & "cdassr\" & sTmp, sDst & sTmp)
         End If
         sTmp = Dir()
      Loop

      'Copy common tables
      sDst = g_sWorkDir & "PQAux"
      If Dir(sDst, FileAttribute.Directory) = "" Then
         LogMsg("Create folder: " & sDst)
         MkDir(sDst)
      End If
      sDst = sDst & "\"

      LogMsg("Copying common file to " & sDst)
      sSrc = g_sCommonTbl & "\*.*"

      sTmp = Dir(sSrc)
      Do While sTmp <> ""
         FileCopy(g_sCommonTbl & "\" & sTmp, sDst & sTmp)
         sTmp = Dir()
      Loop

      'Copy PQFieldMapping.csv to working folder for backward compatibility
      sTmp = "PQFieldMapping.csv"
      FileCopy(g_sCommonTbl & "\" & sTmp, g_sWorkDir & sTmp)

      'Remove space within county name
      iTmp = InStr(1, sCntyName, " ")
      If iTmp > 1 Then
         sTmp = Left(sCntyName, iTmp - 1) & Mid(sCntyName, iTmp + 1)
         iTmp = InStr(1, sTmp, " ")
         If iTmp > 1 Then
            sUseName = Left(sTmp, iTmp - 1) & Mid(sTmp, iTmp + 1)
         Else
            sUseName = sTmp
         End If
      Else
         sUseName = sCntyName
      End If

      'Copy UseCode
      sSrc = g_sUsecodeTbl & "\" & sUseName & ".pdf"
      sTmp = Dir(sSrc)
      If sTmp <> "" Then
         FileCopy(g_sUsecodeTbl & "\" & sTmp, g_sWorkDir & sTmp)
      Else
         LogMsg("*** Usecode file is missing (" & sSrc & "). Please check!")
      End If

      'Copy Lookup file to working folder
      sSrc = g_sLookupTbl & "\PQLkup." & Right(sProduct, 3)

      sTmp = Dir(sSrc)
      If sTmp <> "" Then
         sDst = Replace(sTmp, "." & Right(sProduct, 3), ".txt", , , CompareMethod.Text)
         If sDst <> "" And sDst <> sTmp Then
            FileCopy(sSrc, g_sWorkDir & "PQAux\" & sDst)
         End If
      End If

      copyProdFiles = True
      Exit Function

copyProdFiles_Error:
      LogMsg("Fail to copy file in copyProdFiles(): " & sSrc & " to [" & g_sWorkDir & "]. Please check and retry!")
      copyProdFiles = False
   End Function

   Public Function copyIndexFiles(ByRef sFromIndex As String, ByRef sToIndex As String) As Boolean
      Dim sSrc, sDst As String

      On Error GoTo copyIndexFiles_Error

      'Copy TRM
      sSrc = g_sWorkDir & sFromIndex & ".TRM"
      sDst = g_sWorkDir & sToIndex & ".TRM"
      FileCopy(sSrc, sDst)

      'Copy IND
      sSrc = g_sWorkDir & sFromIndex & ".IND"
      sDst = g_sWorkDir & sToIndex & ".IND"
      FileCopy(sSrc, sDst)

      'Copy RCD
      sSrc = g_sWorkDir & sFromIndex & ".RCD"
      sDst = g_sWorkDir & sToIndex & ".RCD"
      FileCopy(sSrc, sDst)

      copyIndexFiles = True
      Exit Function

copyIndexFiles_Error:
      LogMsg("***** Error copying " & sSrc & " to " & sDst)
      copyIndexFiles = False
   End Function
End Module