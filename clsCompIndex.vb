Option Strict Off
Option Explicit On
Option Compare Binary
Friend Class clsCompIndex


   Dim m_zHndl As Integer
   Dim m_sBuffer As String
   Public zModNumber As Integer

   Public Sub New()
      MyBase.New()
      zModNumber = zMOD_NUMBER
   End Sub

   Public Function indexAll() As Integer
      Dim iX As Integer
      Dim iRet As Integer

      g_bStopProcess = False

      Try
         For iX = 0 To g_iSearchFields - 1
            Select Case g_aFields(g_aSearchFields(iX)).Type
               Case iFIELD_TYPE_STRING
                  StringIndex((g_aSearchFields(iX)))

               Case iFIELD_TYPE_WORD
                  StringIndex((g_aSearchFields(iX)))

               Case iFIELD_TYPE_LONG, iFIELD_TYPE_LJNUM
                  longIndex((g_aSearchFields(iX)))

               Case iFIELD_TYPE_FLOAT
                  floatIndex((g_aSearchFields(iX)))
            End Select
         Next
         iRet = 0
      Catch ex As Exception
         LogMsg("clsIndex!indexAll: " & ex.Message())
         iRet = -1
      End Try

      indexAll = iRet
   End Function

   Public Sub reindex(ByRef iX As Integer)
      Try
         Select Case g_aFields(iX).Type
            Case iFIELD_TYPE_STRING
               StringIndex((iX))

            Case iFIELD_TYPE_WORD
               StringIndex((iX))

            Case iFIELD_TYPE_LONG
               longIndex((iX))

            Case iFIELD_TYPE_FLOAT
               floatIndex((iX))
         End Select
      Catch ex As Exception
         LogMsg("clsIndex!reindex: " & ex.Message())
      End Try
   End Sub

   Public Sub StringIndex(ByRef iFieldNumber As Integer)
      On Error GoTo lblErr

      Dim objReadOps As New clsReadBuffered
      Dim objTermWriteOps As New clsWriteString
      Dim objRecordWriteOps As New clsWriteLong

      Dim lTermFilePos As Integer
      Dim lrecordFilePos As Integer
      Dim lFirstTermFilePos As Integer

      Dim sRecordFile As String
      Dim iBeep As Integer
      Dim sInTemp As String
      Dim sCurrentTerm As String
      Dim sNewTerm As String
      Dim sFirstTerm As String

      Dim lCurrentTerms As Integer
      Dim lDistinctTerms As Integer

      Dim sHead As String
      Dim lRecordNumber As Integer
      Dim lPrevRecNum As Integer

      Dim lRecordCount As Integer
      Dim lTerms As Integer
      Dim lRecordSinceLastIndex As Integer
      Dim lDistinctRecords As Integer
      Dim iTmp As Integer

      lRecordCount = 0

      '   If g_aFields(iFieldNumber).Terms <> 0 Then
      '      lTerms = g_aFields(iFieldNumber).Terms
      '   Else
      '      lTerms = g_lRecords
      '   End If

      sRecordFile = UTFileName(RECORD, iFieldNumber)
      g_aFields(iFieldNumber).RecordFile = sRecordFile
      sRecordFile = g_sWorkDir & sRecordFile
      g_aFields(iFieldNumber).IndexFile = UTFileName(Index, iFieldNumber)

      If Dir(sRecordFile) <> "" Then
         Kill((sRecordFile))
      End If

      objReadOps.OpenFile(g_sWorkDir & g_aFields(iFieldNumber).TermFile)
      objTermWriteOps.OpenFile(UTTempFile(g_sWorkDir))
      objRecordWriteOps.OpenFile(sRecordFile)
      OpenFile(g_sWorkDir & g_aFields(iFieldNumber).IndexFile)
      sInTemp = objReadOps.stringGet

      sNewTerm = ""
      Do Until UTHeadTail(sInTemp, sNewTerm, lRecordNumber) Or sInTemp = ""
         sInTemp = objReadOps.stringGet
      Loop

      lPrevRecNum = 0
      sFirstTerm = sNewTerm
      lFirstTermFilePos = objTermWriteOps.lLogicalSeek
      sCurrentTerm = ""
      Do Until sInTemp = ""
         sCurrentTerm = sNewTerm
         lrecordFilePos = objRecordWriteOps.lLogicalSeek
         lTermFilePos = objTermWriteOps.lLogicalSeek
         lCurrentTerms = 0
         lDistinctTerms = lDistinctTerms + 1

         Do Until sNewTerm <> sCurrentTerm
            If lPrevRecNum <> lRecordNumber Then
               objRecordWriteOps.longWrite((lRecordNumber))
               lCurrentTerms = lCurrentTerms + 1
               lRecordCount = lRecordCount + 1
               lRecordSinceLastIndex = lRecordSinceLastIndex + 1
               lPrevRecNum = lRecordNumber
            End If

            sInTemp = objReadOps.stringGet
            If sInTemp = "" Then
               GoTo lblExit
            End If

            Do Until UTHeadTail(sInTemp, sNewTerm, lRecordNumber) Or sInTemp = ""
               sInTemp = objReadOps.stringGet
            Loop
         Loop

         System.Windows.Forms.Application.DoEvents()
         '      If g_bStopProcess Then
         '         Exit Do
         '      End If

         'Output current term since new term can be the beginning of a new index block
         objTermWriteOps.stringWrite((sCurrentTerm & sINDEX_DELIMITER & lCurrentTerms & sINDEX_DELIMITER & lrecordFilePos & sINDEX_SEPARATOR))
         lDistinctRecords = lDistinctRecords + 1

         'Output to IND file each 100 distinct terms
         If countCheck(lDistinctTerms) And lDistinctTerms <> 0 Then
            IndexAdd(((sFirstTerm & sINDEX_DELIMITER & lRecordSinceLastIndex & sINDEX_DELIMITER & lFirstTermFilePos)))
            'IndexAdd (sCurrentTerm & sINDEX_DELIMITER & lRecordSinceLastIndex & sINDEX_DELIMITER & lTermFilePos)
            lRecordSinceLastIndex = 0
            sFirstTerm = sNewTerm
            lFirstTermFilePos = objTermWriteOps.lLogicalSeek

            '         If lRecordCount < lTerms Then
            '            iBeep = (lRecordCount / lTerms) * 100
            '            setProgress (iBeep)
            '         End If
            '         setStatus "Indexing Field " & iFieldNumber + 1 & " Term :  " & lRecordCount
         End If
      Loop

lblExit:
      If lRecordSinceLastIndex > 0 And lDistinctTerms > 0 Then
         IndexAdd(((sFirstTerm & sINDEX_DELIMITER & lRecordSinceLastIndex & sINDEX_DELIMITER & lFirstTermFilePos)))
         'IndexAdd (sCurrentTerm & sINDEX_DELIMITER & lRecordSinceLastIndex & sINDEX_DELIMITER & lTermFilePos)
      End If
      objTermWriteOps.stringWrite((sCurrentTerm & sINDEX_DELIMITER & lCurrentTerms & sINDEX_DELIMITER & lrecordFilePos & sINDEX_SEPARATOR))
      lDistinctRecords = lDistinctRecords + 1

      '   setStatus ""
      '   setProgress 0

      On Error GoTo lblErr1

      'Do not close m_zHndl until terminate object "objTermWriteOps"
      Call objReadOps.CloseFile()
      objReadOps = Nothing
      Call objTermWriteOps.closeFile()
      objTermWriteOps = Nothing
      Call objRecordWriteOps.CloseFile()
      objRecordWriteOps = Nothing

      Call IndexClose()

      'Call FileCopy(UTTempFile(g_sWorkDir), g_sWorkDir & g_aFields(iFieldNumber).TermFile)
      'Kill (UTTempFile(g_sWorkDir))
      sInTemp = g_sWorkDir & g_aFields(iFieldNumber).TermFile

      If Dir(sInTemp) <> "" Then
         Kill((sInTemp))
      End If
      Rename(UTTempFile(g_sWorkDir), sInTemp)

      g_aFields(iFieldNumber).UniqueTerms = lDistinctRecords
      'System.Diagnostics.Debug.WriteLine("Distinct Records : " & lDistinctRecords)
      Exit Sub

lblErr:
      'Testing
      LogMsg("clsCompIndex!StringIndex.Err: " & Err.Description)
      GoTo lblExit
lblErr1:
      LogMsg("clsCompIndex!StringIndex.Err1: " & Err.Description)
   End Sub

   Public Function OpenFile(ByRef szFileName As String) As Boolean
      On Error GoTo lblErr

      If Dir(szFileName) <> "" Then
         Kill((szFileName))
      End If

      'open the file
      m_zHndl = FreeFile
      FileOpen(m_zHndl, szFileName, OpenMode.Binary)

      'empty the buffer
      m_sBuffer = ""
      OpenFile = True
      Exit Function

lblErr:
      LogMsg("clsCompIndex!Openfile: " & Err.Description)
   End Function

   Public Function countCheck(ByRef lCount As Integer) As Boolean
      If lCount Mod zModNumber = 0 Then countCheck = True
   End Function

   Public Sub IndexClose()
      If Len(m_sBuffer) > 1 Then
         FilePut(m_zHndl, m_sBuffer)
         m_sBuffer = ""
      End If
      FileClose(m_zHndl)
   End Sub

   Public Sub IndexAdd(ByRef sTerm As String)

      On Error GoTo lblErr
      m_sBuffer = m_sBuffer & sTerm & vbCrLf

      If Len(m_sBuffer) > OUT_BUFFER_SIZE Then
         FilePut(m_zHndl, m_sBuffer)
         m_sBuffer = ""
      End If

      Exit Sub
lblErr:
      LogMsg("clsCompIndex!IndexAdd: " & Err.Description)
      'MsgBox Err.Description, vbExclamation, "clsModIndex : IndexAdd"
   End Sub

   Public Sub longIndex(ByRef iFieldNumber As Integer)
      On Error GoTo lblErr

      Dim objReadOps As New clsReadLongBuffered
      Dim objTermWriteOps As New clsWriteLong
      Dim objRecordWriteOps As New clsWriteLong
      Dim objIndexWrite As New clsWriteLong

      Dim lTermFilePos As Integer
      Dim lrecordFilePos As Integer
      Dim sRecordFile As String

      Dim aInTemp As Object
      Dim aTemp() As Integer
      Dim lCurrentTerm As Integer
      Dim lCurBlkTerm As Integer
      Dim lNewTerm As Integer

      Dim lCurrentTerms As Integer
      Dim lDistinctTerms As Integer
      Dim lDistinctRecords As Integer
      Dim lHead As Integer
      Dim lRecordNumber As Integer
      Dim lRecordCount As Integer
      Dim lRecordSinceLastIndex As Integer

      ReDim aTemp(2)

      sRecordFile = UTFileName(RECORD, iFieldNumber)
      g_aFields(iFieldNumber).RecordFile = sRecordFile
      sRecordFile = g_sWorkDir & sRecordFile

      g_aFields(iFieldNumber).IndexFile = UTFileName(Index, iFieldNumber)


      If Dir(sRecordFile) <> "" Then
         Kill((sRecordFile))
      End If

      objReadOps.ReturnArrayLength = 2

      'Open files
      objReadOps.OpenFile(g_sWorkDir & g_aFields(iFieldNumber).TermFile)
      objTermWriteOps.OpenFile(UTTempFile(g_sWorkDir))
      objRecordWriteOps.OpenFile(sRecordFile)
      objIndexWrite.OpenFile(g_sWorkDir & g_aFields(iFieldNumber).IndexFile)

      aInTemp = objReadOps.ArrayGet
      lNewTerm = aInTemp(0)
      lRecordNumber = aInTemp(1)
      lCurBlkTerm = lNewTerm
      lTermFilePos = objTermWriteOps.lLogicalSeek
      lRecordSinceLastIndex = 0


      Do Until IsDBNull(aInTemp)

         lNewTerm = aInTemp(0)

         lCurrentTerm = lNewTerm
         lrecordFilePos = objRecordWriteOps.lLogicalSeek
         lCurrentTerms = 0
         lDistinctTerms = lDistinctTerms + 1

         Do Until lNewTerm <> lCurrentTerm
            objRecordWriteOps.longWrite((lRecordNumber))
            lCurrentTerms = lCurrentTerms + 1
            lRecordCount = lRecordCount + 1



            aInTemp = objReadOps.ArrayGet


            If IsDBNull(aInTemp) Then
               Exit Do
            End If


            lNewTerm = aInTemp(0)

            lRecordNumber = aInTemp(1)
         Loop

         System.Windows.Forms.Application.DoEvents()
         'If g_bStopProcess Then
         '   Exit Do
         'End If

         'If Not IsNull(aInTemp) Then
         'Write term info
         aTemp(0) = lCurrentTerm
         aTemp(1) = lCurrentTerms
         aTemp(2) = lrecordFilePos
         Call objTermWriteOps.arrayWrite(aTemp)
         lDistinctRecords = lDistinctRecords + 1
         'End If

         'Write index block of 100 each

         If countCheck(lDistinctTerms) Or IsDBNull(aInTemp) Then
            aTemp(0) = lCurBlkTerm
            aTemp(1) = lRecordSinceLastIndex
            aTemp(2) = lTermFilePos
            Call objIndexWrite.arrayWrite(aTemp)

            'Setting for next block
            lTermFilePos = objTermWriteOps.lLogicalSeek - 12
            lRecordSinceLastIndex = lCurrentTerms
            lCurBlkTerm = lCurrentTerm

            'Update status bar
            'setProgress((CInt(lRecordCount * 100 / g_lRecords)))
            'setStatus("Indexing Field " & iFieldNumber + 1 & " Term :  " & lRecordCount)
         Else
            lRecordSinceLastIndex = lRecordSinceLastIndex + lCurrentTerms
         End If

      Loop

      g_aFields(iFieldNumber).UniqueTerms = lDistinctTerms
      GoTo lblExit

lblErr:
      LogMsg("clsCompIndex!LongIndex: " & g_aFields(iFieldNumber).IndexFile & ":" & Err.Description)

lblExit:
      'setStatus("")
      'setProgress(0)

      objIndexWrite.CloseFile()
      objIndexWrite = Nothing
      objRecordWriteOps.CloseFile()
      objRecordWriteOps = Nothing
      objTermWriteOps.CloseFile()
      objTermWriteOps = Nothing
      objReadOps.CloseFile()
      objReadOps = Nothing

      g_aFields(iFieldNumber).UniqueTerms = lDistinctRecords

      'Call FileCopy(UTTempFile(g_sWorkDir), g_sWorkDir & g_aFields(iFieldNumber).TermFile)
      'Kill (UTTempFile(g_sWorkDir))
      Dim sInTemp As String
      sInTemp = g_sWorkDir & g_aFields(iFieldNumber).TermFile
      If Dir(sInTemp) <> "" Then
         Kill((sInTemp))
      End If
      Rename(UTTempFile(g_sWorkDir), sInTemp)

   End Sub

   Public Sub floatIndex(ByRef iFieldNumber As Integer)
      On Error GoTo lblErr

      Dim objReadOps As New clsReadFloatBuffered
      Dim objTermWriteOps As New clsWriteFloat
      Dim objRecordWriteOps As New clsWriteLong
      Dim objIndexWrite As New clsWriteFloat

      Dim lTermFilePos As Integer
      Dim lrecordFilePos As Integer
      Dim sRecordFile As String

      Dim aInTemp As Object
      Dim aTemp() As Single
      Dim lCurrentTerm As Single
      Dim lCurBlkTerm As Single
      Dim lNewTerm As Single

      Dim lCurrentTerms As Integer
      Dim lDistinctTerms As Integer
      Dim lDistinctRecords As Integer
      Dim lHead As Single
      Dim lRecordNumber As Integer
      Dim lRecordCount As Integer
      Dim lRecordSinceLastIndex As Integer

      ReDim aTemp(2)

      lRecordCount = 0
      sRecordFile = UTFileName(RECORD, iFieldNumber)
      g_aFields(iFieldNumber).RecordFile = sRecordFile
      sRecordFile = g_sWorkDir & sRecordFile

      g_aFields(iFieldNumber).IndexFile = UTFileName(Index, iFieldNumber)


      If Dir(sRecordFile) <> "" Then
         Kill((sRecordFile))
      End If

      objReadOps.ReturnArrayLength = 2

      'Open files
      objReadOps.OpenFile(g_sWorkDir & g_aFields(iFieldNumber).TermFile)
      objTermWriteOps.OpenFile(UTTempFile(g_sWorkDir))
      objRecordWriteOps.OpenFile(sRecordFile)
      objIndexWrite.OpenFile(g_sWorkDir & g_aFields(iFieldNumber).IndexFile)



      aInTemp = objReadOps.ArrayGet

      lNewTerm = aInTemp(0)

      lRecordNumber = aInTemp(1)
      lCurBlkTerm = lNewTerm
      lTermFilePos = objTermWriteOps.lLogicalSeek
      lRecordSinceLastIndex = 0


      Do Until IsDBNull(aInTemp)

         lNewTerm = aInTemp(0)

         lCurrentTerm = lNewTerm
         lrecordFilePos = objRecordWriteOps.lLogicalSeek
         lCurrentTerms = 0
         lDistinctTerms = lDistinctTerms + 1

         Do Until lNewTerm <> lCurrentTerm
            objRecordWriteOps.longWrite((lRecordNumber))
            lCurrentTerms = lCurrentTerms + 1
            lRecordCount = lRecordCount + 1



            aInTemp = objReadOps.ArrayGet


            If IsDBNull(aInTemp) Then
               Exit Do
            End If


            lNewTerm = aInTemp(0)

            lRecordNumber = aInTemp(1)
         Loop

         System.Windows.Forms.Application.DoEvents()
         If g_bStopProcess Then
            Exit Do
         End If

         'If Not IsNull(aInTemp) Then
         'Write term info
         aTemp(0) = lCurrentTerm
         aTemp(1) = lCurrentTerms
         aTemp(2) = lrecordFilePos
         Call objTermWriteOps.arrayWrite(aTemp)
         lDistinctRecords = lDistinctRecords + 1
         'End If

         'Write index block of 100 each
         If countCheck(lDistinctTerms) And lDistinctTerms <> 0 Then
            aTemp(0) = lCurBlkTerm
            aTemp(1) = lRecordSinceLastIndex
            aTemp(2) = lTermFilePos
            Call objIndexWrite.arrayWrite(aTemp)

            'Setting for next block
            lTermFilePos = objTermWriteOps.lLogicalSeek - 12
            lRecordSinceLastIndex = lCurrentTerms
            lCurBlkTerm = lCurrentTerm

            'Update status bar
            'setProgress((CInt(lRecordCount * 100 / g_lRecords)))
            'setStatus("Indexing Field " & iFieldNumber + 1 & " Term :  " & lRecordCount)
         Else
            lRecordSinceLastIndex = lRecordSinceLastIndex + lCurrentTerms
         End If

      Loop

      g_aFields(iFieldNumber).UniqueTerms = lDistinctTerms

lblExit:
      'setStatus("")
      'setProgress(0)

      'UPGRADE_NOTE: Object objIndexWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
      objIndexWrite = Nothing
      'UPGRADE_NOTE: Object objRecordWriteOps may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
      objRecordWriteOps = Nothing
      'UPGRADE_NOTE: Object objTermWriteOps may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
      objTermWriteOps = Nothing
      'UPGRADE_NOTE: Object objReadOps may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
      objReadOps = Nothing

      'Call FileCopy(UTTempFile(g_sWorkDir), g_sWorkDir & g_aFields(iFieldNumber).TermFile)
      'Kill (UTTempFile(g_sWorkDir))
      Dim sInTemp As String
      sInTemp = g_sWorkDir & g_aFields(iFieldNumber).TermFile

      If Dir(sInTemp) <> "" Then
         Kill((sInTemp))
      End If
      Rename(UTTempFile(g_sWorkDir), sInTemp)
      Exit Sub

lblErr:

      LogMsg("clsCompIndex!FloatIndex: " & Err.Description)
      'MsgBox Err.Description, vbExclamation, "clsCompIndex : FloatIndex"
      GoTo lblExit
   End Sub
End Class