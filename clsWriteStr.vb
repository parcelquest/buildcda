Option Strict Off
Option Explicit On
Friend Class clsWriteString

   Dim m_zHndl As Integer
   Dim m_sBuffer As String

   Public lLogicalSeek As Integer
   Public BufferSize As Integer

   Public Function termCheck(ByRef sTerm As String) As Boolean
      termCheck = True
   End Function

   Public Sub stringWrite(ByRef sNewString As String)

      On Error GoTo lblErr

      lLogicalSeek = lLogicalSeek + Len(sNewString)

      m_sBuffer = m_sBuffer & sNewString

      If Len(m_sBuffer) > BufferSize Then

         FilePut(m_zHndl, m_sBuffer)
         m_sBuffer = ""
      End If

      Exit Sub

lblErr:
      LogMsg("clsWriteString!StringWrite: " & Err.Description)
      '    MsgBox Err.Description, vbExclamation, "clsWriteString : StringWrite"
   End Sub


   Public Function OpenFile(ByRef szFileName As String) As Boolean

      On Error GoTo lblErr


      If Dir(szFileName) <> "" Then
         Kill((szFileName))
      End If

      'open the file
      m_zHndl = FreeFile
      FileOpen(m_zHndl, szFileName, OpenMode.Binary)

      'empty the buffer
      m_sBuffer = ""

      'set the logical seek
      lLogicalSeek = Seek(m_zHndl)

      OpenFile = True

      Exit Function

lblErr:
      LogMsg("clsWriteString!OpenFile: " & Err.Description)
      '    MsgBox Err.Description, vbExclamation, "clsWriteString : OpenFile"
   End Function

   Private Sub Class_Initialize_Renamed()
      BufferSize = OUT_BUFFER_SIZE_STRING_EXTRACTION
   End Sub

   Public Sub New()
      MyBase.New()
      Class_Initialize_Renamed()
   End Sub

   Protected Overrides Sub Finalize()
      CloseFile()
      MyBase.Finalize()
   End Sub

   Public Function termWrite(ByRef sTerm As String, ByRef lRecordNumber As Integer) As Boolean
      Dim sTemp As String

      sTemp = Trim(sTerm)

      If sTemp = "" Then
         sTemp = " "
      End If
      stringWrite((sTemp & sINDEX_DELIMITER & lRecordNumber & vbCrLf))

      termWrite = True
   End Function

   Public Sub CloseFile()
      Try
         If m_sBuffer <> "" Then
            FilePut(m_zHndl, m_sBuffer)
         End If
         FileClose(m_zHndl)
      Catch ex As Exception
         'Ignore error
      End Try

   End Sub
End Class