Option Strict Off
Option Explicit On
Option Compare Text
Imports System.IO
Imports VB = Microsoft.VisualBasic

Module modMain

   'VB Fileio functions
   Public Const FA_S_IFDIR As Integer = 40000 ' directory
   Public Const FA_S_IFCHR As Integer = 20000 ' character special
   Public Const FA_S_IFREG As Integer = 100000 ' regular
   Public Const FA_S_IREAD As Integer = 400 ' read permission, owner
   Public Const FA_S_IWRITE As Integer = 200 ' write permission, owner
   Public Const FA_S_IEXEC As Integer = 100 ' execute/search permission, owner

   Public Const gc_MAXRECS As Integer = 100
   Public Const DefaultFieldHeight As Integer = 300

   'Public Const gc_FONTBOLD As Integer = 1
   'Public Const gc_FONTITALIC As Integer = 2
   'Public Const gc_FONTSTRIKE As Integer = 4
   'Public Const gc_FONTUNDERLINE As Integer = 8

   'Following constants control height and width of child windows
   'Public Const SUB_HEIGHT As Integer = 2135
   'Public Const SUB_WIDTH As Integer = 230
   'Public Const MAX_FORMHEIGHT As Integer = 8000
   'Public Const MAX_PAGEHEIGHT As Integer = 14500

   'To be used in frmBrowse and clsBrowseDisplay
   Public Const TERMSCOL As Integer = 1
   Public Const COUNTCOL As Integer = 2

   Public Const zMOD_NUMBER As Integer = 100
   Public Const sINDEX_SEPARATOR As String = vbCrLf
   Public Const IN_BUFFER_SIZE As Integer = 1000000
   Public Const IN_ARRAY_BUFFER_SIZE As Integer = 10000
   Public Const OUT_BUFFER_SIZE As Integer = 5000
   Public Const OUT_BUFFER_SIZE_STRING_EXTRACTION As Integer = 5000
   Public Const OUT_BUFFER_SIZE_LONG_EXTRACTION As Integer = 10000

   Public Const iFIELD_TYPE_STRING As Integer = 0
   Public Const iFIELD_TYPE_WORD As Integer = 1
   Public Const iFIELD_TYPE_LONG As Integer = 2
   Public Const iFIELD_TYPE_FLOAT As Integer = 3
   Public Const iFIELD_TYPE_LJNUM As Integer = 4 'left justified number

   Public Const MEKeySpace As Integer = 32
   Public Const MEKeyComma As Integer = 44
   Public Const MEKeyDoubleQuote As Integer = 34

   Public Const g_cNULLLONG As Integer = 0.0#
   Public Const g_cNULLFLOAT As Single = 0.0#
   Public Const g_cUPPERLONG As Integer = 2147483647.0#
   Public Const g_cUPPERFLOAT As Single = 3.402823E+38

   Public Const TERM As String = "TRM"
   Public Const RECORD As String = "RCD"
   Public Const WORD As String = "WRD"
   Public Const Index As String = "IND"
   Public Const MODINDEX As String = "MOD"

   Const sRECORD_FILE_NAME As String = "RCD"
   Const sINDEX_FILE_NAME As String = "IND"
   Public sINDEX_DELIMITER As String

   Public arrIndex() As Integer
   Public g_arrResult() As Integer
   Public g_aSearchFields() As Integer
   Public g_aDisplayFields() As Integer
   Public g_aExtractFields() As Integer
   Public g_iFields As Integer
   Public g_iSearchFields As Integer
   Public g_iExtractFields As Integer
   Public g_iDisplayFields As Integer
   Public g_sSortOrder As String

   Public g_sPrefix As String
   Public g_sRawFile As String
   Public g_iRawFile As Integer 'Number of raw files for big county
   Public g_sProdRoot As String
   Public g_sTmpDir As String
   Public g_sWorkDir As String
   Public g_sMasterRecordFile As String
   Public g_iRecLen As Integer
   Public g_lRecords As Integer
   Public g_lMinRecs As Integer
   Public g_sProjectFile As String
   Public g_sProjectINI As String
   Public g_sProvider As String
   Public g_sStorage As String
   Public g_sZipPath As String
   Public g_sCityTbl As String
   Public g_sCommonTbl As String
   Public g_sUsecodeTbl As String
   Public g_sLookupTbl As String
   Public g_sCfgFile As String
   Public g_sExt As String
   Public g_sMergeAdr As String

   Public RecordNumbers() As Integer

   Public g_bStopProcess As Boolean
   Public g_bDebugSort As Boolean
   Public g_bEditListView As Boolean

   Public g_iProdFound As Integer
   Public g_atCntyInfo(100) As CountyInfo

   Public g_asNoMerge() As String
   Public g_iNoMerge As Integer
   Public g_asResort() As String
   Public g_iResort As Integer
   Public g_asNoPqz() As String

   Public g_asMail() As String
   Public g_iMail As Integer

   Public g_iNoPqz As Integer
   Public g_bNoMrg As Boolean
   Public g_bNoZip As Boolean
   Public g_bNoChars As Boolean

   Declare Sub CopyMemory Lib "kernel32" Alias "RltMoveMemory" (ByRef dest As String, ByRef Source As String, ByVal Bytes As Integer)
   Declare Function PQCompress Lib "prodlib2" (ByVal lpCounty As String) As Integer
   Private Declare Sub Sortvb Lib "OTSW32.DLL" Alias "s_1mains" (ByVal Infile As String, ByVal Outfile As String, ByVal Ctlstmt As String, ByRef Numrec As Long, ByRef Retcode As Integer)

   Public Sub initGlobals()
      Dim sTemp As String

      Try
         g_sProvider = My.Settings.SqlConn
         g_sStorage = My.Settings.Storage
         g_sZipPath = My.Settings.ZipPath
         g_sCityTbl = My.Settings.City_Tbl
         g_sUsecodeTbl = My.Settings.Use_Tbl
         g_sLookupTbl = My.Settings.Lkup_Tbl

         'Should use one common location for tables, no county specific
         g_sCommonTbl = My.Settings.Common_Tbl
         'g_sCommonTbl = GetIniData("BuildCda", g_sPrefix, "Common_Tbl")
         'If g_sCommonTbl = "" Then
         '   g_sCommonTbl = My.Settings.Common_Tbl
         'End If

         'sTemp = GetIniData("BuildCda", "NoMerge", "County")
         sTemp = My.Settings.NoMerge
         g_iNoMerge = ParseStr(sTemp, ",", g_asNoMerge)
         'sTemp = GetIniData("BuildCda", "Resort", "County")
         sTemp = My.Settings.Resort
         g_iResort = ParseStr(sTemp, ",", g_asResort)
         'sTemp = GetIniData("BuildCda", "NoPqz", "County")
         sTemp = My.Settings.NoPqz
         g_iNoPqz = ParseStr(sTemp, ",", g_asNoPqz)
         'sTemp = GetIniData("BuildCda", "Debug", "Sort")
         sTemp = My.Settings.DebugSort
         If sTemp = "Y" Then
            g_bDebugSort = True
         End If

         'Mail info
         gMailFrom = My.Settings.MailFrom
         gSMTPSecured = My.Settings.SecuredSSL
         gSMTPPort = My.Settings.SmtpPort
         gSMTPAccount = My.Settings.SmtpUserID
         gSMTPPassword = My.Settings.SmtpPwd

         sTemp = My.Settings.MailTo
         If sTemp > " " Then
            g_iMailTo = ParseStr(sTemp, ";", g_asMailTo)
         End If

         sTemp = My.Settings.MailCC
         If sTemp > " " Then
            g_iCCMail = ParseStr(sTemp, ";", g_asCCMail)
         End If

         sTemp = My.Settings.MailBCC
         If sTemp > " " Then
            g_iBCCMail = ParseStr(sTemp, ";", g_asBCCMail)
         End If
      Catch ex As Exception
         LogMsg("***** Error in BuildCda.initGlobals()")
      End Try
   End Sub

   Public Sub Usage()
      Console.WriteLine("Usage: BuildCda [-Nm] [-Nz] [-Nc] [<product>]")
      Console.WriteLine("          -Nm = Do not merge geocoded situs file")
      Console.WriteLine("          -Nc = Blank out chars data to produce CD product (i.e. YOL)")
      Console.WriteLine("          -Nz = Do not zip the result files")
      Console.WriteLine("          <product> = Product to be indexed (i.e. SAMA)")
      Console.WriteLine(" ")
   End Sub

   Private Function ParseCmd(ByVal sCmd As String) As Integer
      Dim sTemp As String
      Dim iRet As Integer

      iRet = 0
      sTemp = GetToken(sCmd, " ")
      Do While sTemp <> ""
         If sTemp = "-Nm" Then
            g_bNoMrg = True
         ElseIf sTemp = "-Nz" Then
            g_bNoZip = True
         ElseIf sTemp = "-Nc" Then
            g_bNoChars = True
         ElseIf Left(sTemp, 2) = "-C" Then
            g_sPrefix = Mid(sTemp, 3)
         ElseIf Left(sTemp, 1) <> "-" Then
            g_sPrefix = sTemp
         End If
         sTemp = GetToken(sEmpty, " ")
         iRet += 1
      Loop

      ParseCmd = iRet
   End Function

   '**************************************************************************
   Public Sub Main()
      '**************************************************************************
      Dim sTemp, sCmd, sCurDir As String
      Dim bRet, bOneWay, MailToError As Boolean
      Dim iCntyIdx, iRet, hInst As Integer
      Dim objFileAction As New clsFileAction

      MailToError = False
      'Single instance only - Remove this after testing on multi-instance
      If (UBound(Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName)) > 0) Then Exit Sub
      sINDEX_DELIMITER = Chr(31)

      'Setup log file
      g_logFile = My.Settings.LogDir & "\BuildCda_" & Format(Now, "yyyyMMdd") & ".log"
      LogMsg("BuildCda " & My.Application.Info.Version.ToString)

      g_bNoMrg = False
      g_bNoZip = False
      g_bNoChars = False
      g_sPrefix = ""
      sCurDir = ""
      sCmd = ""
      iRet = 0

      ' Parse command line
      For Each argument As String In My.Application.CommandLineArgs
         If argument = "-Nm" Then
            g_bNoMrg = True
         ElseIf argument = "-Nz" Then
            g_bNoZip = True
         ElseIf argument = "-Nc" Then
            g_bNoChars = True
         ElseIf Left(argument, 2) = "-C" Then
            g_sPrefix = Mid(argument, 3)
         ElseIf Left(argument, 1) <> "-" Then
            g_sPrefix = argument
         End If
         sCmd = argument
         iRet += 1
      Next

      'sCmd = VB.Command()
      If InStr(sCmd, "?") Then
         Call Usage()
         End
      End If
      'iRet = ParseCmd(sCmd)
      'If iRet = 0 Then
      '   Call Usage()
      '   End
      'End If

      'Determine whether it should return to process next county
      bOneWay = IIf(g_sPrefix <> "", True, False)

      'Init global variables
      Call initGlobals()

      If sCmd = "TESTEMAIL" Then
         LogMsg("Test sending email")
         gMailBody = "* Email testing *"
         gMailSubject = "BuildCda test email"
         doSendMail(gMailSubject, gMailBody, g_logFile)
         Exit Sub
      End If
Next_County:
      If g_sPrefix = "" Then
         'Get available county to process
         g_iProdFound = getCDA(g_atCntyInfo)
         If g_iProdFound <= 0 Then
            LogMsg("No county available")
            GoTo BuildCda_Exit
         End If

         For iCntyIdx = 0 To g_iProdFound - 1
            g_sPrefix = g_atCntyInfo(iCntyIdx).Prefix
            If g_sPrefix = "" Or InitSettings(g_sPrefix, True) = True Then
               Exit For
            End If
         Next
         If iCntyIdx = g_iProdFound Then
            LogMsg("No county available")
            GoTo BuildCda_Exit
         End If
      Else
         If InitSettings(g_sPrefix, False) = False Then
            LogMsg("Error initializing BUILDCDA: " & Err.Description)
            gMailSubject = g_sPrefix & " - Error initializing BUILDCDA"
            MailToError = True
            GoTo BuildCda_Exit
         End If
         'Make sure data is ready
         g_iProdFound = getCDA(g_atCntyInfo)
         If g_iProdFound < 1 Then
            LogMsg("***** Product does not exist in Products table: " & g_sPrefix)
            GoTo BuildCda_Exit
         End If
         iCntyIdx = 0
      End If
      g_iRawFile = g_atCntyInfo(iCntyIdx).NumFiles
      g_lMinRecs = g_atCntyInfo(iCntyIdx).LastRecsCount
      If g_lMinRecs > 10000 Then
         g_lMinRecs = (g_lMinRecs / 100) * 95 'Take 95% of last count
      Else
         g_lMinRecs = 500
      End If

      LogMsg("Start build index for " & g_sPrefix)

      'Copying required files
      LogMsg("Copy DPF and mask files for " & g_sPrefix)
      If reqPqz(g_sPrefix) Then
         bRet = copyProdFiles(g_sPrefix, g_atCntyInfo(iCntyIdx).CountyName)
      Else
         bRet = copyCdaFiles()
      End If

      If Not bRet Then
         gMailSubject = g_sPrefix & " - Error copying CDA files"
         MailToError = True
         GoTo BuildCda_Exit
      End If

      Dim sTmpFile, sInfile, sTmp As String
      If File.Exists(g_sProjectFile) Then
         'Preprocess - Merge geocoded address if available
         If (g_bNoMrg = False) And geoReady(g_sPrefix) Then
            'Resort if needed
            If reqResort(g_sPrefix) Then

               'Create saved file name
               sTmpFile = g_sProdRoot & "RAW\" & g_sPrefix & ".sav"
               If File.Exists(sTmpFile) Then
                  Kill(sTmpFile)
               End If
               'Rename current raw file before sort
               Rename(g_sRawFile, sTmpFile)

               'otsort %1.org %1.r01 C:\TOOLS\1900.ctl
               sCmd = "OTSORT " & sTmpFile & " " & g_sRawFile & " C:\Tools\1900.ctl"
               LogMsg(sCmd)
               hInst = Shell(sCmd, AppWinStyle.MinimizedNoFocus, True, 900000)   'wait for 10 mins
               If hInst > 0 Then
                  LogMsg("*** Sorting take more than 15 mins.  program abort!")
                  gMailSubject = g_sPrefix & " - Sorting take more than 15 mins.  program abort!"
                  MailToError = True
                  GoTo BuildCda_Exit
               End If
            End If

            If g_bNoChars Then
               'Set blank
               sInfile = " -I" & g_sTmpDir & "\" & g_sPrefix & ".G01"
               g_sRawFile = " -O" & g_sTmpDir & "\" & g_sPrefix & g_sExt

               sCmd = My.Settings.SetBlank & " -L1934" & sInfile & g_sRawFile & " -C" & g_sCfgFile
               LogMsg("SetBlank: " & sCmd)
               hInst = Shell(sCmd, AppWinStyle.MinimizedNoFocus, True, 600000)
            Else
               'Merge address
               sCmd = g_sMergeAdr & " " & Right(g_sPrefix, 3)
               LogMsg("Merge address: " & sCmd)
               hInst = Shell(sCmd, AppWinStyle.MinimizedNoFocus, True, 900000)
               g_sExt = ""
            End If
            If hInst > 0 Then
               LogMsg("*** Sorting take more than 15 mins.  program abort!")
               gMailSubject = g_sPrefix & " - Sorting take more than 15 mins.  program abort!"
               MailToError = True
               GoTo BuildCda_Exit
            End If

            'Set raw file name
            If g_sTmpDir <> "" Then
               If g_sExt <> "" Then
                  g_sRawFile = g_sTmpDir & "\" & g_sPrefix & g_sExt
               Else
                  g_sRawFile = g_sTmpDir & "\" & g_sPrefix & ".G01"
               End If
            Else
               g_sRawFile = g_sProdRoot & "RAW\" & g_sPrefix & ".G01"
            End If
         End If

         'Verify input processing file
         If File.Exists(g_sRawFile) = False Then
            gMailSubject = g_sPrefix & " - Missing input file: " & g_sRawFile
            LogMsg("***** " & gMailSubject)
            MailToError = True
            GoTo BuildCda_Exit
         End If

         'Read definition
         LogMsg("Read data layout")
         objFileAction.readAll()

         'Build index
         LogMsg("Build index ...")
         iRet = doBuildAll()
         If iRet <> 0 Then
            MailToError = True
            gMailSubject = "Build index error"
            GoTo BuildCda_Exit
         End If

         'Validate output file
         bRet = validateCounty(g_sPrefix)
         If bRet = False Then
            sTemp = "Invalid number of output files.  Please check ===> " & g_sPrefix
            LogMsg(sTemp)
            gMailSubject = g_sPrefix & " - Invalid number of output files"
            MailToError = True
            GoTo BuildCda_Exit
         End If

         If g_lRecords > g_lMinRecs Then
            'Compress file
            If reqPqz(g_sPrefix) Then
               LogMsg("Build PQZ file")
               iRet = PQCompress(Right(g_sPrefix, 3))
               If iRet <> 1 Then
                  LogMsg("***** Error compress CMP file.  Please check PQComp.log file")
                  gMailSubject = g_sPrefix & " - Error creating CMP file"
                  MailToError = True
                  GoTo BuildCda_Exit
               End If
            End If

            If g_bNoZip Then GoTo BuildCda_NoZip

            'Zip output only if production root is a drive letter
            If Left(g_sProdRoot, 1) <> "\" Then
               'Package files
               LogMsg("Build ZIP file")

               'Save current directory
               sCurDir = CurDir()

               LogMsg("CurDir = " & sCurDir)
               LogMsg("RootDir = " & g_sProdRoot)

               'Change current working directory
               If Left(sCurDir, 1) <> Left(g_sProdRoot, 1) Then
                  ChDrive(Left(g_sProdRoot, 1))
               End If
               ChDir(g_sProdRoot)

               'Create zip file
               LogMsg("Start zipping " & g_sPrefix & " ...")
               bRet = zipProduct()

               'Restore current directory
               LogMsg("Restore current directory to: " & sCurDir)
               If Left(sCurDir, 1) <> Left(g_sProdRoot, 1) Then
                  ChDrive(Left(sCurDir, 1))
               End If
               ChDir(sCurDir)
               sCurDir = ""

               If bRet = False Then
                  'MsgBox sTemp
                  MailToError = True
                  GoTo BuildCda_Exit
               End If
            Else
               LogMsg("*** Zip file is not created.  Make sure that Root_dir is not UNC path: " & g_sProdRoot)
            End If

BuildCda_NoZip:
            'Rename folder as needed
            If g_bNoChars Then
               sTmp = g_sProdRoot & g_sPrefix & "assr.cd"
               LogMsg("Remove " & sTmp & " if exist")
               If My.Computer.FileSystem.DirectoryExists(sTmp) Then
                  Kill(sTmp & "\PQAux\*.*")
                  RmDir(sTmp & "\PQAux")
                  Kill(sTmp & "\*.*")
                  RmDir(sTmp)
               End If
               'Rename folder
               LogMsg("Rename " & g_sWorkDir & " to " & sTmp)
               Rename(g_sWorkDir, sTmp)
               sTmp = g_sProdRoot & "C" & Right(g_sPrefix, 3) & "assr"
               If My.Computer.FileSystem.DirectoryExists(sTmp) Then
                  LogMsg("Rename " & sTmp & " to " & g_sWorkDir)
                  Rename(sTmp, g_sWorkDir)
               End If
            Else
               'Update product state
               updProductState(g_sPrefix, "B")
               'Create flag file
               createFlagFile(g_sZipPath & "\" & g_sPrefix & "assr.flg")

               'Handle special case - map transfer to sale1
               If copyXfer2Sale(g_sPrefix, "Xfer2Sale") Then
                  LogMsg("Copy Transfer to Sale1")
                  'Copy 71 & 73 to 74 & 76
                  bRet = copyIndexFiles(g_sPrefix & "071", g_sPrefix & "074")
                  bRet = copyIndexFiles(g_sPrefix & "073", g_sPrefix & "076")

                  'Turn index flag ON for 74 & 76
                  If bRet Then
                     g_aFields(74).Index = True
                     bRet = objFileAction.SaveOneRec(74)
                     g_aFields(76).Index = True
                     bRet = objFileAction.SaveOneRec(76)
                  End If
               End If
            End If

            LogMsg("Build index completed for " & g_sPrefix & " with " & g_lRecords & " records")
         Else
            LogMsg("***** Number of record is too small (" & g_lRecords & ").  Flag file is removed." & "To activate this county again, fix data problem and add a flag file.")
            'Remove flag
            sTemp = g_sProdRoot & "RAW\" & Right(g_sPrefix, 3) & ".flg"
            Kill(sTemp)
            gMailSubject = g_sPrefix & " - Number of record is too small"
            MailToError = True
            GoTo BuildCda_Exit
         End If
      Else
         LogMsg("***** Missing project file: " & g_sProjectFile)
         gMailSubject = g_sPrefix & " - Missing project file"
         MailToError = True
         bOneWay = True
      End If

      If bOneWay = False Then
         g_sPrefix = ""
         GoTo Next_County
      End If

BuildCda_Error:
      If sCurDir <> "" Then
         LogMsg("***** ERROR while building index for " & g_sPrefix & ".  Err Desc: " & Err.Description)
         LogMsg("Change CurDir to " & sCurDir)
         ChDir(sCurDir)
         MailToError = True
      End If

BuildCda_Exit:
      objFileAction = Nothing
      If MailToError Then
         gMailBody = "***** Error in BUILDCDA.  Please review log file for more detail."
         doSendMail(gMailSubject, gMailBody, g_logFile)
      End If
      LogMsg("Build index complete!" & vbCrLf)
   End Sub

   Private Function InitSettings(ByRef sPrefix As String, ByRef chkFlg As Boolean) As Boolean
      Dim sTmp As String
      Dim bRet As Boolean
      Dim sFlagfile As String

      On Error Resume Next

      InitSettings = False

      g_sTmpDir = My.Settings.TmpDir
      g_sMergeAdr = My.Settings.MergeAdr
      'g_sMergeAdr = GetIniData("BuildCda", sPrefix, "MergeAdr")
      'If g_sMergeAdr = "" Then
      '   g_sMergeAdr = My.Settings.MergeAdr
      'End If

      'Get input extension (default .R01)
      If g_bNoChars Then
         g_sExt = My.Settings.CDExt
      Else
         g_sExt = My.Settings.InputExt
      End If
      'g_sExt = GetIniData("BuildCda", sPrefix, "InputExt")
      'If g_sExt = "" Then
      '   g_sExt = My.Settings.InputExt
      'End If

      'Get specific product root
      sTmp = My.Settings.RootDir
      'sTmp = GetIniData("BuildCda", sPrefix, "Root_Dir")
      'If sTmp = "" Then
      '   'Not found, get default root
      '   sTmp = My.Settings.RootDir
      'End If

      g_sProdRoot = sTmp & "\" & sPrefix & "_CD\"
      If g_bNoChars Then
         'g_sCfgFile = GetIniData("BuildCda", sPrefix, "Cfg")
         g_sCfgFile = My.Settings.BlankCfg
         g_sCfgFile = g_sCfgFile.Replace("[PROD]", sPrefix)
         sTmp = g_sProdRoot & sPrefix & "assr"
         g_sWorkDir = g_sProdRoot & "C" & Right(sPrefix, 3) & "assr"
         If My.Computer.FileSystem.DirectoryExists(sTmp) Then
            If My.Computer.FileSystem.DirectoryExists(g_sWorkDir) Then
               Kill(g_sWorkDir & "\PQAux\*.*")
               RmDir(g_sWorkDir & "\PQAux")
               Kill(g_sWorkDir & "\*.*")
               RmDir(g_sWorkDir)
            End If
            'Rename folder
            Rename(sTmp, g_sWorkDir)
         End If
      Else
         g_sRawFile = g_sProdRoot & "RAW\" & sPrefix & g_sExt
      End If

      g_sWorkDir = g_sProdRoot & sPrefix & "assr\"
      g_sProjectFile = g_sWorkDir & sPrefix & ".dpf"
      g_sMasterRecordFile = sPrefix & ".mst"
      sFlagfile = g_sProdRoot & "RAW\" & Right(sPrefix, 3) & ".flg"

      'Check working directory
      If Not My.Computer.FileSystem.DirectoryExists(g_sWorkDir) Then
         MkDir(g_sWorkDir)
      End If

      'Check for flag file
      If chkFlg = True Then
         If File.Exists(sFlagfile) Then
            bRet = True
         Else
            LogMsg("***** " & sFlagfile & " not found")
            bRet = False
         End If
      Else
         bRet = True
      End If
      InitSettings = bRet
   End Function

   Public Sub setStatus(ByRef sStatus As String)
   End Sub

   Public Sub setProgress(ByRef iNewValue As Integer)
   End Sub

   Public Function getInterval() As Integer
      Dim iRet As Integer

      Try
         iRet = My.Settings.DispInterval
         If iRet < 100 Then iRet = 100
      Catch ex As Exception
         iRet = 100
      End Try

      getInterval = iRet
   End Function

   Public Function getProjectFile() As String
      Dim objINI As New clsINIWrapper
      Dim strTemp As String = ""

      Call objINI.valueRead("Data", "PrjFile", strTemp, g_sProjectINI)
      getProjectFile = strTemp
      Call objINI.valueRead("Map", "MapExt", strTemp, strTemp)
      If strTemp = "" Then
         strTemp = ".00"
      ElseIf Left(strTemp, 1) <> "." Then
         strTemp = "." & strTemp
      End If
   End Function

   Private Function cdaSort() As Integer
      Dim iX As Integer, iRet As Integer
      Dim sCommand As String
      Dim sFilename As String
      Dim sTempFile As String
      Dim lNumRecs As Long

      sTempFile = g_sWorkDir & "sort.tmp"
      sCommand = ""
      For iX = 0 To g_iSearchFields - 1
         sFilename = g_sWorkDir & g_aFields(g_aSearchFields(iX)).TermFile
         Select Case g_aFields(g_aSearchFields(iX)).Type
            Case iFIELD_TYPE_STRING
               sCommand = "S(#1,C,A,#2,C,A) DEL(31) DUPO F(TXT)"
            Case iFIELD_TYPE_LONG
               sCommand = "S(1,4,W,A,5,4,W,A)  DUPO F(FIX,8)"
            Case iFIELD_TYPE_WORD
               sCommand = "S(#1,C,A,#2,C,A) DEL(31) DUPO F(TXT)"
            Case iFIELD_TYPE_FLOAT
               sCommand = "S(1,4,E,A,5,4,W,A)  DUPO F(FIX,8)"
         End Select

         'Clear up the event list before sorting
         If g_bDebugSort Then
            LogMsg0("Sorting: " & sFilename & ": " & sCommand)
         End If

         Try
            Call Sortvb(sFilename, sTempFile, sCommand, lNumRecs, iRet)
            g_aFields(g_aSearchFields(iX)).Sorted = True
         Catch ex As Exception
            LogMsg("***** Error calling sortvb() on " & sFilename & ": " & sCommand)
            g_aFields(g_aSearchFields(iX)).Sorted = False
         End Try

         If File.Exists(sTempFile) Then
            Kill(sFilename)
            Rename(sTempFile, sFilename)
         End If
      Next
      LogMsg("Sort complete.")
      GoTo cdaSort_Exit

cdaSort_Error:
      iRet = -1
      LogMsg("***** cdaSort(): " & sFilename & ":" & Err.Description)
cdaSort_Exit:
      cdaSort = iRet
   End Function

   Private Function doBuildAll() As Integer
      Dim startt As Date
      Dim EndTime As Date
      Dim iRet As Integer
      startt = Now
      FileClose()

      On Error GoTo doBuildAll_Error

      LogMsg("Extracting data ...")
      Dim objRec As New clsRecordExtraction
      iRet = objRec.recordsExtract
      EndTime = Now
      objRec = Nothing
      FileClose()
      System.Windows.Forms.Application.DoEvents()
      If iRet <> 0 Then
         LogMsg("***** Extracting error.  Please check for error then rebuilt")
         GoTo doBuildAll_Exit
      End If

      'Sort
      LogMsg("Sorting data using cdaSort() ...")
      iRet = cdaSort()
      FileClose()
      System.Windows.Forms.Application.DoEvents()
      If iRet <> 0 Then
         LogMsg("***** Sorting error.  Please check for error then rebuilt")
         GoTo doBuildAll_Exit
      End If

      'Build index
      LogMsg("Indexing data ...")
      Dim objIndex As New clsCompIndex
      iRet = objIndex.indexAll
      If iRet <> 0 Then
         LogMsg("***** Indexing error.  Please check for error then rebuilt")
         GoTo doBuildAll_Exit
      End If

      LogMsg("Compressing data ...")
      iRet = doCompress()
      If iRet <> 0 Then
         LogMsg("***** Compressing error.  Please check for error then rebuilt")
         GoTo doBuildAll_Exit
      End If

      'Save environment
      LogMsg("Updating DPF file ...")
      Dim objFileAction As New clsFileAction
      objFileAction.saveAll()

      iRet = 0
      GoTo doBuildAll_Exit

doBuildAll_Error:
      LogMsg("***** Unknown error : " & Err.Description)
      iRet = -1
doBuildAll_Exit:
      doBuildAll = iRet
   End Function

   Private Function doCompress() As Integer
      Dim objCompress As New clsCompress
      doCompress = objCompress.compress

      objCompress = Nothing
   End Function

   'Create empty file
   Private Sub createFlagFile(ByRef sFilename As String)
      Dim fh As Integer

      fh = FreeFile
      FileOpen(fh, sFilename, OpenMode.Output)
      FileClose(fh)
   End Sub

   'Return TRUE if all goes well, FALSE otherwise
   Public Function zipProduct() As Boolean
      Dim sTemp As String
      Dim bRet As Boolean
      Dim myZip As clsZip

      bRet = False
      myZip = New clsZip
      myZip.FilesToZip = g_sPrefix & "assr\*.*"
      myZip.ZipFilename = g_sZipPath & "\" & g_sPrefix & "assr.zip"

      LogMsg("Zipping --> " & myZip.ZipFilename & "...")

      Try
         'Zip it - subfolder included
         bRet = myZip.zipIt(True)
         If bRet = False Then
            sTemp = myZip.getErrMsg
            LogMsg("Zip message: " & Left(sTemp, Len(sTemp) - 2))
         Else
            LogMsg("Zip successful!")
         End If

         'Copy zip file to storage area
         If g_sStorage <> "" Then
            LogMsg("Copy zip file to " & g_sStorage)
            FileCopy(myZip.ZipFilename, g_sStorage & "\" & g_sPrefix & "assr.zip")
         End If
      Catch ex As Exception
         sTemp = "***** Error creating zip file " & myZip.ZipFilename
         LogMsg(sTemp)
      End Try

      myZip = Nothing
      zipProduct = bRet
   End Function

   'Is county been geocoded?
   Private Function geoReady(ByRef sPrefix As String) As Boolean
      Dim bRet As Boolean
      Dim iTmp As Integer

      'Assuming data is ready
      bRet = True
      iTmp = 0
      Do While iTmp < g_iNoMerge
         If sPrefix = g_asNoMerge(iTmp) Then
            bRet = False
            Exit Do
         End If
         iTmp = iTmp + 1
      Loop

      geoReady = bRet
   End Function

   'Determine whether this county need resort
   Private Function reqResort(ByRef sPrefix As String) As Boolean
      Dim bRet As Boolean
      Dim iTmp As Integer

      'Assuming data is ready
      bRet = False
      iTmp = 0
      Do While iTmp < g_iResort
         If sPrefix = g_asResort(iTmp) Then
            bRet = True
            Exit Do
         End If
         iTmp = iTmp + 1
      Loop

      reqResort = bRet
   End Function

   'Is PQZ needed
   Private Function reqPqz(ByRef sPrefix As String) As Boolean
      Dim bRet As Boolean
      Dim iTmp As Integer

      'Assuming data is ready
      bRet = True
      iTmp = 0
      Do While iTmp < g_iNoPqz
         If sPrefix = g_asNoPqz(iTmp) Then
            bRet = False
            Exit Do
         End If
         iTmp = iTmp + 1
      Loop

      reqPqz = bRet
   End Function

   'CHeck to see if this county needs copy xfer to sale1
   Public Function copyXfer2Sale(ByRef sPrefix As String, ByRef sMapFld As String) As Boolean
      Dim strTemp As String

      'Xfer2Sale is list of products separate by space i.e. SAMA SSUT, ...
      strTemp = My.Settings.Xfer2Sale
      If InStr(strTemp, sPrefix) > 0 Then
         copyXfer2Sale = True
      Else
         copyXfer2Sale = False
      End If
      'strTemp = GetIniData("BuildCda", sPrefix, sMapFld)
      'If Left(strTemp, 1) = "Y" Then
      '   copyXfer2Sale = True
      'Else
      '   copyXfer2Sale = False
      'End If
   End Function
End Module