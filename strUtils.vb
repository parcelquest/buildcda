Option Strict Off
Option Explicit On
Module strUtils

   Public gUTTokens() As String ' For UTTokenize()
   Public gUTTokenCount As Integer
   Public gUTTokens2D() As String ' For UTTokenize2D()
   Public gUTTokenRowCount As Integer
   Public gUTTokenColCount As Integer

   '
   ' Sort Constants:
   '

   Public Const kUTSort_Ascending As Integer = 0
   Public Const kUTSort_Descending As Integer = 1

   '
   ' UTForm_Center() Centering methods:
   '

   Public Const kUTForm_None As Integer = 0
   Public Const kUTForm_Top As Integer = 1
   Public Const kUTForm_Bottom As Integer = 2
   Public Const kUTForm_Center As Integer = 3
   Public Const kUTForm_Left As Integer = 1
   Public Const kUTForm_Right As Integer = 2

   '
   ' Windows API:
   '

   Declare Function GetSystemMetrics Lib "USER" (ByVal Index As Integer) As Integer


   Public Const SM_CXBORDER As Integer = 5
   Public Const SM_CYBORDER As Integer = 6
   Public Const SM_CYCAPTION As Integer = 4
   Public Const SM_CYFRAME As Integer = 32
   Public Const SM_CXFRAME As Integer = 33
   '
   '

   '
   ' UTBoolToString()
   '
   Function UTBoolToString(ByRef Value As Integer) As String
      If Value Then
         UTBoolToString = "Yes"
      Else
         UTBoolToString = "No"
      End If
   End Function
   '
   Function UTDay_LongName(ByRef num As Integer) As String
      If num < 1 Or num > 7 Then
         UTDay_LongName = ""
         Exit Function
      End If

      UTDay_LongName = Format(System.DateTime.FromOADate(CDate("1/1/1995").ToOADate + (num - 1)), "dddd")
   End Function

   '
   ' UTForm_Center()
   '
   Sub UTForm_Center(ByRef frm As System.Windows.Forms.Form, ByRef onFrm As System.Windows.Forms.Form, ByRef dx As Integer, ByRef dy As Integer)
      UTForm_Place(frm, onFrm, dx, dy, kUTForm_Center, kUTForm_Center)
   End Sub

   '
   ' UTForm_CenterTop()
   '
   Sub UTForm_CenterTop(ByRef frm As System.Windows.Forms.Form, ByRef onFrm As System.Windows.Forms.Form, ByRef dx As Integer, ByRef dy As Integer)
      UTForm_Place(frm, onFrm, dx, dy, kUTForm_Center, kUTForm_Top)
   End Sub


   Function UTForm_HeightForContent(ByRef content_H As Integer, ByRef frm As System.Windows.Forms.Form) As Integer
      Dim amount As Integer
      Dim bs As Integer

      If frm.WindowState = 1 Then ' minimized
         UTForm_HeightForContent = -1
         Exit Function
      End If

      If TypeOf frm Is System.Windows.Forms.Form Then
         bs = 2 'sizeable
      Else
         bs = frm.FormBorderStyle
      End If

      Select Case bs
         Case 0 ' None
            amount = 0
         Case 1 ' Fixed Single
            amount = 2 * GetSystemMetrics(SM_CYBORDER) * VB6.TwipsPerPixelY
         Case 2 ' Sizable
            amount = 2 * GetSystemMetrics(SM_CYFRAME) * VB6.TwipsPerPixelY
         Case 3 ' Fixed Double
            amount = 2 * GetSystemMetrics(SM_CYFRAME) * VB6.TwipsPerPixelY
      End Select

      If frm.WindowState = 2 Then ' maximized
         amount = 0
      End If

      If frm.MinimizeBox Or frm.MaximizeBox Or frm.ControlBox Or frm.Text <> "" Then
         amount = amount + ((GetSystemMetrics(SM_CYCAPTION) - GetSystemMetrics(SM_CYBORDER)) * VB6.TwipsPerPixelY)
      End If

      UTForm_HeightForContent = content_H + amount
   End Function

   '
   ' UTForm_Place()
   '
   Sub UTForm_Place(ByRef frm As System.Windows.Forms.Form, ByRef onFrm As System.Windows.Forms.Form, ByRef dx As Integer, ByRef dy As Integer, ByRef horizMethod As Integer, ByRef vertMethod As Integer)
      Dim l As Integer
      Dim t As Integer
      Dim w As Integer
      Dim h As Integer
      Dim fl As Integer
      Dim ft As Integer

      If frm.WindowState = vbNormal Then

         '
         ' Get dimensions of enclosure:
         '

         If onFrm Is Nothing Then
            l = 0
            t = 0
            w = VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width)
            h = VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height)
         Else
            l = VB6.PixelsToTwipsX(onFrm.Left)
            t = VB6.PixelsToTwipsY(onFrm.Top)
            w = VB6.PixelsToTwipsX(onFrm.Width)
            h = VB6.PixelsToTwipsY(onFrm.Height)
         End If

         '
         ' Perform the horizontal placement:
         '

         Select Case horizMethod
            Case kUTForm_None
               fl = VB6.PixelsToTwipsX(frm.Left)
            Case kUTForm_Left
               fl = l + dx
            Case kUTForm_Right
               fl = l + w - VB6.PixelsToTwipsX(frm.Width) - dx
            Case kUTForm_Center
               fl = l + (0.5 * w) - (0.5 * VB6.PixelsToTwipsX(frm.Width)) + dx
            Case Else
               fl = VB6.PixelsToTwipsX(frm.Left)
         End Select

         '
         ' Perform vertical placement:
         '

         Select Case vertMethod
            Case kUTForm_None
               ft = VB6.PixelsToTwipsY(frm.Top)
            Case kUTForm_Top
               ft = t + dy
            Case kUTForm_Bottom
               ft = t + h - VB6.PixelsToTwipsY(frm.Height) - dy
            Case kUTForm_Center
               ft = t + (0.5 * h) - (0.5 * VB6.PixelsToTwipsY(frm.Height)) + dy
            Case Else
               ft = VB6.PixelsToTwipsY(frm.Top)
         End Select

         '
         ' Constrain the form to be on the screen:
         '

         If fl + VB6.PixelsToTwipsX(frm.Width) > VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) Then
            fl = VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) - VB6.PixelsToTwipsX(frm.Width)
         End If

         If ft + VB6.PixelsToTwipsY(frm.Height) > VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) Then
            ft = VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - VB6.PixelsToTwipsY(frm.Height)
         End If

         If fl < 0 Then
            fl = 0
         End If

         If ft < 0 Then
            ft = 0
         End If

         '
         ' Do the placement:
         '

         frm.Left = VB6.TwipsToPixelsX(fl)
         frm.Top = VB6.TwipsToPixelsY(ft)
      End If
   End Sub

   '
   ' UTForm_TweekForScreen()
   '
   'Sub UTForm_TweekForScreen(ByRef ThisForm As System.Windows.Forms.Form)
   '	'Changes position and size of controls and forms
   '	' to create a device-independent form

   '	Dim DesignX As integer 'Screen.TwipsPerPixelX value
   '	' on design system
   '	Dim DesignY As integer 'Screen.TwipsPerPixelX value
   '	' on design system
   '	Dim XFactor As Single 'Horizontal resize ratio
   '	Dim YFactor As Single 'Vertical resize ratio
   '	Dim X As integer 'For/Next loop variable

   '	On Error Resume Next

   '	'Set the Design so you can avoid resizing forms on
   '	' systems that use the resolution of your design monitor
   '	DesignX = 12 : DesignY = 12

   '	'Calculate the ratio of the current screen size to
   '	' the size of the design screen
   '	XFactor = CSng(VB6.TwipsPerPixelX) / CSng(DesignX)
   '	YFactor = CSng(VB6.TwipsPerPixelY) / CSng(DesignY)

   '	'If you're running on the design system
   '	' don't resize form and controls
   '	If XFactor = 1 And YFactor = 1 Then Exit Sub

   '	'Adjust ThisForm so that it is in the same relative
   '	' location and is proportionally the same size on the
   '	' current system as it is on the design system
   '	ThisForm.SetBounds(VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Left) * XFactor), VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(ThisForm.Top) * YFactor), VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Width) * XFactor), VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(ThisForm.Height) * YFactor))

   '	'Modify each control on the form so that it is
   '	' proportionally spaced and sized
   '	'UPGRADE_WARNING: Controls method Controls.count has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '	For X = 0 To ThisForm.Controls.Count() - 1
   '		'Treat the Drive List Box and Sample Combo Box
   '		' differently than other controls because these
   '		' two types of controls produce dropdown lists
   '		'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '		'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
   '		If TypeOf ThisForm.Controls(X) Is Microsoft.VisualBasic.Compatibility.VB6.DriveListBox Then
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			ThisForm.Controls(X).SetBounds(VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Controls(X).Left) * XFactor), VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(ThisForm.Controls(X).Top) * YFactor), VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Controls(X).Width) * XFactor), 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y Or Windows.Forms.BoundsSpecified.Width)
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
   '		ElseIf TypeOf ThisForm.Controls(X) Is System.Windows.Forms.ComboBox Then 
   '			'If not Simple Combo box
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls(X).Style. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			If ThisForm.Controls(X).Style <> 1 Then
   '				'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '				ThisForm.Controls(X).SetBounds(VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Controls(X).Left) * XFactor), VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(ThisForm.Controls(X).Top) * YFactor), VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Controls(X).Width) * XFactor), 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y Or Windows.Forms.BoundsSpecified.Width)
   '			End If
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
   '		ElseIf TypeOf ThisForm.Controls(X) Is System.Windows.Forms.Label Then 
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls(X).X1. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls().X1. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			ThisForm.Controls(X).X1 = ThisForm.Controls(X).X1 * XFactor
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls(X).X2. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls().X2. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			ThisForm.Controls(X).X2 = ThisForm.Controls(X).X2 * XFactor
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls(X).Y1. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls().Y1. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			ThisForm.Controls(X).Y1 = ThisForm.Controls(X).Y1 * YFactor
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls(X).Y2. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls().Y2. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			ThisForm.Controls(X).Y2 = ThisForm.Controls(X).Y2 * YFactor
   '		Else 'Move and size all other controls
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			ThisForm.Controls(X).SetBounds(VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Controls(X).Left) * XFactor), VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(ThisForm.Controls(X).Top) * YFactor), VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ThisForm.Controls(X).Width) * XFactor), VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(ThisForm.Controls(X).Height) * YFactor))
   '			'Adjust the font size of text box and label controls
   '			'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '			'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
   '			If TypeOf ThisForm.Controls(X) Is System.Windows.Forms.TextBox Then
   '				'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '				'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls(X).FontSize. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '				ThisForm.Controls(X).Font = VB6.FontChangeSize(ThisForm.Controls(X).Font, ThisForm.Controls(X).FontSize * XFactor)
   '				'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '				'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
   '			ElseIf TypeOf ThisForm.Controls(X) Is System.Windows.Forms.Label Then 
   '				'UPGRADE_WARNING: Controls method Controls.Item has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"'
   '				'UPGRADE_WARNING: Couldn't resolve default property of object ThisForm.Controls(X).FontSize. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '				ThisForm.Controls(X).Font = VB6.FontChangeSize(ThisForm.Controls(X).Font, ThisForm.Controls(X).FontSize * XFactor)
   '			End If
   '		End If
   '	Next X

   'End Sub

   '
   ' UTForm_WidthForContent()
   '
   Function UTForm_WidthForContent(ByRef content_W As Integer, ByRef frm As System.Windows.Forms.Form) As Integer
      Dim amount As Integer
      Dim bs As Integer

      If frm.WindowState = 1 Then ' minimized
         UTForm_WidthForContent = -1
         Exit Function
      End If

      'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
      If TypeOf frm Is System.Windows.Forms.Form Then
         bs = 2 'sizeable
      Else
         bs = frm.FormBorderStyle
      End If

      Select Case bs
         Case 0 ' None
            amount = 0
         Case 1 ' Fixed Single
            amount = 2 * GetSystemMetrics(SM_CXBORDER) * VB6.TwipsPerPixelX
         Case 2 ' Sizable
            amount = 2 * GetSystemMetrics(SM_CXFRAME) * VB6.TwipsPerPixelX
         Case 3 ' Fixed Double
            amount = 2 * GetSystemMetrics(SM_CXFRAME) * VB6.TwipsPerPixelX
      End Select

      If frm.WindowState = 2 Then ' maximized
         amount = 0
      End If

      UTForm_WidthForContent = content_W + amount
   End Function

   '
   ' UTRemoveSpaces()
   '
   Function UTRemoveSpaces(ByVal s As String) As String
      UTRemoveSpaces = UTReplaceStrings(s, " ", "")
   End Function


   '
   ' UTReplaceStrings()
   '
   Function UTReplaceStrings(ByVal Source As String, ByVal rep As String, ByVal withString As String) As String
      Dim remainder As String
      Dim result As String
      Dim Index As Integer

      If Source = "" Or rep = "" Then
         UTReplaceStrings = Source
         Exit Function
      End If

      remainder = Source
      result = ""

      Do
         Index = InStr(remainder, rep)

         If Index < 1 Then
            result = result & remainder
            Exit Do
         End If

         result = result & Left(remainder, Index - 1) & withString
         remainder = Right(remainder, (Len(remainder) + 1) - (Index + Len(rep)))
      Loop

      UTReplaceStrings = result
   End Function


   '
   ' UTSort_Selection()
   '
   ' Sorts the items of the containter 'obj' on its field number 'fld'
   ' in the direction indicated by 'dir'.
   '
   ' -----------------
   ' PROTOCOL for OBJ:
   ' -----------------
   '
   ' 'obj' must consider its items to have 1-based indices.
   '
   '   Property Get Count() As Long
   '       Return the number of elements in the collection
   '
   '   Function CompareOnField(a As Long, b As Long, f As Long, d As Integer) As Integer
   '       Return True or False. Return True if items A and B should be
   '       swapped based on their values in field f and the sort order
   '       in d.
   '
   '   Sub Swap(a As Long, b As Long)
   '       Swap the items at a and b.
   '
   'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Sub UTSort_Selection(ByRef Obj As Object, ByRef Fld As Integer, ByRef Dir_Renamed As Integer)
      Dim Start As Integer
      Dim count As Integer
      Dim Index As Integer
      Dim I As Integer

      On Error GoTo UTS_SEL_ERROR

      Start = 1
      'UPGRADE_WARNING: Couldn't resolve default property of object Obj.count. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
      count = Obj.count

      While Start < count
         Index = Start

         For I = Start + 1 To count
            'UPGRADE_WARNING: Couldn't resolve default property of object Obj.CompareOnField. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            If Obj.CompareOnField(Index, I, Fld, Dir_Renamed) Then
               Index = I
            End If
         Next I

         If Index <> Start Then
            'UPGRADE_WARNING: Couldn't resolve default property of object Obj.swap. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            Obj.swap(Start, Index)
         End If

         Start = Start + 1
      End While

UTS_SEL_ERROR:
      Exit Sub
   End Sub

   '
   ' UTStringToBool()
   '
   Function UTStringToBool(ByVal Value As String) As Integer
      UTStringToBool = UTStringToBoolDefault(Value, False)
   End Function

   '
   ' UTStringToBoolDefault()
   '
   'UPGRADE_NOTE: DEFAULT was upgraded to DEFAULT_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Function UTStringToBoolDefault(ByVal Value As String, ByRef DEFAULT_Renamed As Integer) As Integer
      Dim temp As Integer

      On Error GoTo UTSTRINGTOBOOLDEFAULT_ERROR

      '
      ' Check for empty value:
      '

      Value = UCase(Trim(Value))

      If Value = "" Then
         If DEFAULT_Renamed Then
            temp = True
         Else
            temp = False
         End If

         UTStringToBoolDefault = temp
         Exit Function
      End If

      '
      ' Handle numeric case:
      '

      If IsNumeric(Value) Then
         If CInt(Value) = 0 Then
            temp = False
         Else
            temp = True
         End If

         UTStringToBoolDefault = temp
         Exit Function
      End If

      '
      ' Handle non-numeric cases:
      '

      Select Case Value
         Case "T", "TRUE", "Y", "YES", "ON"
            temp = True
         Case "F", "FALSE", "N", "NO", "OFF"
            temp = False
         Case Else
            If DEFAULT_Renamed Then
               temp = True
            Else
               temp = False
            End If
      End Select

      UTStringToBoolDefault = temp
      Exit Function

UTSTRINGTOBOOLDEFAULT_ERROR:  ' Handles any overflow from CInt()
      If DEFAULT_Renamed Then
         temp = True
      Else
         temp = False
      End If

      UTStringToBoolDefault = temp
      Exit Function
   End Function

   '
   ' UTStringToInteger()
   '
   Function UTStringToInteger(ByVal inString As String) As Integer
      UTStringToInteger = 0
      On Error Resume Next
      UTStringToInteger = CInt(inString)
   End Function

   '
   ' UTStringToWindowState()
   '
   'Function UTStringToWindowStateDefault(ByVal inString As String, ByVal def As Integer) As Integer
   '    Dim temp As String
   '    Dim ws As Integer
   '
   '    temp$ = Trim$(inString$)
   '    temp$ = UCase$(temp$)
   '
   '    If IsNumeric(temp$) Then
   '        ws% = CInt(temp$)
   '    Else
   '        Select Case temp$
   '            Case "N", "NORM", "NORMAL", "NORMALIZED"
   '                ws% = WS_NORMAL%
   '            Case "MIN", "MINIMUM", "MINIMIZED"
   '                ws% = WS_MINIMIZED%
   '            Case "MAX", "MAXIMUM", "MAXIMIZED"
   '                ws% = WS_MAXIMIZED%
   '            Case Else
   '                ws% = def%
   '        End Select
   '    End If
   '
   '    '
   '    ' Ensure it is a valid window state:
   '    '
   '    ' Invalid ones are mapped to WS_NORMAL%
   '    '
   '
   '    Select Case ws%
   '        Case WS_MINIMIZED%, WS_MAXIMIZED%
   '            '
   '            ' Do nothing:
   '            '
   '        Case Else
   '            ws% = WS_NORMAL%
   '    End Select
   '
   '    UTStringToWindowStateDefault% = ws%
   'End Function

   '
   ' UTTokenAt()
   '
   ' This function picks a token out of a string.
   '
   ' NOTE: "index" is zero-based.
   '
   ' See also UTTokenCount().
   '
   Function uttokenat(ByVal s As String, ByVal delim As String, ByRef I As Integer) As String
      Dim Index As Integer
      Dim tc As Integer
      Dim temp As String

      If I < 0 Or s = "" Or delim = "" Then
         uttokenat = ""
         Exit Function
      End If

      tc = 0

      Do
         Index = InStr(s, delim)

         If Index = 0 Then
            temp = s
         Else
            temp = Left(s, Index - 1)
            s = Right(s, Len(s) - (Index + Len(delim) - 1))
         End If

         If tc = I Then
            uttokenat = temp
            Exit Function
         End If

         tc = tc + 1

         If Index = 0 Then
            Exit Do
         End If
      Loop

      uttokenat = ""
   End Function

   '
   ' UTTokenCount()
   '
   ' This function treats "s" as a series of
   ' tokens separated by delimiters. It returns the
   ' number of tokens in the string.
   '
   ' An empty string has one (empty) token.
   '
   ' See also UTTokenAt().
   '
   Function uttokencount(ByVal s As String, ByVal delim As String) As Integer
      Dim Index As Integer
      Dim tc As Integer

      '
      ' Take care of trivial cases:
      '

      If s = "" Then
         uttokencount = 1
         Exit Function
      End If

      If delim = "" Then
         uttokencount = Len(s)
         Exit Function
      End If

      '
      ' Count the tokens:
      '

      tc = 0

      Do
         tc = tc + 1

         Index = InStr(s, delim)

         If Index = 0 Then
            Exit Do
         Else
            s = Right(s, Len(s) - (Index + Len(delim) - 1))
         End If
      Loop

      uttokencount = tc
   End Function

   '
   ' UTTokenize()
   '
   ' An empty string has one (empty) token.
   '
   Function UTTokenize(ByVal s As String, ByVal delim As String) As Integer
      Dim Index As Integer
      Dim temp As String

      '
      ' Empty the tokens array:
      '
      ReDim gUTTokens(0)
      gUTTokenCount = 0
      '
      ' Scan the string:
      '
      Do
         '
         ' Look for a token:
         '
         If Len(s) < Len(delim) Then
            Index = 0
            temp = s
         Else
            Index = InStr(s, delim)

            If Index = 0 Then
               temp = s
            Else
               temp = Left(s, Index - 1)
               s = Right(s, Len(s) - (Index + Len(delim) - 1))
            End If
         End If


         '
         ' Add the token to the tokens() array:
         '

         gUTTokenCount = gUTTokenCount + 1
         ReDim Preserve gUTTokens(gUTTokenCount - 1)
         gUTTokens(gUTTokenCount - 1) = temp

         If Index = 0 Then
            Exit Do
         End If
      Loop

      UTTokenize = gUTTokenCount
   End Function

   '
   ' UTTokenize2D()
   '
   'Sub UTTokenize2D(ByVal s As String, ByVal rowDelim As String, ByVal colDelim As String, ByRef RowCount As Integer, ByRef ColCount As Integer)
   '	Dim rows() As String
   '	Dim I As Integer
   '	Dim J As Integer
   '	Dim tk As Integer
   '	Dim Value As String

   '	'
   '	' Split out the rows:
   '	'

   '	RowCount = UTTokenize(s, rowDelim)

   '	ReDim rows(RowCount - 1)

   '	For I = 0 To RowCount - 1
   '		rows(I) = gUTTokens(I)
   '	Next I

   '	'
   '	' If no rows, then stop:
   '	'

   '	If RowCount < 1 Then
   '		ReDim gUTTokens2D(0, 0)
   '		ColCount = 0
   '		Exit Sub
   '	End If

   '	'
   '	' Determine the column count from the row with the most columns
   '	' and redim gTokens2D:
   '	'

   '	For I = 0 To RowCount - 1
   '		tk = UTTokenize(rows(I), colDelim)

   '		If tk > ColCount Then
   '			ColCount = tk
   '		End If
   '	Next I

   '	ReDim gUTTokens2D(RowCount - 1, ColCount - 1)

   '	'
   '	' Parse the data out of the rows and into gTokens2D:
   '	'
   '	' If the row has too few tokens, substitute empty strings
   '	' in gTokens2D for those missing from the row.
   '	'

   '	For I = 0 To RowCount - 1
   '		tk = UTTokenize(rows(I), colDelim)

   '		For J = 0 To ColCount - 1
   '			If J < tk Then
   '				Value = gUTTokens(J)
   '			Else
   '				Value = ""
   '			End If

   '			gUTTokens2D(I, J) = Value
   '		Next J
   '	Next I
   'End Sub

   Function UTTokenizeToArray(ByVal s As String, ByVal delim As String, ByRef myArray() As String) As Integer
      Dim Index As Integer
      Dim temp As String

      '
      ' Empty the tokens array:
      '

      ReDim myArray(0)
      gUTTokenCount = 0

      '
      ' Scan the string:
      '

      Do
         '
         ' Look for a token:
         '

         If Len(s) < Len(delim) Then
            Index = 0
            temp = s
         Else
            Index = InStr(s, delim)

            If Index = 0 Then
               temp = s
            Else
               temp = Left(s, Index - 1)
               s = Right(s, Len(s) - (Index + Len(delim) - 1))
            End If
         End If


         '
         ' Add the token to the tokens() array:
         '

         gUTTokenCount = gUTTokenCount + 1
         ReDim Preserve myArray(gUTTokenCount - 1)
         myArray(gUTTokenCount - 1) = temp

         If Index = 0 Then
            Exit Do
         End If
      Loop

      UTTokenizeToArray = gUTTokenCount


   End Function

   '
   ' UTUntokenize_2()
   '
   Function UTUntokenize_2(ByRef inTokens() As String, ByVal delim As String) As String
      Dim count As Integer
      Dim result As String
      Dim I As Integer

      On Error GoTo UTUNTOKENIZE_2_ERROR

      result = ""

      count = UBound(inTokens) - LBound(inTokens) + 1

      For I = 0 To count - 1
         If I <> 0 Then
            result = result & delim
         End If

         result = result & inTokens(I)
      Next I

UTUNTOKENIZE_2_ERROR:
      UTUntokenize_2 = result
      Exit Function
   End Function

   '
   ' UTUntokenize2D()
   '
   '	Function UTUntokenize2D(ByVal rowDelim As String, ByVal colDelim As String) As String
   '		Dim RowCount As Integer
   '		Dim columnCount As Integer
   '		Dim result As String
   '		Dim I As Integer
   '		Dim J As Integer

   '		On Error GoTo UNTOKENIZE2D_ERROR

   '		result = ""
   '		RowCount = UBound(gUTTokens2D, 1) + 1
   '		columnCount = UBound(gUTTokens2D, 2) + 1

   '		For I = 0 To RowCount - 1
   '			For J = 0 To columnCount - 1
   '				result = result & gUTTokens2D(I, J)

   '				If J <> columnCount - 1 Then
   '					result = result & colDelim
   '				End If
   '			Next J

   '			If I <> RowCount - 1 Then
   '				result = result & rowDelim
   '			End If
   '		Next I

   'UNTOKENIZE2D_ERROR: 
   '		UTUntokenize2D = result
   '		Exit Function
   '	End Function

   '
   ' UTWindowStateToString()
   '
   'Function UTWindowStateToString(ByVal inInt As Integer) As String
   '    Dim out As String
   '
   '    Select Case inInt%
   '        Case WS_NORMAL%
   '            out$ = "NORM"
   '        Case WS_MINIMIZED%
   '            out$ = "MIN"
   '        Case WS_MAXIMIZED%
   '            out$ = "MAX"
   '        Case Else
   '            out$ = "NORM"
   '    End Select
   '
   '    UTWindowStateToString$ = out$
   'End Function



   '	Public Function UTIsFormUp(ByRef sFormName As String) As Boolean
   '		On Error GoTo lblErr
   '		Dim I As Object ' Declare variable.

   '		'UPGRADE_ISSUE: Forms collection was not upgraded. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2068"'
   '		'UPGRADE_WARNING: Couldn't resolve default property of object Forms.count. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '		For I = 0 To Forms.count - 1
   '			'UPGRADE_ISSUE: Forms collection was not upgraded. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2068"'
   '			'UPGRADE_WARNING: Couldn't resolve default property of object Forms(I).Tag. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
   '			If Forms(I).Tag = sFormName Then
   '				UTIsFormUp = True
   '			End If
   '		Next I

   '		Exit Function

   'lblErr: 

   '		MsgBox(Err.Description, MsgBoxStyle.Exclamation, "ModUT : UTIsFormUp")

   '	End Function

   Public Sub ShowList()
   End Sub

   Public Sub UTHideOtherForms(ByRef sFormName As String)
   End Sub

   Public Sub UTUnloadOtherForms()
   End Sub

   'Search the global field table for field number.  If not found, return -1
   'else return field number
   Public Function UTGetFldNum(ByRef strName As String) As Integer
      Dim iNum As Integer

      UTGetFldNum = -1
      For iNum = 0 To g_iFields - 1
         If g_aFields(iNum).Tag = strName Then
            UTGetFldNum = iNum
            Exit For
         End If
      Next
   End Function

   Public Function GetAssessor() As String
      Dim strTemp1 As String
      Dim strTemp2 As String

      strTemp1 = New String(" ", 128)
      strTemp2 = New String(" ", 128)
      Call IniFileUtil.GetPrivateProfileString("Data", "County", "", strTemp1, 127, g_sProjectFile)
      Call IniFileUtil.GetPrivateProfileString("Data", "Assessor", "", strTemp2, 127, g_sProjectFile)
      If InStr(strTemp1, Chr(0)) > 0 Then
         strTemp1 = Trim(strTemp1)
         strTemp2 = Trim(strTemp2)
         strTemp1 = Left(strTemp1, Len(strTemp1) - 1)
         strTemp2 = Left(strTemp2, Len(strTemp2) - 1)
      End If

      strTemp1 = strTemp1 & " - " & strTemp2
      GetAssessor = strTemp1
   End Function

   Public Function StripNull(ByVal strInput As String) As String
      Dim strTemp As String
      Dim lTmp As Integer

      lTmp = InStr(strInput, Chr(0))
      If lTmp > 0 Then
         strTemp = Trim(strInput)
         StripNull = Left(strTemp, lTmp - 1)
      Else
         StripNull = strInput
      End If

   End Function
End Module