Option Strict Off
Option Explicit On
Imports System.IO
Friend Class clsCompress

   Public Function compress() As Integer
      Dim zUbound As Integer
      Dim lInterval As Integer
      Dim sTemp As String
      Dim lRecords As Integer
      Dim bGoodData As Boolean
      Dim zHndlMaster As Integer
      Dim lMasterPosition As Integer
      Dim iCurFile As Integer
      Dim iX As Integer
      Dim iRet As Integer
      Dim aFields() As Integer
      Dim objArrayWrite As New clsWriteLong
      Dim objFieldExtraction As New clsFieldExtraction
      Dim objStringWrite As New clsWriteString

      iRet = -1
      On Error GoTo lbl_ErrorCompress

      zHndlMaster = FreeFile()
      FileOpen(zHndlMaster, g_sWorkDir & g_sMasterRecordFile, OpenMode.Binary)

      objStringWrite.OpenFile(g_sWorkDir & "TempFile")

      zUbound = g_iDisplayFields - 1
      ReDim aFields(g_iDisplayFields)

      'Setup progress window
      g_bStopProcess = False
      iCurFile = 1
      lInterval = getInterval()

ReExtract:
      objFieldExtraction.OpenFile(g_sRawFile)
      sTemp = ""
      Do
         For iX = 0 To zUbound
            sTemp = sTemp & Trim(objFieldExtraction.fieldGetFixed(g_aFields(g_aDisplayFields(iX)).FieldOffSet, g_aFields(g_aDisplayFields(iX)).FieldLength)) & sINDEX_DELIMITER
         Next
         sTemp = sTemp & sINDEX_SEPARATOR
         lMasterPosition = objStringWrite.lLogicalSeek

         'Write record location
         FilePut(zHndlMaster, lMasterPosition)
         'Write data
         objStringWrite.stringWrite((sTemp))

         sTemp = ""
         bGoodData = objFieldExtraction.nextRecord

         'Check for more than one raw files
         If Not bGoodData And g_iRawFile > iCurFile Then
            bGoodData = True
            objFieldExtraction.closeFile()
            iCurFile = iCurFile + 1
            g_sRawFile = Left(g_sRawFile, Len(g_sRawFile) - 2) & Format(iCurFile, "00")
            GoTo ReExtract
         End If

         lRecords = lRecords + 1
      Loop Until Not bGoodData Or lRecords >= g_lRecords

      objStringWrite.CloseFile()
      objStringWrite = Nothing
      objFieldExtraction.CloseFile()
      objFieldExtraction = Nothing

      'Close master file
      FileClose(zHndlMaster)

      sTemp = g_sWorkDir & g_sPrefix & ".CMP"

      'Remove existing file first
      If File.Exists(sTemp) Then
         Kill((sTemp))
      End If

      Rename(g_sWorkDir & "TempFile", sTemp)
      g_sRawFile = sTemp

      Dim objFileAction As New clsFileAction
      If Not g_bStopProcess Then
         'objFileAction.saveAll
         objFileAction.SaveSettings()
         objFileAction.setCompression((True))
      End If

      iRet = 0
      GoTo lblExitCompress

lbl_ErrorCompress:
      LogMsg("***** Error in clsCompress!compress: " & Err.Description)

lblExitCompress:
      compress = iRet
   End Function
End Class