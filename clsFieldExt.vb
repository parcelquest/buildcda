Option Strict Off
Option Explicit On
'Imports System.IO.FileStream

Friend Class clsFieldExtraction

   Public BIND_BUFFER_SIZE As Integer
   Public lApproxRecords As Integer
   Public lLogicalSeek As Integer
   Public lEOF As Integer

   Dim m_lCurrentPos As Integer
   Dim m_sBuffer As String
   Dim m_zHndl As Integer
   Dim m_lLenBuffer As Integer
   Dim m_zHndlMaster As Integer

   Public Function OpenFile(ByRef szFileName As String) As Integer
      Dim iRet As Integer

      'open the file
      m_zHndl = FreeFile()
      'Open pathname For mode [Access access] [lock] As [#]filenumber [Len=reclength]
      FileOpen(m_zHndl, szFileName, OpenMode.Binary, OpenAccess.Read, OpenShare.LockWrite)

      lEOF = LOF(m_zHndl)
      If lEOF > 0 Then
         iRet = 1
      Else
         iRet = 0
      End If

      'fill the buffer - BIND_BUFFER_SIZE = gc_MAXRECS * g_iRecLen
      m_sBuffer = New String(" ", BIND_BUFFER_SIZE)
      Call FillBuffer()
      If Left(m_sBuffer, 8) = "99999999" Then
         Call nextRecord()
      End If

      OpenFile = iRet
      Exit Function

lblErr:
      LogMsg("clsFieldExtraction!OpenFile" & Err.Description)
      OpenFile = -1
   End Function

   Private Sub FillBuffer()
      Dim lCurrentSeek As Integer
      Dim lSeekOffset As Integer

      If m_lCurrentPos = 0 Then
         lSeekOffset = 0
      Else
         lSeekOffset = (Len(m_sBuffer) - (m_lCurrentPos))
      End If

      Try
         lCurrentSeek = Seek(m_zHndl) - lSeekOffset
         m_lCurrentPos = 0

         If Not EOF(m_zHndl) Then
            If lCurrentSeek + BIND_BUFFER_SIZE - 1 > lEOF Then
               m_sBuffer = New String(" ", lEOF - (lCurrentSeek - 1))
            End If

            FileGet(m_zHndl, m_sBuffer, lCurrentSeek)
            lLogicalSeek = lCurrentSeek
            m_lLenBuffer = Len(m_sBuffer)
         Else
            lLogicalSeek = lCurrentSeek
            m_lLenBuffer = 0
         End If
      Catch ex As Exception
         LogMsg("clsFieldExtraction!FillBuffer" & ex.Message())
      End Try
   End Sub

   Public Function fieldGetFixed(ByRef zOffset As Integer, ByRef zLen As Integer) As String
      Dim lStartPos As Integer
      Dim lEndpos As Integer

      lStartPos = m_lCurrentPos + zOffset
      lEndpos = m_lCurrentPos + zOffset + zLen
      fieldGetFixed = ""

      If lEndpos - 1 > m_lLenBuffer Then
         Exit Function
      End If

      If lEndpos - 1 <= m_lLenBuffer Then
         fieldGetFixed = Mid(m_sBuffer, lStartPos, zLen)
      Else
         If lStartPos <= m_lLenBuffer Then
            fieldGetFixed = Mid(m_sBuffer, lStartPos, m_lLenBuffer - (m_lCurrentPos))
         End If
      End If
   End Function

   Public Function nextRecord() As Boolean
      If m_lCurrentPos + g_iRecLen + g_iRecLen > m_lLenBuffer Then
         Call FillBuffer()
      End If

      If m_lCurrentPos + g_iRecLen + g_iRecLen > m_lLenBuffer Then
         nextRecord = False
      Else
         m_lCurrentPos = m_lCurrentPos + g_iRecLen
         lLogicalSeek = lLogicalSeek + g_iRecLen
         nextRecord = True
      End If
   End Function

   Public Sub CloseFile()
      If m_zHndl > 0 Then
         FileClose(m_zHndl)
         m_sBuffer = ""
         m_zHndl = 0
      End If
   End Sub

   Public Sub GotoPos(ByRef lPos As Integer)
      Seek(m_zHndl, lPos)

      m_lCurrentPos = 0
      lLogicalSeek = lPos
      m_sBuffer = New String(" ", BIND_BUFFER_SIZE)
      Call FillBuffer()
   End Sub

   Public Sub New()
      MyBase.New()
      BIND_BUFFER_SIZE = gc_MAXRECS * g_iRecLen
   End Sub
End Class