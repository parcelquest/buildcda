Option Strict Off
Option Explicit On
Friend Class clsWriteBuffer

   Dim m_alLongBuffer() As Integer
   Dim m_alLongOffsets() As Integer
   Dim m_alLongBounds() As Integer
   Dim m_alLongCurrentPos() As Integer 'current position in the buffer - ZERO BASED!!

   Dim m_lLongArraySize As Integer
   Dim m_alLongTempBuffer() As Integer

   Dim m_lSingleArraySize As Integer
   Dim m_alSingleBuffer() As Single

   Dim m_alSingleOffsets() As Single
   Dim m_alSingleBounds() As Single
   Dim m_alSingleCurrentPos() As Single 'current position in the buffer - ZERO BASED!!
   Dim m_alSingleTempBuffer() As Single

   Public ArrayBufferSize As Integer

   Dim m_zHndl() As Integer
   Dim m_zTypes() As Integer
   Dim m_sBuffer As String
   Dim m_asBuffers() As String
   Public BufferSize As Integer

   Public Function longTermWrite(ByRef sTerm As String, ByRef lRecordNumber As Integer, ByRef iID As Integer) As Boolean
      Dim lX As Integer
      Dim lTempBufferX As Integer
      Dim lNextPos As Integer
      Dim lTempTerm As UInteger

      longTermWrite = False

      If Trim(sTerm) = "" Then
         'lTempTerm = g_cNULLLONG
         '5/23/99 to fix item #17
         lTempTerm = 0
      Else
         Try
            lTempTerm = CUInt(sTerm)
         Catch ex As Exception
            LogMsg("*** Invalid number: " & sTerm & " RecNum=" & lRecordNumber & " FldID=" & iID)
            lTempTerm = 0
         End Try
         If lTempTerm > 2147483647 Then
            lTempTerm = 0
         End If
      End If

      If m_alLongCurrentPos(iID) = m_alLongBounds(iID) + 1 Then
         m_lLongArraySize = m_lLongArraySize + ((UBound(m_alLongTempBuffer) + 1) / 2)
         For lX = m_alLongOffsets(iID) To m_alLongCurrentPos(iID) - 1
            m_alLongTempBuffer(lTempBufferX) = m_alLongBuffer(lX)
            lTempBufferX = lTempBufferX + 1
         Next

         Try
            FilePut(m_zHndl(iID), m_alLongTempBuffer)
         Catch ex As Exception
            LogMsg("***** Write error om longTermWrite: " & ex.Message)
         End Try
         m_alLongCurrentPos(iID) = m_alLongOffsets(iID)

      ElseIf lNextPos > m_alLongBounds(iID) Then
         LogMsg("!!!!! Bad Data - Please call Sony Nguyen !!!!!")
      End If

      'Move data to buffer
      m_alLongBuffer(m_alLongCurrentPos(iID)) = lTempTerm
      m_alLongBuffer(m_alLongCurrentPos(iID) + 1) = lRecordNumber
      m_alLongCurrentPos(iID) = m_alLongCurrentPos(iID) + 2
      longTermWrite = True

   End Function

   Public Function SingleTermWrite(ByRef sTerm As String, ByRef lRecordNumber As Integer, ByRef iID As Integer) As Boolean
      On Error GoTo lblErr2

      Dim lX As Integer
      Dim lTempBufferX As Single
      Dim lNextPos As Integer
      Dim lTempTerm As Single

      If Trim(sTerm) = "" Then
         lTempTerm = g_cNULLFLOAT
      Else
         lTempTerm = CSng(sTerm)
      End If

      If m_alSingleCurrentPos(iID) = m_alSingleBounds(iID) + 1 Then
         m_lSingleArraySize = m_lSingleArraySize + ((UBound(m_alSingleTempBuffer) + 1) / 2)
         For lX = m_alSingleOffsets(iID) To m_alSingleCurrentPos(iID) - 1
            m_alSingleTempBuffer(lTempBufferX) = m_alSingleBuffer(lX)
            lTempBufferX = lTempBufferX + 1
         Next


         FilePut(m_zHndl(iID), m_alSingleTempBuffer)
         m_alSingleCurrentPos(iID) = m_alSingleOffsets(iID)
      ElseIf lNextPos > m_alSingleBounds(iID) Then
         LogMsg("oh shit")
      End If

      m_alSingleBuffer(m_alSingleCurrentPos(iID)) = lTempTerm
      m_alSingleBuffer(m_alSingleCurrentPos(iID) + 1) = lRecordNumber
      m_alSingleCurrentPos(iID) = m_alSingleCurrentPos(iID) + 2

      SingleTermWrite = True

      Exit Function

lblErr2:
      LogMsg("clsWriteBuffer!SingleTermWrite" & Err.Description)
      '   MsgBox Err.Description, vbExclamation, "clsWriteBuffer : SingleTermWrite"
      SingleTermWrite = False
   End Function

   Public Function longOpenFile(ByRef szFileName As String, ByRef iID As Integer) As Boolean
      On Error GoTo lblErr3

      Dim lTempLbound As Integer
      Dim lTempUBound As Integer

      If Dir(szFileName) <> "" Then
         Kill((szFileName))
      End If

      'open the file #3
      m_zHndl(iID) = UTFreeFile()
      FileOpen(m_zHndl(iID), szFileName, OpenMode.Binary)
      If UBound(m_alLongBuffer) = 0 Then
         lTempLbound = 0
      Else
         lTempLbound = UBound(m_alLongBuffer) + 1
      End If
      lTempUBound = lTempLbound + ((ArrayBufferSize * 2) - 1)

      ReDim Preserve m_alLongCurrentPos(iID)
      ReDim Preserve m_alLongOffsets(iID)
      ReDim Preserve m_alLongBounds(iID)
      ReDim m_alLongBuffer(lTempUBound)

      m_alLongOffsets(iID) = lTempLbound
      m_alLongCurrentPos(iID) = lTempLbound
      m_alLongBounds(iID) = lTempUBound

      longOpenFile = True
      Exit Function

lblErr3:
      LogMsg("clsWriteBuffer!longOpenFile" & Err.Description)
      '    MsgBox Err.Description, vbExclamation, "clsWriteString : longOpenFile " & szFileName
   End Function

   Public Function SingleOpenFile(ByRef szFileName As String, ByRef iID As Integer) As Boolean
      On Error GoTo lblErr4

      Dim lTempLbound As Integer
      Dim lTempUBound As Integer

      If Dir(szFileName) <> "" Then
         Kill((szFileName))
      End If

      'open the file #4
      m_zHndl(iID) = UTFreeFile()
      FileOpen(m_zHndl(iID), szFileName, OpenMode.Binary)

      If UBound(m_alSingleBuffer) = 0 Then
         lTempLbound = UBound(m_alSingleBuffer)
         lTempUBound = lTempLbound + ((ArrayBufferSize * 2) - 1)
      Else
         lTempLbound = UBound(m_alSingleBuffer) + 1
         lTempUBound = lTempLbound + ((ArrayBufferSize * 2) - 1)
      End If

      ReDim Preserve m_alSingleCurrentPos(iID)
      ReDim Preserve m_alSingleOffsets(iID)
      ReDim Preserve m_alSingleBounds(iID)

      m_alSingleOffsets(iID) = lTempLbound
      m_alSingleCurrentPos(iID) = lTempLbound

      ReDim m_alSingleBuffer(lTempUBound)
      m_alSingleBounds(iID) = lTempUBound

      SingleOpenFile = True
      Exit Function

lblErr4:
      LogMsg("clsWriteBuffer!singleOpenFile" & Err.Description)
      '    MsgBox Err.Description, vbExclamation, "clsWriteString : OpenFile"
   End Function

   Public Sub New()
      MyBase.New()
      ArrayBufferSize = OUT_BUFFER_SIZE_LONG_EXTRACTION
      BufferSize = OUT_BUFFER_SIZE_STRING_EXTRACTION

      ReDim m_zHndl(0)
      ReDim m_zTypes(0)

      ReDim m_asBuffers(0)

      ReDim m_alLongBuffer(0)
      ReDim m_alLongOffsets(0)
      ReDim m_alLongBounds(0)
      ReDim m_alLongCurrentPos(0)
      ReDim m_alLongTempBuffer((ArrayBufferSize * 2) - 1)

      ReDim m_alSingleBuffer(0)
      ReDim m_alSingleOffsets(0)
      ReDim m_alSingleBounds(0)
      ReDim m_alSingleCurrentPos(0)
      ReDim m_alSingleTempBuffer((ArrayBufferSize * 2) - 1)
   End Sub

   Public Sub CloseFile()
      On Error GoTo lblErr5
      Dim iX As Integer

      Dim lX As Integer
      Dim lTempBufferX As Integer
      Dim sfTempBufferX As Single

      For iX = 0 To UBound(m_asBuffers)
         If (m_zTypes(iX) = iFIELD_TYPE_STRING Or m_zTypes(iX) = iFIELD_TYPE_WORD) Then
            If m_asBuffers(iX) <> "" Then FilePut(m_zHndl(iX), m_asBuffers(iX))
         ElseIf m_zTypes(iX) = iFIELD_TYPE_LONG Or m_zTypes(iX) = iFIELD_TYPE_LJNUM Then
            If m_alLongCurrentPos(iX) > m_alLongOffsets(iX) Then
               lTempBufferX = 0
               ReDim m_alLongTempBuffer((m_alLongCurrentPos(iX) - 1) - m_alLongOffsets(iX))
               m_lLongArraySize = m_lLongArraySize + ((UBound(m_alLongTempBuffer) + 1) / 2)
               For lX = m_alLongOffsets(iX) To m_alLongCurrentPos(iX) - 1
                  m_alLongTempBuffer(lTempBufferX) = m_alLongBuffer(lX)
                  lTempBufferX = lTempBufferX + 1
               Next

               FilePut(m_zHndl(iX), m_alLongTempBuffer)
            End If
         ElseIf m_zTypes(iX) = iFIELD_TYPE_FLOAT Then
            If m_alSingleCurrentPos(iX) > m_alSingleOffsets(iX) Then
               sfTempBufferX = 0
               ReDim m_alSingleTempBuffer((m_alSingleCurrentPos(iX) - 1) - m_alSingleOffsets(iX))
               m_lSingleArraySize = m_lSingleArraySize + ((UBound(m_alSingleTempBuffer) + 1) / 2)
               For lX = m_alSingleOffsets(iX) To m_alSingleCurrentPos(iX) - 1
                  m_alSingleTempBuffer(sfTempBufferX) = m_alSingleBuffer(lX)
                  sfTempBufferX = sfTempBufferX + 1
               Next

               FilePut(m_zHndl(iX), m_alSingleTempBuffer)
            End If
         End If

         'Close file handle doesn't matter what
         FileClose(m_zHndl(iX))

         'Free up file handle
         If m_zLastFreeFile > m_zHndl(iX) Then m_zLastFreeFile = m_zHndl(iX)
      Next

lblErr5:
      'Need to put specific message here to handle different error.
   End Sub

   Protected Overrides Sub Finalize()
      CloseFile()
      MyBase.Finalize()
   End Sub

   Public Function strOpenFile(ByRef szFileName As String, ByRef iID As Integer) As Boolean
      On Error GoTo lblErr6

      If Dir(szFileName) <> "" Then
         Kill((szFileName))
      End If

      'open the file #2
      m_zHndl(iID) = UTFreeFile()
      FileOpen(m_zHndl(iID), szFileName, OpenMode.Binary)

      'empty the buffer
      m_asBuffers(iID) = ""
      strOpenFile = True
      Exit Function

lblErr6:
      strOpenFile = False
      LogMsg("clsWriteBuffer!strOpenFile" & Err.Description)
      '   MsgBox Err.Description, vbExclamation, "clsWriteString : strOpenFile " & szFileName
   End Function

   Public Function strTermWrite(ByRef sTerm As String, ByRef lRecordNumber As Integer, ByRef iID As Integer) As Boolean
      Dim sTemp As String

      sTemp = Trim(sTerm)

      If sTemp = "" Then
         sTemp = " "
      End If

      Call stringWrite(sTemp & sINDEX_DELIMITER & CStr(lRecordNumber) & vbCrLf, iID)

      strTermWrite = True
   End Function

   Public Sub stringWrite(ByRef sNewString As String, ByRef iID As Integer)
      On Error GoTo lblErr7

      If Len(m_asBuffers(iID)) + Len(sNewString) > BufferSize Then
         FilePut(m_zHndl(iID), m_asBuffers(iID))
         m_asBuffers(iID) = sNewString
      Else
         m_asBuffers(iID) = m_asBuffers(iID) & sNewString
      End If

      Exit Sub

lblErr7:
      LogMsg("clsWriteBuffer!StringWrite" & Err.Description)
      '   MsgBox Err.Description, vbExclamation, "clsWriteBuffer : StringWrite"
   End Sub

   Public Function OpenFile(ByRef szFileName As String, ByRef iType As Integer, ByRef iID As Integer) As Boolean
      On Error GoTo lblErr8

      If UBound(m_zHndl) < iID Then
         ReDim Preserve m_zHndl(iID)
         ReDim m_asBuffers(iID)
         ReDim Preserve m_zTypes(iID)
      End If

      Select Case iType
         Case iFIELD_TYPE_STRING
            m_zTypes(iID) = iFIELD_TYPE_STRING
            OpenFile = strOpenFile(szFileName, iID)

         Case iFIELD_TYPE_LONG, iFIELD_TYPE_LJNUM
            m_zTypes(iID) = iFIELD_TYPE_LONG
            OpenFile = longOpenFile(szFileName, iID)

         Case iFIELD_TYPE_WORD
            m_zTypes(iID) = iFIELD_TYPE_WORD
            OpenFile = strOpenFile(szFileName, iID)

         Case iFIELD_TYPE_FLOAT
            m_zTypes(iID) = iFIELD_TYPE_FLOAT
            OpenFile = SingleOpenFile(szFileName, iID)
      End Select
lblErr8:
   End Function

   Public Function termWrite(ByRef sTerm As String, ByRef lRecordNumber As Integer, ByRef iID As Integer) As Boolean
      Select Case m_zTypes(iID)
         Case iFIELD_TYPE_STRING
            termWrite = strTermWrite(sTerm, lRecordNumber, iID)

         Case iFIELD_TYPE_LONG, iFIELD_TYPE_LJNUM
            termWrite = longTermWrite(sTerm, lRecordNumber, iID)

         Case iFIELD_TYPE_WORD
            termWrite = wordTermWrite(sTerm, lRecordNumber, iID)

         Case iFIELD_TYPE_FLOAT
            termWrite = SingleTermWrite(sTerm, lRecordNumber, iID)
      End Select
   End Function

   Public Function wordTermWrite(ByRef sTerm As String, ByRef lRecordNumber As Integer, ByRef iID As Integer) As Boolean
      '5 secs just to call
      Dim sTemp As String
      Dim sNewTerm As String
      Dim iLen As Integer
      Dim iCurrentPos As Integer
      Dim iLastPos As Integer
      Dim iChar As Integer

      On Error GoTo lblErr9
      ' Static lCount As Long
      '2 secs # 75
      sTemp = Trim(sTerm)
      iLen = Len(sTemp)

      If sTemp = "" Then
         sTemp = " "
         Call stringWrite(sTemp & sINDEX_DELIMITER & CStr(lRecordNumber) & vbCrLf, iID)
         wordTermWrite = True
         g_aFields(iID).Terms = g_aFields(iID).Terms + 1

         Exit Function
      End If

      iCurrentPos = InStr(1, sTemp, " ")
      iLastPos = InStr(1, sTemp, ",")
      If iLastPos > 0 And iLastPos < iCurrentPos Then
         iCurrentPos = iLastPos
      End If

      iLastPos = 1
      If iCurrentPos <= 0 Then
         Call stringWrite(sTemp & sINDEX_DELIMITER & CStr(lRecordNumber) & vbCrLf, iID)
         g_aFields(iID).Terms = g_aFields(iID).Terms + 1
         Exit Function
      End If

      Do Until iCurrentPos >= iLen
         iChar = Asc(Mid(sTemp, iLastPos, 1))
         Do Until iChar <> MEKeySpace And iChar <> MEKeyComma
            iLastPos = iLastPos + 1
            If iLastPos > iLen Then GoTo lblExit
            iChar = Asc(Mid(sTemp, iLastPos, 1))
         Loop

         iCurrentPos = iLastPos

         iChar = Asc(Mid(sTemp, iCurrentPos, 1))
         Do Until iChar = MEKeySpace Or iChar = MEKeyComma
            iCurrentPos = iCurrentPos + 1
            If iCurrentPos > iLen Then Exit Do
            iChar = Asc(Mid(sTemp, iCurrentPos, 1))
         Loop

         'to do - greater than one space between words
         sNewTerm = Mid(sTemp, iLastPos, iCurrentPos - iLastPos)
         Call stringWrite(sNewTerm & sINDEX_DELIMITER & CStr(lRecordNumber) & vbCrLf, iID)
         iLastPos = iCurrentPos '+ 1
         g_aFields(iID).Terms = g_aFields(iID).Terms + 1
      Loop

lblExit:
      wordTermWrite = True
      Exit Function

lblErr9:
      LogMsg("clsWriteBuffer!wordTermWrite" & Err.Description)
      '   MsgBox Err.Description, vbExclamation, "clsWriteBuffer : wordTermWrite"
      wordTermWrite = False
   End Function
End Class