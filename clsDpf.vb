﻿Option Strict Off
Option Explicit On
Friend Class clsDpf
   Private Const FLD_ELEMENTS As Integer = 18
   Private Const FLD_ID As Integer = 0
   Private Const FLD_TAG As Integer = 1
   Private Const FLD_TYPE As Integer = 2
   Private Const FLD_OFFSET As Integer = 3
   Private Const FLD_LENGTH As Integer = 4
   Private Const FLD_INDEXED As Integer = 5
   Private Const FLD_SORTED As Integer = 6
   Private Const FLD_ARRAYLEN As Integer = 7
   Private Const FLD_TERMS As Integer = 8
   Private Const FLD_UTERMS As Integer = 9
   Private Const FLD_GROUP As Integer = 10
   Private Const FLD_INDEX As Integer = 11
   Private Const FLD_DISPLAY As Integer = 12
   Private Const FLD_TERMFILE As Integer = 13
   Private Const FLD_NDXFILE As Integer = 14
   Private Const FLD_RECFILE As Integer = 15
   Private Const FLD_WORDFILE As Integer = 16
   Private Const FLD_MEMBEROF As Integer = 17

   Dim fhDpf As Integer
   Dim asDpfRec(256) As DPFREC
   Dim sIniFile, sErrMsg As String

   Public m_iRecLen, m_iNumFlds As Integer

   Public Function GetLastErrorMsg() As String
      GetLastErrorMsg = sErrMsg
   End Function

   'Find field desc by ID
   Public Function GetFldDef(ByRef pField As DPFREC, ByVal iID As Integer) As Integer
   End Function

   Public Function GetValueStr(ByRef sSection As String, ByRef sItem As String) As String
      GetValueStr = ""
   End Function

   Public Function GetValueInt(ByRef sSection As String, ByRef sItem As String) As Integer
   End Function

   Public Function LoadDpf(ByVal sProjFile As String) As Integer
      Dim iRet, iCnt, fhDpf As Integer
      Dim strTemp, strRec As String
      Dim objINI As New clsINIWrapper
      Dim asItems As String() = Nothing

      LoadDpf = -1
      fhDpf = 0
      LogMsg("Loading DPF file: " & sProjFile)
      sIniFile = sProjFile

      'Initialize globals
      strTemp = ""
      Call objINI.valueRead("Data", "RecordLength", strTemp, sIniFile)
      If strTemp <> "" Then
         g_iRecLen = CInt(strTemp)
      Else
         sErrMsg = "Missing RecordLength in project file: " & sIniFile
         GoTo LoadDpf_Err
      End If

      Call objINI.valueRead("Data", "RecordNumber", strTemp, sIniFile)
      If strTemp <> "" Then
         m_iNumFlds = CInt(strTemp)
      Else
         sErrMsg = "Missing RecordNumber in project file: " & sIniFile
         GoTo LoadDpf_Err
      End If

      g_iFields = m_iNumFlds
      g_iDisplayFields = 0
      g_iSearchFields = 0
      g_iExtractFields = 0

      'Skip to [RecordDef] section
      fhDpf = FreeFile()
      FileOpen(fhDpf, sProjFile, OpenMode.Input)
      While Not EOF(fhDpf)
         strTemp = LineInput(fhDpf)
         If Left(strTemp, 11) = "[RecordDef]" Then Exit While
      End While
      If EOF(fhDpf) Then
         sErrMsg = "Missing [RecordDef] section in DPF file"
         GoTo LoadDpf_Err
      End If

      ReDim g_aFields(m_iNumFlds)

      'Fill up field array
      For iCnt = 0 To m_iNumFlds - 1
         strTemp = LineInput(fhDpf)
         iRet = strTemp.IndexOf("=")
         If iRet > 1 Then
            strRec = strTemp.Substring(iRet + 1)
         Else
            Continue For
         End If
         'iRet = ParseStr(strRec, ";", asItems)
         asItems = strRec.Split(";")
         iRet = asItems.Length
         If iRet < FLD_ELEMENTS Then
            sErrMsg = "Invalid DPF field: " & asItems(0)
            GoTo LoadDpf_Err
         End If

         Try
            g_aFields(iCnt).IndexFile = Trim(asItems(FLD_NDXFILE))
            g_aFields(iCnt).RecordFile = Trim(asItems(FLD_RECFILE))
            g_aFields(iCnt).TermFile = Trim(asItems(FLD_TERMFILE))
            g_aFields(iCnt).WordFile = Trim(asItems(FLD_WORDFILE))
            g_aFields(iCnt).Tag = asItems(FLD_TAG)
            g_aFields(iCnt).MemberOfGroup = asItems(FLD_MEMBEROF)
            g_aFields(iCnt).Display = CBool(asItems(FLD_DISPLAY))
            g_aFields(iCnt).Group = CBool(asItems(FLD_GROUP))
            g_aFields(iCnt).Index = CBool(asItems(FLD_INDEX))
            g_aFields(iCnt).Indexed = CBool(asItems(FLD_INDEXED))
            g_aFields(iCnt).Sorted = CBool(asItems(FLD_SORTED))
            g_aFields(iCnt).ID = asItems(FLD_ID)
            g_aFields(iCnt).FieldLength = asItems(FLD_LENGTH)
            g_aFields(iCnt).FieldOffSet = asItems(FLD_OFFSET)
            g_aFields(iCnt).RecordArrayLength = asItems(FLD_ARRAYLEN)
            g_aFields(iCnt).Type = asItems(FLD_TYPE)
            g_aFields(iCnt).Terms = asItems(FLD_TERMS)
            g_aFields(iCnt).UniqueTerms = asItems(FLD_UTERMS)
         Catch ex As Exception
            sErrMsg = "Invalid DPF field format at " & iCnt + 1
            GoTo LoadDpf_Err
         End Try

         If g_aFields(iCnt).Group = True Then
            g_aFields(iCnt).Display = False
         ElseIf g_aFields(iCnt).Display = True Then
            g_iDisplayFields = g_iDisplayFields + 1
         End If
         If g_aFields(iCnt).Index = True Then
            g_iSearchFields = g_iSearchFields + 1
            If g_aFields(iCnt).Group = False Then g_iExtractFields = g_iExtractFields + 1
         Else
            If g_aFields(iCnt).MemberOfGroup <> "" Then g_iExtractFields = g_iExtractFields + 1
         End If
      Next

      FileClose(fhDpf)
      LoadDpf = 0
      GoTo LoadDpf_Exit

LoadDpf_Err:
      If fhDpf > 0 Then
         FileClose(fhDpf)
      End If
      LoadDpf = -1

LoadDpf_Exit:
      objINI = Nothing
   End Function

End Class
