Option Strict Off
Option Explicit On
Imports System.IO

Friend Class clsINIWrapper

   ' Wrapper around the writeprivateprofilestring
   '
   ' Since some values will have carriage returns and/or line feeds, or tabs, we must convert them for the ini file
   '
   Public Function valueWrite(ByRef Section As String, ByRef Variable As String, ByRef Value As String, ByRef FilePath As String) As Integer
      On Error GoTo WriteError
      Dim lRet As Integer

      If InStr(Value, Chr(10)) > 0 Then
         Value = ReplaceStrings(Value, Chr(10), "**chr10**")
      End If

      If InStr(Value, Chr(13)) > 0 Then
         Value = ReplaceStrings(Value, Chr(13), "**chr13**")
      End If

      If InStr(Value, Chr(9)) > 0 Then
         Value = ReplaceStrings(Value, Chr(9), "**chr9**")
      End If

      'If lRet > 0, function is successful written
      lRet = IniFileUtil.WritePrivateProfileString(Section, Variable, Value, FilePath)
      valueWrite = True
      If lRet = 0 Then
         File.SetAttributes(FilePath, FileAttributes.Normal)
         lRet = IniFileUtil.WritePrivateProfileString(Section, Variable, Value, FilePath)
         If lRet = 0 Then valueWrite = False
      End If
      Exit Function

WriteError:
      valueWrite = 0
   End Function

   '
   ' Wrapper around the getprivateprofilestring
   '
   ' ***Warning** We might have to account for larger than 500 characters
   '
   ' Since some values will have carriage returns and/or line feeds, we must convert them for the ini file
   '
   Public Function valueRead(ByRef Section As String, ByRef Variable As String, ByRef Value As String, ByRef FilePath As String) As Integer
      Dim AllDataRetrieved As Boolean
      Dim tempval As String = ""
      Dim BufferSize As Integer

      On Error GoTo ReadError

      AllDataRetrieved = False

      Do While AllDataRetrieved = False

         BufferSize = BufferSize + 500

         tempval = New String(" ", BufferSize)

         Call IniFileUtil.GetPrivateProfileString(Section, Variable, "", tempval, Len(tempval), FilePath)

         tempval = Trim(tempval)

         If Len(tempval) > (BufferSize - 1) Then
            AllDataRetrieved = False
         Else
            AllDataRetrieved = True
         End If

      Loop

      Value = tempval

      If InStr(Value, "**chr10**") > 0 Then
         Value = ReplaceStrings(Value, "**chr10**", Chr(10))
      End If

      If InStr(Value, "**chr13**") > 0 Then
         Value = ReplaceStrings(Value, "**chr13**", Chr(13))
      End If

      If InStr(Value, "**chr9**") > 0 Then
         Value = ReplaceStrings(Value, "**chr9**", Chr(9))
      End If

      ' For some stupid reason, we get this character sometimes
      If InStr(Value, Chr(0)) > 0 Then
         Value = ReplaceStrings(Value, Chr(0), "")
      End If

      valueRead = True
      Value = Trim(Value)
      Exit Function

ReadError:
      valueRead = False
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "clsINIWrapper : valueRead")
   End Function

End Class