Option Strict Off
Option Explicit On
Module XceedZipDllApi
   '
   ' Xceed Zip Compresion Library - Dll Api
   ' Copyright (c) 1998-2002 - Xceed Software Inc.
   '
   ' [xcdDllApi.bas]
   '
   ' This module contains data types, and function declarations required
   ' by VB in order to be able to access the Xceed Zip 5 DLL API
   ' exported functions.
   '
   ' This file is part of Xceed Zip Compresion Library. The source code in this file
   ' is only intended as a supplement to the documentation, and is provided
   ' "as is", without warranty of any kind, either expressed or implied.
   '

   '=============================
   'Useful Windows API prototypes
   '=============================

   Private Declare Sub CopyMemory Lib "KERNEL32" Alias "RtlMoveMemory" (ByRef lpvDest As Object, ByRef lpvSource As Object, ByVal cbCopy As Integer)

   '===========
   'Data types
   '===========

   '-----------------------------
   'Windows SYSTEMTIME structure
   '-----------------------------

   Public Structure SYSTEMTIME
      Dim wYear As Short
      Dim wMonth As Short
      Dim wDayOfWeek As Short
      Dim wDay As Short
      Dim wHour As Short
      Dim wMinute As Short
      Dim wSecond As Short
      Dim wMilliseconds As Short
   End Structure

   Public Delegate Sub FPtr(ByVal wParam As Integer, ByVal lParam As Integer)
   Public Delegate Function FPtr2(ByVal value As String) As Boolean

   Public Class LibWrap
      ' Declares managed prototypes for unmanaged functions. 
      Declare Sub XzSetXceedZipCallback Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal lpfnCallback As FPtr)
      'Declare Sub TestCallBack Lib "..\LIB\PinvokeLib.dll" (ByVal cb As FPtr, ByVal value As Integer)

      'Declare Sub TestCallBack2 Lib "..\LIB\PinvokeLib.dll" (ByVal cb2 As FPtr2, ByVal value As String)
   End Class

   '------------------------------------------------------
   'Event parameter structures passed to callback function
   '------------------------------------------------------

   'ListingFile event parameters

   Public Structure xcdListingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String
      Dim lSize As Integer
      Dim lCompressedSize As Integer
      Dim nCompressionRatio As Short
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim lCRC As Integer
      Dim stLastModified As SYSTEMTIME
      Dim stLastAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      Dim xMethod As Integer 'xcdCompressionMethod data type
      Dim bEncrypted As Integer
      Dim lDiskNumber As Integer
      Dim bExcluded As Integer
      Dim xReason As Integer 'xcdSkippingReason data type
   End Structure

   'PreviewingFile event parameters

   Public Structure xcdPreviewingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szSourceFilename As String
      Dim lSize As Integer
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim stModified As SYSTEMTIME
      Dim stAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      Dim bExcluded As Integer
      Dim xReason As Integer 'xcdSkippingReason data type
   End Structure

   'InsertDisk event parameters

   Public Structure xcdInsertDiskParams
      Dim wStructSize As Short
      Dim hZip As Integer
      Dim lDiskNumber As Integer
      Dim bDiskInserted As Integer 'modifyable
   End Structure

   'ZipPreprocessingFile event parameters

   Public Structure xcdZipPreprocessingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String 'modifyable
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String 'modifyable
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szSourceFilename As String
      Dim lSize As Integer
      Dim xAttributes As Integer 'modifyable            'xcdFileAttributes data type
      Dim stModified As SYSTEMTIME 'modifyable
      Dim stAccessed As SYSTEMTIME 'modifyable
      Dim stCreated As SYSTEMTIME 'modifyable
      Dim xMethod As Integer 'modifyable            'xcdCompressionMethod data type
      Dim bEncrypted As Integer 'modifyable
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szPassword As String 'modifyable
      Dim bExcluded As Integer 'modifyable
      Dim xReason As Integer 'xcdSkippingReason data type
      Dim bExisting As Integer
   End Structure

   'UnzipPreprocessingFile event parameters

   Public Structure xcdUnzipPreprocessingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szDestFilename As String 'modifyable
      Dim lSize As Integer
      Dim lCompressedSize As Integer
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim lCRC As Integer
      Dim stModified As SYSTEMTIME 'modifyable
      Dim stAccessed As SYSTEMTIME 'modifyable
      Dim stCreated As SYSTEMTIME 'modifyable
      Dim xMethod As Integer 'xcdCompressionMethod data type
      Dim bEncrypted As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szPassword As String 'modifyable
      Dim lDiskNumber As Integer
      Dim bExcluded As Integer 'modifyable
      Dim xReason As Integer 'xcdSkippingReason data type
      Dim bExisting As Integer
      Dim xDestination As Integer 'modifyable             'xcdUnzipDestination data type
   End Structure

   'SkippingFile event parameters

   Public Structure xcdSkippingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilenameOnDisk As String
      Dim lSize As Integer
      Dim lCompressedSize As Integer
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim lCRC As Integer
      Dim stModified As SYSTEMTIME
      Dim stAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      Dim xMethod As Integer 'xcdCompressionMethod
      Dim bEncrypted As Integer
      Dim xReason As Integer 'xcdSkippingReason
   End Structure

   'RemovingFile event parameters

   Public Structure xcdRemovingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String
      Dim lSize As Integer
      Dim lCompressedSize As Integer
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim lCRC As Integer
      Dim stModified As SYSTEMTIME
      Dim stAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      Dim xMethod As Integer 'xcdCompressionMethod data type
      Dim bEncrypted As Integer
   End Structure

   'TestingFile event parameters

   Public Structure xcdTestingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String
      Dim lSize As Integer
      Dim lCompressedSize As Integer
      Dim nCompressionRatio As Short
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim lCRC As Integer
      Dim stModified As SYSTEMTIME
      Dim stAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      Dim xMethod As Integer 'xcdCompressionMethod data type
      Dim bEncrypted As Integer
      Dim lDiskNumber As Integer
   End Structure

   'FileStatus event parameters

   Public Structure xcdFileStatusParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      Dim lSize As Integer
      Dim lCompressedSize As Integer
      Dim lBytesProcessed As Integer
      Dim nBytesPercent As Short
      Dim nCompressionRatio As Short
      Dim bFileCompleted As Integer
   End Structure

   'GlobalStatus event parameters

   Public Structure xcdGlobalStatusParams
      Dim wStructSize As Short
      Dim hZip As Integer
      Dim lFilesTotal As Integer
      Dim lFilesProcessed As Integer
      Dim lFilesSkipped As Integer
      Dim nFilesPercent As Short
      Dim lBytesTotal As Integer
      Dim lBytesProcessed As Integer
      Dim lBytesSkipped As Integer
      Dim nBytesPercent As Short
      Dim lBytesOutput As Integer
      Dim nCompressionRatio As Short
   End Structure

   'DiskNotEmpty event parameters

   Public Structure xcdDiskNotEmptyParams
      Dim wStructSize As Short
      Dim hZip As Integer
      Dim xAction As Integer 'modifyable             'xcdNotEmptyAction data type
   End Structure

   'ProcessCompleted event parameters

   Public Structure xcdProcessCompletedParams
      Dim wStructSize As Short
      Dim hZip As Integer
      Dim lFilesTotal As Integer
      Dim lFilesProcessed As Integer
      Dim lFilesSkipped As Integer
      Dim lBytesTotal As Integer
      Dim lBytesProcessed As Integer
      Dim lBytesSkipped As Integer
      Dim lBytesOutput As Integer
      Dim nCompressionRatio As Short
      Dim xResult As Integer 'xcdError data type
   End Structure

   'ZipComment event parameters

   Public Structure xcdZipCommentParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(10240), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=10240)> Public szComment As String 'modifyable
   End Structure

   'QueryMemoryFile event parameters

   Public Structure xcdQueryMemoryFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      Dim lUserTag As Integer 'modifyable
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String 'modifyable
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String 'modifyable
      Dim xAttributes As Integer 'modifyable         'xcdFileAttributes data type
      Dim stModified As SYSTEMTIME 'modifyable
      Dim stAccessed As SYSTEMTIME 'modifyable
      Dim stCreated As SYSTEMTIME 'modifyable
      Dim bEncrypted As Integer 'modifyable
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szPassword As String 'modifyable
      Dim bFileProvided As Integer 'modifyable
   End Structure

   'ZippingMemoryFile event parameters

   Public Structure xcdZippingMemoryFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      Dim lUserTag As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      Dim pbDataToCompress As Integer 'modifyable    'Address of buffer
      Dim dwDataSize As Integer 'modifyable
      Dim bEndOfData As Integer 'modifyable
   End Structure

   'UnzippingMemoryFile event parameters

   Public Structure xcdUnzippingMemoryFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      Dim pbUncompressedData As Integer 'Address of buffer
      Dim dwDataSize As Integer
      Dim bEndOfData As Integer
   End Structure

   'Warning event parameters

   Public Structure xcdWarningParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      Dim xWarning As Integer 'xcdWarning data type
   End Structure

   'InvalidPassword event parameters

   Public Structure xcdInvalidPasswordParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szNewPassword As String 'modifyable
      Dim bRetry As Integer 'modifyable
   End Structure

   'ReplacingFile event parameters

   Public Structure xcdReplacingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String
      Dim lSize As Integer
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim stLastModified As SYSTEMTIME
      Dim stLastAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szOrigFilename As String
      Dim lOrigSize As Integer
      Dim xOrigAttributes As Integer 'xcdFileAttributes data type
      Dim stOrigLastModified As SYSTEMTIME
      Dim stOrigLastAccessed As SYSTEMTIME
      Dim stOrigCreated As SYSTEMTIME
      Dim bReplaceFile As Integer
   End Structure

   'ZipContentsStatus event parameters

   Public Structure xcdZipContentsStatusParams
      Dim wStructSize As Short
      Dim hZip As Integer
      Dim lFilesTotal As Integer
      Dim lFilesRead As Integer
      Dim nFilesPercent As Short
   End Structure

   'DeletingFile event parameters
   Public Structure xcdDeletingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      Dim lSize As Integer
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim stModified As SYSTEMTIME
      Dim stAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      Dim bDoNotDelete As Integer
   End Structure

   'ConvertPreprocessingFile event parameters
   Public Structure xcdConvertPreprocessingFileParamsA
      Dim wStructSize As Short
      Dim hZip As Integer
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szFilename As String
      <VBFixedString(1024), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=1024)> Public szComment As String
      <VBFixedString(260), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=260)> Public szDestFilename As String
      Dim lSize As Integer
      Dim lCompressedSize As Integer
      Dim xAttributes As Integer 'xcdFileAttributes data type
      Dim lCRC As Integer
      Dim stModified As SYSTEMTIME
      Dim stAccessed As SYSTEMTIME
      Dim stCreated As SYSTEMTIME
      Dim xMethod As Integer 'xcdCompressionMethod data type
      Dim bEncrypted As Integer
      Dim lDiskNumber As Integer
      Dim bExcluded As Integer
      Dim xReason As Integer 'xcdSkippingReason data type
      Dim bExisting As Integer
   End Structure

   '========================
   'Initialization functions
   '========================

   Public Declare Function XceedZipInitDLL Lib "XceedZip.dll" () As Integer
   Public Declare Function XceedZipShutdownDLL Lib "XceedZip.dll" () As Integer

   '===========================
   'Instance creation functions
   '===========================

   Public Declare Function XzCreateXceedZipA Lib "XceedZip.dll" (ByVal sLicense As String) As Integer
   Public Declare Sub XzDestroyXceedZip Lib "XceedZip.dll" (ByVal hZip As Integer)

   Public Declare Function XcCreateXceedCompressionA Lib "XceedZip.dll" (ByVal sLicense As String) As Integer
   Public Declare Sub XcDestroyXceedCompression Lib "XceedZip.dll" (ByVal hComp As Integer)

   'New since version 4.2
   Public Declare Sub XziDestroyXceedZipItems Lib "XceedZip.dll" (ByVal hItems As Integer)

   '===============================================
   'Callback and Windows message support for events
   '===============================================

   Public Declare Sub XzSetXceedZipCallback Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal lpfnCallback As Long)
   Public Declare Sub XzSetXceedZipWindow Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal hWnd As Integer)

   '=============================================
   'Xceed Zip control property handling functions
   '=============================================
   '--------------
   'Abort property
   '--------------

   Public Declare Function XzGetAbort Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetAbort Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '-----------------------------
   'BackgroundProcessing property
   '-----------------------------

   Public Declare Function XzGetBackgroundProcessing Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetBackgroundProcessing Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '-----------------
   'BasePath property
   '-----------------

   Public Declare Function XzGetBasePathA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetBasePathA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '-------------------------
   'CompressionLevel property
   '-------------------------

   Public Declare Function XzGetCompressionLevel Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetCompressionLevel Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal uValue As Integer)

   '-------------------------
   'CurrentOperation property
   '-------------------------

   Public Declare Function XzGetCurrentOperation Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer

   '---------------------------
   'EncryptionPassword property
   '---------------------------

   Public Declare Function XzGetEncryptionPasswordA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetEncryptionPasswordA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '-------------------------------
   'ExcludedFileAttributes property
   '-------------------------------

   Public Declare Function XzGetExcludedFileAttributes Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetExcludedFileAttributes Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal uValue As Integer)

   '---------------------
   'ExtraHeaders property
   '---------------------

   Public Declare Function XzGetExtraHeaders Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetExtraHeaders Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal uValue As Integer)

   '-----------------------
   'FilesToExclude property
   '-----------------------

   Public Declare Function XzGetFilesToExcludeA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetFilesToExcludeA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '-----------------------
   'FilesToProcess property
   '-----------------------

   Public Declare Function XzGetFilesToProcessA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetFilesToProcessA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '-------------------------
   'MaxDateToProcess property
   '-------------------------

   'UPGRADE_WARNING: Structure SYSTEMTIME may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
   Public Declare Sub XzGetMaxDateToProcess Lib "XceedZip.dll" (ByVal hZip As Integer, ByRef lpdtBuffer As SYSTEMTIME)
   'UPGRADE_WARNING: Structure SYSTEMTIME may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
   Public Declare Sub XzSetMaxDateToProcess Lib "XceedZip.dll" (ByVal hZip As Integer, ByRef lpdtValue As SYSTEMTIME)

   '-------------------------
   'MaxSizeToProcess property
   '-------------------------

   Public Declare Function XzGetMaxSizeToProcess Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetMaxSizeToProcess Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal dwValue As Integer)

   '-------------------------
   'MinDateToProcess property
   '-------------------------

   'UPGRADE_WARNING: Structure SYSTEMTIME may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
   Public Declare Sub XzGetMinDateToProcess Lib "XceedZip.dll" (ByVal hZip As Integer, ByRef lpdtBuffer As SYSTEMTIME)
   'UPGRADE_WARNING: Structure SYSTEMTIME may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
   Public Declare Sub XzSetMinDateToProcess Lib "XceedZip.dll" (ByVal hZip As Integer, ByRef lpdtValue As SYSTEMTIME)

   '-------------------------
   'MinSizeToProcess property
   '-------------------------

   Public Declare Function XzGetMinSizeToProcess Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetMinSizeToProcess Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal dwValue As Integer)

   '----------------------
   'PreservePaths property
   '----------------------

   Public Declare Function XzGetPreservePaths Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetPreservePaths Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '--------------------------
   'ProcessSubfolders property
   '--------------------------

   Public Declare Function XzGetProcessSubfolders Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetProcessSubfolders Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '-------------------------------
   'RequiredFileAttributes property
   '-------------------------------

   Public Declare Function XzGetRequiredFileAttributes Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetRequiredFileAttributes Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal dwValue As Integer)

   '------------------------
   'SfxBinaryModule property
   '------------------------

   Public Declare Function XzGetSfxBinaryModuleA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxBinaryModuleA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String)

   '-------------------
   'SfxButtons property
   '-------------------

   Public Declare Function XzGetSfxButtonsA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal xIndex As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxButtonsA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal xIndex As Integer, ByVal sValue As String)

   '---------------------------
   'SfxDefaultPassword property
   '---------------------------

   Public Declare Function XzGetSfxDefaultPasswordA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxDefaultPasswordA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '--------------------------------
   'SfxDefaultUnzipToFolder property
   '--------------------------------

   Public Declare Function XzGetSfxDefaultUnzipToFolderA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxDefaultUnzipToFolderA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '--------------------------------
   'SfxExistingFileBehavior property
   '--------------------------------

   Public Declare Function XzGetSfxExistingFileBehavior Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSfxExistingFileBehavior Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal uValue As Integer)

   '---------------------------------
   'SfxExtensionsToAssociate property
   '---------------------------------

   Public Declare Function XzGetExtensionsToAssociateA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetExtensionsToAssociateA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '------------------------
   'SfxExecuteAfter property
   '------------------------

   Public Declare Function XzGetSfxExecuteAfterA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxExecuteAfterA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '------------------------
   'SfxIconFilename property
   '------------------------

   Public Declare Function XzGetSfxIconFilenameA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxIconFilenameA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String)

   '-----------------------
   'SfxInstallMode property
   '-----------------------

   Public Declare Function XzGetSfxInstallMode Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSfxInstallMode Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '--------------------
   'SfxMessages property
   '--------------------

   Public Declare Function XzGetSfxMessagesA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal xIndex As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxMessagesA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal xIndex As Integer, ByVal sValue As String)

   '------------------------
   'SfxProgramGroup property
   '------------------------

   Public Declare Function XzGetSfxProgramGroupA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxProgramGroupA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '-----------------------------
   'SfxProgramGroupItems property
   '-----------------------------

   Public Declare Function XzGetSfxProgramGroupItemsA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxProgramGroupItemsA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '----------------------
   'SfxReadmeFile property
   '----------------------

   Public Declare Function XzGetSfxReadMeFileA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxReadmeFileA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '-------------------
   'SfxStrings property
   '-------------------

   Public Declare Function XzGetSfxStringsA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal xIndex As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxStringsA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal xIndex As Integer, ByVal sValue As String)

   '-----------------------
   'SkipIfExisting property
   '-----------------------

   Public Declare Function XzGetSkipIfExisting Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSkipIfExisting Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '--------------------------
   'SkipIfNotExisting property
   '--------------------------

   Public Declare Function XzGetSkipIfNotExisting Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSkipIfNotExisting Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '------------------------
   'SkipIfOlderDate property
   '------------------------

   Public Declare Function XzGetSkipIfOlderDate Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSkipIfOlderDate Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '---------------------------
   'SkipIfOlderVersion property
   '---------------------------

   Public Declare Function XzGetSkipIfOlderVersion Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSkipIfOlderVersion Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '--------------------------
   'SpanMultipleDisks property
   '--------------------------

   Public Declare Function XzGetSpanMultipleDisks Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSpanMultipleDisks Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal uValue As Integer)

   '------------------
   'SplitSize property
   '------------------

   Public Declare Function XzGetSplitSize Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetSplitSize Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal dwValue As Integer)

   '-------------------
   'TempFolder property
   '-------------------

   Public Declare Function XzGetTempFolderA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetTempFolderA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '----------------------
   'UnzipToFolder property
   '----------------------

   Public Declare Function XzGetUnzipToFolderA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetUnzipToFolderA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '--------------------
   'UseTempFile property
   '--------------------

   Public Declare Function XzGetUseTempFile Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetUseTempFile Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '--------------------
   'ZipFilename property
   '--------------------

   Public Declare Function XzGetZipFilenameA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetZipFilenameA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sValue As String)

   '-----------------------
   'ZipOpenedFiles property
   '-----------------------

   Public Declare Function XzGetZipOpenedFiles Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetZipOpenedFiles Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '
   'New since version 4.2
   '

   '-----------------------
   'SfxFilesToCopy property
   '-----------------------

   Public Declare Function XzGetSfxFilesToCopyA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxFilesToCopyA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szValue As String)

   '---------------------------
   'SfxFilesToRegister property
   '---------------------------

   Public Declare Function XzGetSfxFilesToRegisterA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxFilesToRegisterA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szValue As String)

   '------------------------
   'SfxRegistryKeys property
   '------------------------

   Public Declare Function XzGetSfxRegistryKeysA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szBuffer As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XzSetSfxRegistryKeysA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szValue As String)

   '--------------------------
   'DeleteZippedFiles property
   '--------------------------

   Public Declare Function XzGetDeleteZippedFiles Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetDeleteZippedFiles Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bValue As Integer)

   '---------------------------
   'FirstDiskFreeSpace property
   '---------------------------

   Public Declare Function XzGetFirstDiskFreeSpace Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetFirstDiskFreeSpace Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal dwValue As Integer)

   '-------------------------
   'MinDiskFreeSpace property
   '-------------------------

   Public Declare Function XzGetMinDiskFreeSpace Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetMinDiskFreeSpace Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal dwValue As Integer)

   '------------------------
   'EventsToTrigger property
   '------------------------

   Public Declare Function XzGetEventsToTrigger Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer
   Public Declare Sub XzSetEventsToTrigger Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal dwValue As Integer)

   '====================================================
   'XceedCompression control property handling functions
   '====================================================

   '---------------------------
   'EncryptionPassword property
   '---------------------------

   Public Declare Function XcGetEncryptionPasswordA Lib "XceedZip.dll" (ByVal hComp As Integer, ByVal sEncryptionPassword As String, ByVal uMaxLength As Integer) As Integer
   Public Declare Sub XcSetEncryptionPasswordA Lib "XceedZip.dll" (ByVal hComp As Integer, ByVal sValue As String)

   '-------------------------
   'CompressionLevel property
   '-------------------------

   Public Declare Function XcGetCompressionLevel Lib "XceedZip.dll" (ByVal hComp As Integer) As Integer
   Public Declare Sub XcSetCompressionLevel Lib "XceedZip.dll" (ByVal hComp As Integer, ByVal nValue As Integer)

   '==============================================
   'Exported API for the Xceed Zip control methods
   '==============================================

   '------------------------
   'AddFilesToProcess method
   '------------------------

   Public Declare Sub XzAddFilesToProcessA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sFileMask As String)

   '------------------------
   'AddFilesToExclude method
   '------------------------

   Public Declare Sub XzAddFilesToExcludeA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sFileMask As String)

   '--------------
   'Convert method
   '--------------

   Public Declare Function XzConvertA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sDestFilename As String) As Integer

   '--------------------------
   'GetErrorDescription method
   '--------------------------

   Public Declare Function XzGetErrorDescriptionA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal xType As Integer, ByVal xCode As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer

   '----------------------------
   'GetZipFileInformation method
   '----------------------------

   Public Declare Function XzGetZipFileInformation Lib "XceedZip.dll" (ByVal hZip As Integer, ByRef plNBFiles As Integer, ByRef plCompressedSize As Integer, ByRef plUnCompressedSize As Integer, ByRef pnCompressionRatio As Short, ByRef pbSpanned As Integer) As Integer

   '----------------------
   'ListZipConetnts method
   '----------------------

   Public Declare Function XzListZipContents Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer

   '-------------------
   'PreviewFiles method
   '-------------------

   Public Declare Function XzPreviewFiles Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bCalcCompSize As Integer) As Integer

   '------------------
   'RemoveFiles method
   '------------------

   Public Declare Function XzRemoveFiles Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer

   '---------------------------------
   'SfxAddExtensionToAssociate method
   '---------------------------------

   Public Declare Sub XzSfxAddExtensionToAssociateA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sDescription As String, ByVal sExtension As String, ByVal sApplication As String)

   '-----------------------------
   'SfxAddProgramGroupItem method
   '-----------------------------

   Public Declare Sub XzSfxAddProgramGroupItemA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sApplication As String, ByVal sDescription As String)

   '-------------------------------------------------------------
   'SfxClearButtons, SfxClearMessages and SfxClearStrings methods
   '-------------------------------------------------------------

   Public Declare Sub XzSfxClearButtons Lib "XceedZip.dll" (ByVal hZip As Integer)
   Public Declare Sub XzSfxClearMessages Lib "XceedZip.dll" (ByVal hZip As Integer)
   Public Declare Sub XzSfxClearStrings Lib "XceedZip.dll" (ByVal hZip As Integer)

   '---------------------------------------
   'SfxLoadConfig and SfxSaveConfig methods
   '---------------------------------------

   Public Declare Function XzSfxLoadConfigA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sConfigFilename As String) As Integer
   Public Declare Function XzSfxSaveConfigA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal sConfigFilename As String) As Integer

   '-------------------------------------------------------------
   'SfxResetButtons, SfxResetMessages and SfxResetStrings methods
   '-------------------------------------------------------------

   Public Declare Sub XzSfxResetButtons Lib "XceedZip.dll" (ByVal hZip As Integer)
   Public Declare Sub XzSfxResetMessages Lib "XceedZip.dll" (ByVal hZip As Integer)
   Public Declare Sub XzSfxResetStrings Lib "XceedZip.dll" (ByVal hZip As Integer)

   '------------------
   'TestZipFile method
   '------------------

   Public Declare Function XzTestZipFile Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal bCheckCompressedData As Integer) As Integer

   '------------
   'Unzip method
   '------------

   Public Declare Function XzUnzip Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer

   '----------
   'Zip method
   '----------

   Public Declare Function XzZip Lib "XceedZip.dll" (ByVal hZip As Integer) As Integer

   '
   'New since version 4.2
   '

   '-------------------------
   'SfxAddExecuteAfter method
   '-------------------------

   Public Declare Sub XzSfxAddExecuteAfterA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szApplication As String, ByVal szParameters As String)

   '-----------------------
   'SfxAddFileToCopy method
   '-----------------------

   Public Declare Sub XzSfxAddFileToCopyA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szSource As String, ByVal szDestination As String)

   '---------------------------
   'SfxAddFileToRegister method
   '---------------------------

   Public Declare Sub XzSfxAddFileToRegisterA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szFilename As String)

   '------------------------
   'SfxAddRegistryKey method
   '------------------------

   Public Declare Sub XzSfxAddRegistryKeyA Lib "XceedZip.dll" (ByVal hZip As Integer, ByVal szKey As String, ByVal szValueName As String, ByVal szValue As String)

   '---------------------
   'GetZipContents method
   '---------------------

   Public Declare Function XzGetZipContents Lib "XceedZip.dll" (ByVal hZip As Integer, ByRef phItems As Integer) As Integer

   '======================================================
   'Exported API for the Xceed Compression control methods
   '======================================================

   '--------------------------
   'GetErrorDescription method
   '--------------------------

   Public Declare Function XcGetErrorDescriptionA Lib "XceedZip.dll" (ByVal hComp As Integer, ByVal sBuffer As String, ByVal uMaxLength As Integer) As Integer

   '=========================================
   'Exported API for XceedZipItems collection
   '=========================================

   'UPGRADE_WARNING: Structure xcdListingFileParamsA may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
   Public Declare Function XziGetFirstItemA Lib "XceedZip.dll" (ByVal hItems As Integer, ByRef pxItem As xcdListingFileParamsA) As Integer
   'UPGRADE_WARNING: Structure xcdListingFileParamsA may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
   Public Declare Function XziGetNextItemA Lib "XceedZip.dll" (ByVal hItems As Integer, ByRef pxItem As xcdListingFileParamsA) As Integer

   '=========================
   'Memory allocation support
   '=========================

   Public Declare Function XzAlloc Lib "XceedZip.dll" (ByVal lDataSize As Integer) As Integer
   Public Declare Sub XzFree Lib "XceedZip.dll" (ByVal pData As Integer)

   '=====================================
   'Enumerated types and public constants
   '=====================================

   '----------------------------
   'Event wParam possible values
   '----------------------------

   Public Const XM_LISTINGFILE As Short = 1
   Public Const XM_PREVIEWINGFILE As Short = 2
   Public Const XM_INSERTDISK As Short = 3
   Public Const XM_ZIPPREPROCESSINGFILE As Short = 4
   Public Const XM_UNZIPPREPROCESSINGFILE As Short = 5
   Public Const XM_SKIPPINGFILE As Short = 6
   Public Const XM_REMOVINGFILE As Short = 7
   Public Const XM_TESTINGFILE As Short = 8
   Public Const XM_FILESTATUS As Short = 9
   Public Const XM_GLOBALSTATUS As Short = 10
   Public Const XM_DISKNOTEMPTY As Short = 11
   Public Const XM_PROCESSCOMPLETED As Short = 12
   Public Const XM_ZIPCOMMENT As Short = 13
   Public Const XM_QUERYMEMORYFILE As Short = 14
   Public Const XM_ZIPPINGMEMORYFILE As Short = 15
   Public Const XM_UNZIPPINGMEMORYFILE As Short = 16
   Public Const XM_WARNING As Short = 17
   Public Const XM_INVALIDPASSWORD As Short = 18
   Public Const XM_REPLACINGFILE As Short = 19
   Public Const XM_ZIPCONTENTSSTATUS As Short = 20
   Public Const XM_DELETINGFILE As Short = 21
   Public Const XM_CONVERTPREPROCESSINGFILE As Short = 22

   '-------------------------
   'xcdSfxButtons enumeration
   '-------------------------

   Public Const xsbOk As Short = 0
   Public Const xsbCancel As Short = 1
   Public Const xsbAbort As Short = 2
   Public Const xsbSkip As Short = 3
   Public Const xsbAlwaysSkip As Short = 4
   Public Const xsbYes As Short = 5
   Public Const xsbNo As Short = 6
   Public Const xsbOverwriteAll As Short = 7
   Public Const xsbOverwriteNone As Short = 8
   Public Const xsbContinue As Short = 9
   Public Const xsbExit As Short = 10
   Public Const xsbAgree As Short = 11
   Public Const xsbRefuse As Short = 12

   '--------------------------
   'xcdSfxMessages enumeration
   '--------------------------

   Public Const xsmSuccess As Short = 0
   Public Const xsmFail As Short = 1
   Public Const xsmErrorCreatingFolder As Short = 2
   Public Const xsmIntro As Short = 3
   Public Const xsmLicense As Short = 4
   Public Const xsmDestinationFolder As Short = 5
   Public Const xsmPassword As Short = 6
   Public Const xsmInsertLastDisk As Short = 7
   Public Const xsmInsertDisk As Short = 8
   Public Const xsmAbortUnzip As Short = 9
   Public Const xsmCreateFolder As Short = 10
   Public Const xsmOverwrite As Short = 11
   Public Const xsmProgress As Short = 12

   '-------------------------
   'xcdSfxStrings enumeration
   '-------------------------

   Public Const xssProgressBar As Short = 0
   Public Const xssTitle As Short = 1
   Public Const xssCurrentFolder As Short = 2
   Public Const xssShareName As Short = 3
   Public Const xssNetwork As Short = 4

   '------------------------
   'xcdValueType enumeration
   '------------------------

   Public Const xvtError As Short = 0
   Public Const xvtWarning As Short = 1
   Public Const xvtSkippingReason As Short = 2

   '-----------------------------
   'xcdFileAttributes enumeration
   '-----------------------------

   Public Const xfaNone As Short = 0
   Public Const xfaReadOnly As Short = 1
   Public Const xfaHidden As Short = 2
   Public Const xfaSystem As Short = 4
   Public Const xfaVolume As Short = 8
   Public Const xfaFolder As Short = 16
   Public Const xfaArchive As Short = 32
   Public Const xfaCompressed As Short = 2048

   '----------------------
   'xcdWarning enumeration
   '----------------------

   Public Const xwrIncompleteWrite As Short = 300
   Public Const xwrInvalidSignature As Short = 301
   Public Const xwrInvalidCentralOffset As Short = 302
   Public Const xwrInvalidLocalOffset As Short = 303
   Public Const xwrInvalidDescriptorOffset As Short = 304
   Public Const xwrJunkInZip As Short = 305
   Public Const xwrSecurityNotSupported As Short = 306
   Public Const xwrSecurityGet As Short = 307
   Public Const xwrSecuritySet As Short = 308
   Public Const xwrSecuritySize As Short = 309
   Public Const xwrSecurityVersion As Short = 310
   Public Const xwrSecurityUnknownCompression As Short = 311
   Public Const xwrSecurityData As Short = 312
   Public Const xwrUnicodeSize As Short = 313
   Public Const xwrUnicodeData As Short = 314
   Public Const xwrExtraHeaderSize As Short = 315
   Public Const xwrTimeStampSize As Short = 316
   Public Const xwrTimeStampFlags As Short = 317
   Public Const xwrFileTimesSize As Short = 318
   Public Const xwrInvalidFileCount As Short = 319
   Public Const xwrFileAlreadyOpenWrite As Short = 320

   '--------------------
   'xcdError enumeration
   '--------------------

   Public Const xerSuccess As Short = 0
   Public Const xerProcessStarted As Short = 1
   Public Const xerEmptyZipFile As Short = 500
   Public Const xerSeekInZipFile As Short = 501
   Public Const xerEndOfZipFile As Short = 502
   Public Const xerOpenZipFile As Short = 503
   Public Const xerCreateTempFile As Short = 504
   Public Const xerReadZipFile As Short = 505
   Public Const xerWriteTempZipFile As Short = 506
   Public Const xerWriteZipFile As Short = 507
   Public Const xerMoveTempFile As Short = 508
   Public Const xerNothingToDo As Short = 509
   Public Const xerCannotUpdateAndSpan As Short = 510
   Public Const xerMemory As Short = 511
   Public Const xerSplitSizeTooSmall As Short = 512
   Public Const xerSfxBinaryNotFound As Short = 513
   Public Const xerReadSfxBinary As Short = 514
   Public Const xerCannotUpdateSpanned As Short = 515
   Public Const xerBusy As Short = 516
   Public Const xerInsertDiskAbort As Short = 517
   Public Const xerUserAbort As Short = 518
   Public Const xerNotAZipFile As Short = 519
   Public Const xerUninitializedString As Short = 520
   Public Const xerUninitializedArray As Short = 521
   Public Const xerInvalidArrayDimensions As Short = 522
   Public Const xerInvalidArrayType As Short = 523
   Public Const xerCannotAccessArray As Short = 524
   Public Const xerUnsupportedDataType As Short = 525
   Public Const xerWarnings As Short = 526
   Public Const xerFilesSkipped As Short = 527
   Public Const xerDiskNotEmptyAbort As Short = 528
   Public Const xerRemoveWithoutTemp As Short = 529
   Public Const xerNotLicensed As Short = 530
   Public Const xerInvalidSfxProperty As Short = 531
   Public Const xerInternalError As Short = 999

   '-----------------------------
   'xcdSkippingReason enumeration
   '-----------------------------

   Public Const xsrIncluded As Short = 0
   Public Const xsrFilesToExclude As Short = 1
   Public Const xsrSkipExisting As Short = 2
   Public Const xsrSkipNotExisting As Short = 3
   Public Const xsrSkipOlderDate As Short = 4
   Public Const xsrSkipOlderVersion As Short = 5
   Public Const xsrRequiredAttributes As Short = 6
   Public Const xsrExcludedAttributes As Short = 7
   Public Const xsrMinDate As Short = 8
   Public Const xsrMaxDate As Short = 9
   Public Const xsrMinSize As Short = 10
   Public Const xsrMaxSize As Short = 11
   Public Const xsrSkipUser As Short = 12
   Public Const xsrDuplicateFilenames As Short = 13
   Public Const xsrSkipReplace As Short = 14
   Public Const xsrUpdateWithoutTemp As Short = 15
   Public Const xsrInvalidDiskNumber As Short = 100
   Public Const xsrFolderWithSize As Short = 101
   Public Const xsrWriteFile As Short = 102
   Public Const xsrOpenFile As Short = 103
   Public Const xsrReadFile As Short = 104
   Public Const xsrMoveFile As Short = 105
   Public Const xsrInvalidPassword As Short = 106
   Public Const xsrInvalidCRC As Short = 107
   Public Const xsrInvalidUncompSize As Short = 108
   Public Const xsrCentralHeaderData As Short = 109
   Public Const xsrLocalHeaderData As Short = 110
   Public Const xsrDescriptorHeaderData As Short = 111
   Public Const xsrCreateFolder As Short = 112
   Public Const xsrAccessDenied As Short = 113
   Public Const xsrCreateFile As Short = 114
   Public Const xsrRenamedToExisting As Short = 116
   Public Const xsrVolumeWithSize As Short = 117
   Public Const xsrCannotSetVolumeLabel As Short = 118
   Public Const xsrCentralHeaderNotFound As Short = 119
   Public Const xsrUnzipDiskFull As Short = 120
   Public Const xsrCompress As Short = 121
   Public Const xsrUncompress As Short = 122

   '---------------------------
   'xcdDiskSpanning enumeration
   '---------------------------

   Public Const xdsNever As Short = 0
   Public Const xdsAlways As Short = 1
   Public Const xdsRemovableDrivesOnly As Short = 2

   '--------------------------
   'xcdExtraHeader enumeration
   '--------------------------

   Public Const xehNone As Short = 0
   Public Const xehExtTimeStamp As Short = 1
   Public Const xehFileTimes As Short = 2
   Public Const xehSecurityDescriptor As Short = 4
   Public Const xehUnicode As Short = 8

   '--------------------------------------
   'xcdSfxExistingFileBehavior enumeration
   '--------------------------------------

   Public Const xseAsk As Short = 0
   Public Const xseSkip As Short = 1
   Public Const xseOverwrite As Short = 2

   '-------------------------------
   'xcdCurrentOperation enumeration
   '-------------------------------

   Public Const xcoIdle As Short = 0
   Public Const xcoPreviewing As Short = 1
   Public Const xcoListing As Short = 2
   Public Const xcoZipping As Short = 3
   Public Const xcoUnzipping As Short = 4
   Public Const xcoRemoving As Short = 5
   Public Const xcoTestingZipFile As Short = 6
   Public Const xcoGettingInformation As Short = 7
   Public Const xcoConverting As Short = 8

   '-------------------------------
   'xcdUnzipDestination enumeration
   '-------------------------------

   Public Const xudDisk As Short = 0
   Public Const xudMemory As Short = 1
   Public Const xudMemoryStream As Short = 2

   '-----------------------------
   'xcdCommonDiskSize enumeration
   '-----------------------------

   Public Const xcsFloppy3p5HD As Integer = 1457664
   Public Const xcsFloppy3p5DD As Integer = 730112
   Public Const xcsFloppy3p5XD As Integer = 3019898
   Public Const xcsFloppy5p25HD As Integer = 1310720
   Public Const xcsFloppy5p25DD As Integer = 655360
   Public Const xcsIomegaZip As Integer = 100431872
   Public Const xcsIomegaJazz1G As Integer = 1121976320
   Public Const xcsIomegaJazz2G As Integer = 2099249152
   Public Const xcsSyquestEzFlyer As Integer = 241172480
   Public Const xcsSyquestSyjet As Integer = 1610612736
   Public Const xcsSyquestSparQ As Integer = 1073741824
   Public Const xcsCDR As Integer = 681574400

   '-------------------------------
   'xcdCompressionError enumeration
   '-------------------------------

   Public Const xceSuccess As Short = 0
   Public Const xceSessionOpened As Short = 1000
   Public Const xceInitCompression As Short = 1001
   Public Const xceUnknownMethod As Short = 1002
   Public Const xceCompression As Short = 1003
   Public Const xceInvalidPassword As Short = 1004
   Public Const xceChecksumFailed As Short = 1005
   Public Const xceDataRemaining As Short = 1006
   Public Const xceNotLicensed As Short = 1007
   Public Const xceBusy As Short = 1008

   '--------------------------------
   'xcdCompressionMethod enumeration
   '--------------------------------

   Public Const xcmStored As Short = 0
   Public Const xcmDeflated As Short = 8

   '-----------------------------
   'xcdNotEmptyAction enumeration
   '-----------------------------

   Public Const xnaErase As Short = 0
   Public Const xnaAppend As Short = 1
   Public Const xnaAskAnother As Short = 2
   Public Const xnaAbort As Short = 3

   '-------------------------------
   'xcdCompressionLevel enumeration
   '-------------------------------

   Public Const xclNone As Short = 0
   Public Const xclLow As Short = 1
   Public Const xclMedium As Short = 6
   Public Const xclHigh As Short = 9

   '---------------------
   'xcdEvents enumeration
   '---------------------
   Public Const xevNone As Short = &H0S
   Public Const xevListingFile As Short = &H1S
   Public Const xevPreviewingFile As Short = &H2S
   Public Const xevInsertDisk As Short = &H4S
   Public Const xevZipPreprocessingFile As Short = &H8S
   Public Const xevUnzipPreprocessingFile As Short = &H10S
   Public Const xevSkippingFile As Short = &H20S
   Public Const xevRemovingFile As Short = &H40S
   Public Const xevTestingFile As Short = &H80S
   Public Const xevFileStatus As Short = &H100S
   Public Const xevGlobalStatus As Short = &H200S
   Public Const xevDiskNotEmpty As Short = &H400S
   Public Const xevProcessCompleted As Short = &H800S
   Public Const xevZipComment As Short = &H1000S
   Public Const xevQueryMemoryFile As Short = &H2000S
   Public Const xevZippingMemoryFile As Short = &H4000S
   Public Const xevUnzippingMemoryFile As Short = &H8000S
   Public Const xevWarning As Integer = &H10000
   Public Const xevInvalidPassword As Integer = &H20000
   Public Const xevReplacingFile As Integer = &H40000
   Public Const xevZipContentsStatus As Integer = &H80000
   Public Const xevDeletingFile As Integer = &H100000
   Public Const xevConvertPreprocessingFile As Integer = &H200000
   Public Const xevAll As Integer = &H7FFFFFFF


   '==============================
   'Callback support and squeleton
   '==============================

   '-----------------------------------------------------------------------
   'VB is limited to 32k of local variables, so we use global private
   'variables for the parameter structures used by the callback function.
   'You can remove the structures you don't need by deleting them from
   'here and declaring them as local variables in the XceedZipCallbackProc.
   '-----------------------------------------------------------------------

   Private xEmpty As xcdDiskNotEmptyParams
   Private xFileStatus As xcdFileStatusParamsA
   Private xGlobal As xcdGlobalStatusParams
   Private xInsert As xcdInsertDiskParams
   Private xPassword As xcdInvalidPasswordParamsA
   Private xListing As xcdListingFileParamsA
   Private xPreview As xcdPreviewingFileParamsA
   Private xCompleted As xcdProcessCompletedParams
   Private xQuery As xcdQueryMemoryFileParamsA
   Private xRemove As xcdRemovingFileParamsA
   Private xReplace As xcdReplacingFileParamsA
   Private xSkip As xcdSkippingFileParamsA
   Private xTest As xcdTestingFileParamsA
   Private xUnzipMem As xcdUnzippingMemoryFileParamsA
   Private xUnzipPre As xcdUnzipPreprocessingFileParamsA
   Private xWarning As xcdWarningParamsA
   Private xComment As xcdZipCommentParamsA
   Private xContents As xcdZipContentsStatusParams
   Private xZipMem As xcdZippingMemoryFileParamsA
   Private xZipPre As xcdZipPreprocessingFileParamsA
   Private xDelete As xcdDeletingFileParamsA
   Private xConvert As xcdConvertPreprocessingFileParamsA


   '-------------------------------------------------------------------------
   'Useful function to pad strings in parameter structures. These strings are
   'null-terminated, and may contain garbage past the null char.
   '-------------------------------------------------------------------------

   Public Function ExtractNullTermString(ByVal sNullString As String) As String
      Dim lNullPos As Integer
      lNullPos = InStr(sNullString, Chr(0))
      If lNullPos > 0 Then
         ExtractNullTermString = Left(sNullString, lNullPos - 1)
      Else
         ExtractNullTermString = sNullString
      End If
   End Function

   '----------------------------------------------------------------------
   'This is the squeleton of a callback procedure to handle events via
   'the Xceed Zip DLL API. You can copy this code to your own module(s),
   'or use it from right here by adding your event handling code wherever
   'the comment "Add your code here" appears.
   '
   'To notify Xceed Zip of which callback function to use, call the
   'Xceed Zip DLL API's XzSetXceedZipCallback function like this:

   'Call XzSetXceedZipCallback(hZip, AddressOf XceedZipCallbackProc)
   '
   'Keep in mind that the function you are using AddressOf on must
   'reside in a normal VB module file.
   '----------------------------------------------------------------------

   Public Sub XceedZipCallbackProc(ByVal wParam As Integer, ByVal lParam As Integer)
      Select Case wParam
         Case XM_DISKNOTEMPTY
            Call CopyMemory(xEmpty, lParam, Len(xEmpty))

            'Add your code here

            'Add the following line if you modified the xEmpty parameter
            'Call CopyMemory(ByVal lParam, xEmpty, Len(xEmpty))
         Case XM_FILESTATUS
            Call CopyMemory(xFileStatus, lParam, Len(xFileStatus))


            'Add your code here
         Case XM_GLOBALSTATUS
            Call CopyMemory(xGlobal, lParam, Len(xGlobal))

            'Add your code here
         Case XM_INSERTDISK
            Call CopyMemory(xInsert, lParam, Len(xInsert))

            'Add your code here

            'Add the following line if you modified the xInsert parameter
            'Call CopyMemory(ByVal lParam, xInsert, Len(xInsert))
         Case XM_INVALIDPASSWORD
            Call CopyMemory(xPassword, lParam, Len(xPassword))

            'Add your code here

            'Add the following line if you modified the xPassword parameter
            'Call CopyMemory(ByVal lParam, xPassword, Len(xPassword))
         Case XM_LISTINGFILE
            Call CopyMemory(xListing, lParam, Len(xListing))

            'Add your code here
         Case XM_PREVIEWINGFILE
            Call CopyMemory(xPreview, lParam, Len(xPreview))

            'Add your code here
         Case XM_PROCESSCOMPLETED
            Call CopyMemory(xCompleted, lParam, Len(xCompleted))

            'Add your code here
         Case XM_QUERYMEMORYFILE
            Call CopyMemory(xQuery, lParam, Len(xQuery))

            'Add your code here

            'Add the following line if you modified the xQuery parameter
            'Call CopyMemory(ByVal lParam, xQuery, Len(xQuery))
         Case XM_REMOVINGFILE
            Call CopyMemory(xRemove, lParam, Len(xRemove))

            'Add your code here
         Case XM_REPLACINGFILE
            Call CopyMemory(xReplace, lParam, Len(xReplace))

            'Add your code here

            'Add the following line if you modified the xReplace parameter
            'Call CopyMemory(ByVal lParam, xReplace, Len(xReplace))
         Case XM_SKIPPINGFILE
            Call CopyMemory(xSkip, lParam, Len(xSkip))

            'Add your code here
         Case XM_TESTINGFILE
            Call CopyMemory(xTest, lParam, Len(xTest))

            'Add your code here
         Case XM_UNZIPPINGMEMORYFILE
            Call CopyMemory(xUnzipMem, lParam, Len(xUnzipMem))

            'Add your code here
         Case XM_UNZIPPREPROCESSINGFILE
            Call CopyMemory(xUnzipPre, lParam, Len(xUnzipPre))

            'Add your code here

            'Add the following line if you modified the xUnzipPre parameter
            'Call CopyMemory(ByVal lParam, xUnzipPre, Len(xUnzipPre))
         Case XM_WARNING
            Call CopyMemory(xWarning, lParam, Len(xWarning))

            'Add your code here
         Case XM_ZIPCOMMENT
            Call CopyMemory(xComment, lParam, Len(xComment))

            'Add your code here

            'Add the following line if you modified the xComment parameter
            'Call CopyMemory(ByVal lParam, xComment, Len(xComment))
         Case XM_ZIPCONTENTSSTATUS
            Call CopyMemory(xContents, lParam, Len(xContents))

            'Add your code here
         Case XM_ZIPPINGMEMORYFILE
            Call CopyMemory(xZipMem, lParam, Len(xZipMem))

            'Add your code here

            'Add the following line if you modified the xZipMem parameter
            'Call CopyMemory(ByVal lParam, xZipMem, Len(xZipMem))
         Case XM_ZIPPREPROCESSINGFILE
            Call CopyMemory(xZipPre, lParam, Len(xZipPre))

            'Add your code here

            'Add the following line if you modified the xZipPre parameter
            'Call CopyMemory(ByVal lParam, xZipPre, Len(xZipPre))
         Case XM_DELETINGFILE
            Call CopyMemory(xDelete, lParam, Len(xDelete))

            'Add your code here

            'Add the following line if you modified the xDelete parameter
            'Call CopyMemory(ByVal lParam, xDelete, Len(xDelete))
         Case XM_CONVERTPREPROCESSINGFILE
            Call CopyMemory(xConvert, lParam, Len(xConvert))

            'Add your code here

            'Add the following line if you modified the xConvert parameter
            'Call CopyMemory(ByVal lParam, xConvert, Len(xConvert))
      End Select
   End Sub
End Module