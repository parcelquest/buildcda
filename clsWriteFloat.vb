Option Strict Off
Option Explicit On
Friend Class clsWriteFloat
	
	Dim m_aBuffer() As Single
   Dim m_zHndl As Integer
	Dim m_zCurrentPos As Integer 'current position in the buffer - ZERO BASED!!
   Const m_zDATA_LENGTH As Integer = 4
	Public lLogicalSeek As Integer
   Public ArrayBufferSize As Integer
	
	Public Sub arrayWrite(ByRef aNew() As Single)
		On Error GoTo lblErr
		
      Dim iX As Integer
      Dim zUbound As Integer
      Dim zNextPos As Integer
		
		zUbound = UBound(aNew)
		zNextPos = m_zCurrentPos + zUbound + 1
		
		If zNextPos > ArrayBufferSize Then
			ReDim Preserve m_aBuffer(m_zCurrentPos - 1)

         FilePut(m_zHndl, m_aBuffer)
         ReDim m_aBuffer(ArrayBufferSize - 1)
         m_zCurrentPos = 0
      End If

      For iX = 0 To zUbound
         m_aBuffer(m_zCurrentPos + iX) = aNew(iX)
      Next

      m_zCurrentPos = m_zCurrentPos + zUbound + 1
      lLogicalSeek = lLogicalSeek + (m_zDATA_LENGTH * (zUbound + 1))

      Exit Sub

lblErr:
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "clsWriteFloat: arrayWrite")
   End Sub

   Public Function termWrite(ByRef sTerm As String, ByRef lRecordNumber As Integer) As Boolean

      On Error GoTo lblErr

      Dim iX As Integer
      Dim zNextPos As Integer

      zNextPos = m_zCurrentPos + 2

      If zNextPos > ArrayBufferSize Then
         ReDim Preserve m_aBuffer(m_zCurrentPos - 1)

         FilePut(m_zHndl, m_aBuffer)
         ReDim m_aBuffer(ArrayBufferSize - 1)
         m_zCurrentPos = 0
      End If

      m_aBuffer(m_zCurrentPos) = CSng(sTerm)
      m_aBuffer(m_zCurrentPos + 1) = lRecordNumber
      m_zCurrentPos = m_zCurrentPos + 2
      termWrite = True

      Exit Function

lblErr:
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "clsWriteFloat : termWrite")
      termWrite = False
   End Function

   Public Sub longWrite(ByRef lTerm As Single)
      On Error GoTo lblErr

      If m_zCurrentPos + 1 > ArrayBufferSize Then

         FilePut(m_zHndl, m_aBuffer)
         m_zCurrentPos = 0
      End If

      m_aBuffer(m_zCurrentPos) = lTerm
      m_zCurrentPos = m_zCurrentPos + 1
      lLogicalSeek = lLogicalSeek + m_zDATA_LENGTH
      Exit Sub

lblErr:
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "clsWriteFloat : longWrite")
   End Sub

   Public Function OpenFile(ByRef szFileName As String) As Boolean

      On Error GoTo lblErr


      If Dir(szFileName) <> "" Then
         Kill((szFileName))
      End If

      'open the file
      m_zHndl = FreeFile
      FileOpen(m_zHndl, szFileName, OpenMode.Binary)

      'empty the buffer
      ReDim m_aBuffer(ArrayBufferSize - 1)
      m_zCurrentPos = 0

      lLogicalSeek = Seek(m_zHndl)

      OpenFile = True
      Exit Function

lblErr:
      MsgBox(Err.Description, MsgBoxStyle.Exclamation, "clsWriteString : OpenFile")
   End Function

   'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Private Sub Class_Initialize_Renamed()
      ArrayBufferSize = OUT_BUFFER_SIZE_LONG_EXTRACTION
   End Sub
   Public Sub New()
      MyBase.New()
      Class_Initialize_Renamed()
   End Sub


   'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Private Sub Class_Terminate_Renamed()
      If m_zCurrentPos > 0 Then
         ReDim Preserve m_aBuffer(m_zCurrentPos - 1)

         FilePut(m_zHndl, m_aBuffer)
      End If
      FileClose(m_zHndl)
   End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class