Option Strict Off
Option Explicit On
Friend Class clsReadLongBuffered

   Dim m_lCurrentPos As Integer 'current position in the buffer - ZERO BASED!!
   Dim m_aBuffer() As Integer
   Dim m_zHndl As Integer

   Const m_zDATA_LENGTH As Integer = 4
   Dim m_zReturnArrayLength As Integer
   Dim m_aReturnArray() As Integer

   Public BufferSize As Integer
   Public lLogicalSeek As Integer
   Public lLOF As Integer

   Public Sub GotoPos(ByRef lPos As Integer)
      ReDim m_aBuffer(BufferSize - 1)
      m_lCurrentPos = BufferSize

      Seek(m_zHndl, lPos)
      'No need to fill buffer since calling function only need to position it
      'Call FillBuffer
   End Sub

   Public Function ArrayGet() As Object
      On Error GoTo lblErr

      Dim zNewPos As Integer
      Dim iX As Integer

      zNewPos = m_lCurrentPos + m_zReturnArrayLength
      If zNewPos > UBound(m_aBuffer) + 1 Then
         If Not FillBuffer Then

            'UPGRADE_WARNING: Couldn't resolve default property of object ArrayGet. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            ArrayGet = System.DBNull.Value
            Exit Function
         End If
         zNewPos = m_lCurrentPos + m_zReturnArrayLength
      End If

      If zNewPos <= UBound(m_aBuffer) + 1 Then 'not minus one as the sign is reversed ( equivalent to <=)
         For iX = 0 To m_zReturnArrayLength - 1
            m_aReturnArray(iX) = m_aBuffer(m_lCurrentPos + iX)
         Next

         m_lCurrentPos = zNewPos
         lLogicalSeek = lLogicalSeek + ((m_zReturnArrayLength) * m_zDATA_LENGTH)
         'ArrayGet = VB6.CopyArray(m_aReturnArray)
         ArrayGet = m_aReturnArray
      Else
         ArrayGet = System.DBNull.Value
      End If

      Exit Function

lblErr:
      LogMsg("clsReadLongBuffered!ArrayGet" & Err.Description)
   End Function

   Public Function LongGet(ByRef aLongArray() As Integer) As Boolean
      On Error GoTo lblErr

      Dim zNewPos As Integer
      Dim iX As Integer

      zNewPos = m_lCurrentPos + m_zReturnArrayLength
      If zNewPos > UBound(m_aBuffer) + 1 Then
         If Not FillBuffer Then
            LongGet = False
            Exit Function
         End If
         zNewPos = m_lCurrentPos + m_zReturnArrayLength
      End If

      If zNewPos <= UBound(m_aBuffer) + 1 Then 'not minus one as the sign is reversed ( equivalent to <=)
         For iX = 0 To m_zReturnArrayLength - 1
            aLongArray(iX) = m_aBuffer(m_lCurrentPos + iX)
         Next

         m_lCurrentPos = zNewPos
         lLogicalSeek = lLogicalSeek + ((m_zReturnArrayLength) * m_zDATA_LENGTH)
         LongGet = True
      Else
         LongGet = False
      End If

      Exit Function

lblErr:
      LogMsg("clsReadLongBuffered!LongGet" & Err.Description)
      '    MsgBox Err.Description, , "LongGet"
   End Function

   Private Function FillBuffer() As Boolean
      On Error GoTo lblErr

      Dim lCurrentSeek As Integer
      Dim lSeekOffset As Integer
      Dim zNewDim As Integer

      'get the number of characters not yet accessed in the buffer
      lSeekOffset = ((UBound(m_aBuffer) + 1) - m_lCurrentPos) * m_zDATA_LENGTH
      'set the corrected position in the file
      lCurrentSeek = Seek(m_zHndl) - lSeekOffset
      'if this position is not passed the end of the file.....
      If lCurrentSeek <= lLOF Then
         If lCurrentSeek - 1 + ((UBound(m_aBuffer) + 1) * m_zDATA_LENGTH) > lLOF Then
            zNewDim = (CInt(lLOF - (lCurrentSeek - 1)) / 4) - 1
            If zNewDim > 0 Then
               ReDim m_aBuffer(zNewDim)
            Else
               FillBuffer = False
            End If
         End If

         If UBound(m_aBuffer) > 0 Then
            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
            FileGet(m_zHndl, m_aBuffer, lCurrentSeek)
            lLogicalSeek = lCurrentSeek
            m_lCurrentPos = 0
            FillBuffer = True
         Else
            FillBuffer = False
         End If
      Else
         FillBuffer = False
      End If

      Exit Function

lblErr:
      LogMsg("clsReadLongBuffered!FillBuffer" & Err.Description)
      '    MsgBox Err.Description, , "clsReadLongBuffered::FillBuffer"
   End Function

   Public WriteOnly Property ReturnArrayLength() As Integer
      Set(ByVal Value As Integer)
         m_zReturnArrayLength = Value
      End Set
   End Property

   Public Function OpenFile(ByRef szFileName As String) As Boolean
      On Error GoTo lblErr

      m_zHndl = FreeFile
      FileOpen(m_zHndl, szFileName, OpenMode.Binary, OpenAccess.Read, OpenShare.Shared)
      ReDim m_aReturnArray(m_zReturnArrayLength - 1)
      ReDim m_aBuffer(BufferSize - 1)
      m_lCurrentPos = BufferSize
      lLOF = LOF(m_zHndl)
      If lLOF < 1 Then
         OpenFile = False
      Else
         OpenFile = FillBuffer
      End If
      Exit Function

lblErr:
      LogMsg("clsReadLongBuffered!OpenFile" & Err.Description)
      '   MsgBox "clsReadLongBuffered::OpenFile", vbExclamation, Err.Description
      OpenFile = False
   End Function

   'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Private Sub Class_Initialize_Renamed()
      BufferSize = IN_ARRAY_BUFFER_SIZE
   End Sub

   Public Sub New()
      MyBase.New()
      Class_Initialize_Renamed()
   End Sub

   'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Public Sub CloseFile()
      If m_zHndl > 0 Then
         FileClose(m_zHndl)
         m_zHndl = 0
      End If
   End Sub

   Protected Overrides Sub Finalize()
      CloseFile()
      MyBase.Finalize()
   End Sub
End Class