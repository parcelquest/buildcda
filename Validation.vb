Option Strict Off
Option Explicit On
Module Validation

   'This module contains function to do the validation of the build making sure that all
   'required files are there.

   'Return number of files that match the spec
   Public Function countFiles(ByRef sFileSpec As String) As Integer
      Dim iNumFiles As Integer
      Dim sTmp As String

      sTmp = Dir(sFileSpec)

      Do While sTmp <> ""
         iNumFiles = iNumFiles + 1
         sTmp = Dir()
      Loop
      countFiles = iNumFiles
   End Function

   Public Function validateCounty(ByRef sPrefix As String) As Boolean
      Dim sTmp As String
      Dim iRet As Integer

      On Error GoTo Validate_Fail

      LogMsg("Start validating number of files.  The number should be at least: " & g_iSearchFields)

      'Check number of TRM, IND, RCD files
      sTmp = g_sWorkDir & sPrefix & "*.TRM"
      iRet = countFiles(sTmp)
      LogMsg("Validate number of TRM files: " & iRet)
      If iRet < g_iSearchFields Then GoTo Validate_Fail

      sTmp = g_sWorkDir & sPrefix & "*.IND"
      iRet = countFiles(sTmp)
      LogMsg("Validate number of IND files: " & iRet)
      If iRet < g_iSearchFields Then GoTo Validate_Fail

      sTmp = g_sWorkDir & sPrefix & "*.RCD"
      iRet = countFiles(sTmp)
      LogMsg("Validate number of RCD files: " & iRet)
      If iRet < g_iSearchFields Then GoTo Validate_Fail

      'check for .cmp, .mst files
      sTmp = g_sWorkDir & sPrefix & ".CMP"
      If Dir(sTmp) = "" Then GoTo Validate_Fail
      LogMsg("Validate CMP file")

      sTmp = g_sWorkDir & sPrefix & ".mst"
      If Dir(sTmp) = "" Then GoTo Validate_Fail
      LogMsg("Validate MST file")

      validateCounty = True
      Exit Function
Validate_Fail:
      LogMsg("Fail validating: " & sTmp)
      LogMsg("iRet = " & iRet)
      validateCounty = False
   End Function
End Module