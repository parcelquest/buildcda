Option Strict Off
Option Explicit On
Friend Class clsFileAction
   Const C_MAP As String = "Map"

   'Update a record in DPF file
   Public Function SaveOneRec(ByRef iNdx As Integer) As Boolean
      Dim objINI As New clsINIWrapper
      'Dim iRecSize, iX, iRet As Integer
      Dim strItem, strData As String

      On Error GoTo SaveOneRec_Err
      If iNdx < 0 Or iNdx > g_iFields Then Exit Function

      'ID,Tag,Type,Offset,Length,Indexed,Sorted,ArrayLen;Terms,UTerms,Group,
      'Index,Display,Termfile,Indexfile,Recordfile,Wordfile,MemberOf.
      'ID0=0;01APN(S);0;1;8;0;-1;1;0;835092;0;-1;-1;ORG000.TRM;ORG000.IND;ORG000.RCD;;
      strData = iNdx & ";" & g_aFields(iNdx).Tag
      strData = strData & ";" & g_aFields(iNdx).Type
      strData = strData & ";" & g_aFields(iNdx).FieldOffSet
      strData = strData & ";" & g_aFields(iNdx).FieldLength
      If g_aFields(iNdx).Indexed = True Then
         strData = strData & ";-1"
      Else
         strData = strData & ";0"
      End If
      If g_aFields(iNdx).Sorted = True Then
         strData = strData & ";-1"
      Else
         strData = strData & ";0"
      End If
      strData = strData & ";" & g_aFields(iNdx).RecordArrayLength
      strData = strData & ";" & g_aFields(iNdx).Terms
      strData = strData & ";" & g_aFields(iNdx).UniqueTerms
      If g_aFields(iNdx).Group = True Then
         strData = strData & ";-1"
      Else
         strData = strData & ";0"
      End If
      If g_aFields(iNdx).Index = True Then
         strData = strData & ";-1"
      Else
         strData = strData & ";0"
      End If
      If g_aFields(iNdx).Display = True Then
         strData = strData & ";-1"
      Else
         strData = strData & ";0"
      End If

      strData = strData & ";" & g_aFields(iNdx).TermFile
      strData = strData & ";" & g_aFields(iNdx).IndexFile
      strData = strData & ";" & g_aFields(iNdx).RecordFile
      strData = strData & ";" & g_aFields(iNdx).WordFile
      strData = strData & ";" & g_aFields(iNdx).MemberOfGroup

      strItem = "ID" & iNdx
      Call objINI.valueWrite("RecordDef", strItem, strData, g_sProjectFile)

      SaveOneRec = True
      Exit Function
SaveOneRec_Err:
      SaveOneRec = False
   End Function

   Public Sub SaveRecordDefs()
      Dim objINI As New clsINIWrapper
      Dim iRecSize, iX, iRet As Integer

      If g_iFields <= 0 Then Exit Sub

      iRecSize = 0
      'Initialize new list
      'CddNewFldList(g_iFields, g_sProjectFile)
      For iX = 0 To g_iFields - 1
         'iRet = DpfUpdate(iX)
         iRet = SaveOneRec(iX)
         iRecSize = iRecSize + g_aFields(iX).FieldLength
      Next
      'CddUpdate()

      If iRecSize <> g_iRecLen Then
         LogMsg("Record length " & g_iRecLen & " is not equal to total field length " & iRecSize)
      End If
   End Sub

   Public Sub SaveSettings()
      Dim objINI As New clsINIWrapper
      Call objINI.valueWrite("Data", "Prefix", g_sPrefix, g_sProjectFile)
      Call objINI.valueWrite("Data", "RecordNumber", CStr(g_iFields), g_sProjectFile)
      Call objINI.valueWrite("Data", "RecordLength", CStr(g_iRecLen), g_sProjectFile)
      Call objINI.valueWrite("Data", "MasterRecordFile", g_sMasterRecordFile, g_sProjectFile)
      Call objINI.valueWrite("Data", "Records", CStr(g_lRecords), g_sProjectFile)
   End Sub

   Public Sub ReadSettings()
      Dim objINI As New clsINIWrapper
      Dim sTemp As String = ""

      Call objINI.valueRead(g_sPrefix, "Cda_Path", sTemp, g_sProjectINI)

      If Right(sTemp, 1) <> "\" Then
         g_sWorkDir = sTemp & "\"
      Else
         g_sWorkDir = sTemp
      End If

      Call objINI.valueRead(g_sPrefix, "Raw_File", sTemp, g_sProjectINI)
      g_sRawFile = sTemp
      Call objINI.valueRead(g_sPrefix, "NumFiles", sTemp, g_sProjectINI)
      If sTemp = "" Then
         g_iRawFile = 1
      Else
         g_iRawFile = Val(sTemp)
      End If

      g_sProjectFile = g_sWorkDir & g_sPrefix & ".dpf"
      g_sMasterRecordFile = g_sPrefix & ".mst"
   End Sub

   Public Sub ClearAll()
      g_sPrefix = ""
      g_sWorkDir = ""
      g_sMasterRecordFile = ""
      g_sRawFile = ""
      g_iRawFile = 1
      g_lRecords = 1

      ReDim g_aFields(0)
      g_iFields = 1
      g_iRecLen = 1
      g_iDisplayFields = 0
      g_iSearchFields = 0
      g_iExtractFields = 0
      ReDim g_aDisplayFields(g_iDisplayFields)
      ReDim g_aSearchFields(g_iSearchFields)
      ReDim g_aExtractFields(g_iExtractFields)

   End Sub

   Public Sub saveAll()
      SaveSettings()
      SaveRecordDefs()
   End Sub

   Public Function readAll() As Integer
      Dim iRet As Integer
      Dim Dpf As New clsDpf

      'ReadRecordDefs
      iRet = Dpf.LoadDpf(g_sProjectFile)
      'iRet = DpfParse()
      If iRet < 0 Then
         LogMsg("Bad record definition file")
      Else
         makeFieldArrays()
      End If

      Dpf = Nothing
      readAll = iRet
   End Function

   Public Sub readVirtualSearchMask(ByRef SearchForm As System.Windows.Forms.Form, ByRef sFormName As String, ByRef sFileName As String)
   End Sub

   Public Sub saveVirtualSearchMask(ByRef SearchForm As System.Windows.Forms.Form, ByRef sFormName As String, ByRef sFileName As String)
   End Sub

   Public Sub readVirtualEditSearchMask(ByRef SearchForm As System.Windows.Forms.Form, ByRef sFormName As String, ByRef sFileName As String)
   End Sub

   Public Sub saveListView(ByRef frmList As System.Windows.Forms.Form, ByRef sFileName As String)
   End Sub

   Public Sub readListView(ByRef frmList As System.Windows.Forms.Form, ByRef sFileName As String)
   End Sub

   Public Sub saveDefaultForm(ByRef sFileName As String, ByRef sFormType As String)
   End Sub

   Public Sub setCompression(ByRef bSetting As Boolean)
      Dim objINI As New clsINIWrapper

      Call objINI.valueWrite("System", "Compression", CStr(bSetting), g_sProjectFile)
   End Sub

   Public Function getGroups(ByRef sGroups() As String) As Boolean
      On Error GoTo lblErr

      Dim sTemp As String = ""
      Dim iX As Integer
      Dim iTemp As Integer

      Dim objINI As New clsINIWrapper

      Call objINI.valueRead("Groups", "Number", sTemp, g_sProjectFile)

      If Not IsNumeric(sTemp) Then
         getGroups = False
         Exit Function
      End If

      iTemp = CInt(sTemp)
      ReDim sGroups(iTemp)

      For iX = 0 To iTemp - 1
         Call objINI.valueRead("Group" & iX, "Number", sTemp, g_sProjectFile)
         sGroups(iX) = sTemp
         getGroups = CBool(sTemp)
      Next

      Exit Function

lblErr:
      LogMsg("clsFileAction!getGroups" & Err.Description)
   End Function

   Public Sub ParsePos(ByRef sInBuf As String, ByRef lFldNum As Integer, ByRef lTop As Integer, ByRef lLeft As Integer, ByRef lWidth As Integer, ByRef lHeight As Integer)
      Dim sTmp As String
      Dim sBuf As String
      Dim lTmp As Integer

      'Look for top value
      lTmp = InStr(1, sInBuf, ":", CompareMethod.Text) + 1
      If lTmp > 1 Then
         lFldNum = Val(sInBuf)
         sBuf = Mid(sInBuf, lTmp)
      Else
         sBuf = sInBuf
      End If

      'Look for left value
      lTmp = InStr(1, sBuf, ",", CompareMethod.Text) + 1
      lTop = Val(sBuf)
      If lTmp <= 1 Then GoTo ParsePos_Err
      sTmp = Mid(sBuf, lTmp)
      lLeft = Val(sTmp)

      'Look for width value
      lTmp = InStr(lTmp, sBuf, ",", CompareMethod.Text) + 1
      If lTmp <= 1 Then GoTo ParsePos_Err
      sTmp = Mid(sBuf, lTmp)
      lWidth = Val(sTmp)

      'Look for height value
      lTmp = InStr(lTmp, sBuf, ",", CompareMethod.Text) + 1
      If lTmp <= 1 Then
         lHeight = DefaultFieldHeight
      Else
         sTmp = Mid(sBuf, lTmp)
         lHeight = Val(sTmp)
      End If
      GoTo ParsePos_Exit

ParsePos_Err:
      lTop = 1
      lLeft = 1
      lWidth = 1
      lHeight = DefaultFieldHeight
ParsePos_Exit:
   End Sub

   Public Sub ParseFont(ByRef sBuf As String, ByRef sFont As String, ByRef nSize As Decimal, ByRef lForeColor As Integer, ByRef lBackColor As Integer, ByRef lStyle As Integer)
      On Error GoTo ParseFont_Err
      Dim sTmp As String
      Dim lTmp As Integer

      'Parse font information
      'Font=MS Sans Serif,8.25,-2147483630,12632256,0
      'Look for font size
      lTmp = InStr(1, sBuf, ",", CompareMethod.Text) + 1
      sFont = Left(sBuf, lTmp - 2)
      If lTmp <= 1 Then GoTo ParseFont_Err

      'Get font size
      sTmp = Mid(sBuf, lTmp)
      nSize = Val(sTmp)
      'Look for forecolor
      lTmp = InStr(lTmp, sBuf, ",", CompareMethod.Text) + 1
      If lTmp <= 1 Then GoTo ParseFont_Err
      sTmp = Mid(sBuf, lTmp)
      lForeColor = Val(sTmp)

      'Look for backcolor
      lTmp = InStr(lTmp, sBuf, ",", CompareMethod.Text) + 1
      If lTmp <= 1 Then GoTo ParseFont_Err
      sTmp = Mid(sBuf, lTmp)
      lBackColor = Val(sTmp)

      'Look for font style
      lTmp = InStr(lTmp, sBuf, ",", CompareMethod.Text) + 1
      If lTmp <= 1 Then GoTo ParseFont_Err
      sTmp = Mid(sBuf, lTmp)
      lStyle = Val(sTmp)

      Exit Sub

ParseFont_Err:
   End Sub

End Class