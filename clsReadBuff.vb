Option Strict Off
Option Explicit On
Friend Class clsReadBuffered

   Dim m_lCurrentPos As Integer 'current position in the buffer - ZERO BASED!!
   Dim m_sBuffer As String
   Dim m_zHndl As Integer

   Dim m_zSepLength As Integer
   Dim m_sStringSeparator As String
   Public BufferSize As Integer
   Public lLogicalSeek As Integer
   Public lLOF As Integer

   Public Sub GotoPos(ByRef lPos As Integer)
      m_sBuffer = New String(" ", BufferSize)
      m_lCurrentPos = BufferSize

      Seek(m_zHndl, lPos)
   End Sub

   Public Function OpenFile(ByRef szFileName As String) As Boolean
      Try
         m_zHndl = FreeFile()
         FileOpen(m_zHndl, szFileName, OpenMode.Binary, OpenAccess.Read, OpenShare.Shared)
         m_sBuffer = New String(" ", BufferSize)
         m_lCurrentPos = BufferSize
         lLOF = LOF(m_zHndl)
         If lLOF < 1 Then
            OpenFile = False
         Else
            OpenFile = FillBuffer()
         End If
      Catch ex As Exception
         LogMsg("OpenFile: " & szFileName & ":" & Err.Description)
         OpenFile = False
      End Try
   End Function

   Public Function stringGet() As String
      On Error GoTo lblErr
      Dim sTempString As String = ""
      Dim zPos As Integer

      zPos = InStr(m_lCurrentPos + 1, m_sBuffer, m_sStringSeparator)
      If zPos = 0 Then
         If Not FillBuffer() Then
            stringGet = ""
            lLogicalSeek = lLOF + 2
            Exit Function
         End If
         zPos = InStr(m_lCurrentPos + 1, m_sBuffer, m_sStringSeparator)
      End If

      If zPos <> 0 Then
         sTempString = Mid(m_sBuffer, m_lCurrentPos + 1, zPos - (m_lCurrentPos + 1))
         m_lCurrentPos = zPos + m_zSepLength - 1
         lLogicalSeek = lLogicalSeek + Len(sTempString) + m_zSepLength
      Else
         If m_sBuffer <> "" Then
            sTempString = Mid(m_sBuffer, m_lCurrentPos + 1, Len(m_sBuffer) - (m_lCurrentPos))
         End If
         m_lCurrentPos = Len(m_sBuffer)
         lLogicalSeek = lLogicalSeek + Len(m_sBuffer)
      End If
      stringGet = sTempString
      Exit Function

lblErr:
      LogMsg("clsReadBuffered!StringGet: " & Err.Description)
   End Function

   Private Function FillBuffer() As Boolean
      Dim lCurrentSeek As Integer
      Dim lSeekOffset As Integer

      FillBuffer = False
      Try
         'get the number of characters not yet accessed in the buffer
         lSeekOffset = (Len(m_sBuffer) - (m_lCurrentPos))
         'set the corrected position in the file
         lCurrentSeek = Seek(m_zHndl) - lSeekOffset
         'if this position is not passed the end of the file.....
         If lCurrentSeek <= lLOF Then
            If lCurrentSeek - 1 + Len(m_sBuffer) > lLOF Then
               m_sBuffer = New String(" ", lLOF - (lCurrentSeek - 1))
            End If

            If Len(m_sBuffer) > 0 Then
               FileGet(m_zHndl, m_sBuffer, lCurrentSeek)
               lLogicalSeek = lCurrentSeek
               m_lCurrentPos = 0
               FillBuffer = True
            End If
         End If
      Catch ex As Exception
         LogMsg("clsReadBuffered!FillBuffer: " & Err.Description)
      End Try
   End Function

   Public Sub New()
      MyBase.New()
      m_sStringSeparator = sINDEX_SEPARATOR
      m_zSepLength = Len(m_sStringSeparator)
      BufferSize = IN_BUFFER_SIZE
      m_zHndl = 0
   End Sub

   Public WriteOnly Property StringSeparator() As String
      Set(ByVal Value As String)
         m_sStringSeparator = Value
         m_zSepLength = Len(m_sStringSeparator)
      End Set
   End Property

   Public Sub CloseFile()
      If m_zHndl > 0 Then
         FileClose(m_zHndl)
         m_zHndl = 0
      End If
   End Sub

   Protected Overrides Sub Finalize()
      CloseFile()
      MyBase.Finalize()
   End Sub
End Class