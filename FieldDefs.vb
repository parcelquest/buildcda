Option Strict Off
Option Explicit On
Module modFieldDefs

   Public Sub fieldAdd(ByRef fldNew As Field)

      Dim zUbound As Integer

      zUbound = g_iFields

      'Increase number of fields in array
      g_iFields = g_iFields + 1

      If zUbound = 0 And g_aFields(0).FieldLength = 0 Then
         g_aFields(zUbound) = fldNew
      Else
         ReDim Preserve g_aFields(g_iFields)
         g_aFields(zUbound) = fldNew
      End If

   End Sub

   Public Sub makeFieldArrays()
      On Error GoTo lblErr
      Dim iX As Integer
      Dim iDispNdx As Integer
      Dim iSrchNdx As Integer
      Dim iExtNdx As Integer

      ReDim g_aDisplayFields(g_iDisplayFields)
      ReDim g_aSearchFields(g_iSearchFields)
      ReDim g_aExtractFields(g_iExtractFields)

      iSrchNdx = 0
      iDispNdx = 0
      iExtNdx = 0
      For iX = 0 To g_iFields - 1
         If g_aFields(iX).Display = True Then
            g_aDisplayFields(iDispNdx) = iX
            iDispNdx = iDispNdx + 1
         End If

         'If a field that already belongs to a group, don't index it
         'If g_aFields(iX).MemberOfGroup = "" And g_aFields(iX).Index = True Then
         If g_aFields(iX).Index = True Then
            g_aSearchFields(iSrchNdx) = iX
            iSrchNdx = iSrchNdx + 1
            If g_aFields(iX).Group = False Then
               g_aExtractFields(iExtNdx) = iX
               iExtNdx = iExtNdx + 1
            End If
         ElseIf g_aFields(iX).MemberOfGroup <> "" Then
            g_aExtractFields(iExtNdx) = iX
            iExtNdx = iExtNdx + 1
         End If

      Next
      Exit Sub
lblErr:
      'Something wrong here
      LogMsg("Invalid array size in MakeFieldArrays()")
   End Sub
   'Public Sub makeExtractFieldArrays()
   '   Dim iX As Integer

   '   For iX = 0 To g_iFields - 1
   '      If g_aFields(iX).Group = False And g_aFields(iX).Index = True Then
   '         ReDim Preserve g_aExtractFields(Uboundg_aExtractFields + 1)
   '         g_aExtractFields(Uboundg_aExtractFields) = iX
   '      End If
   '   Next
   'End Sub
End Module