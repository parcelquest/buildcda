Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module QuickSort

   'QuickSort2 - QuickSort iterative (rather than recursive) by Cornel Huth.

   'UPGRADE_NOTE: DefInt A-Z statement was removed. Variables were explicitly declared as type integer. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1005"'

   Structure stacktype 'for QuickSort2
      Dim low As Integer
      Dim hi As Integer
   End Structure

   Sub Fastsorti(ByRef inarray() As Integer, ByRef lower As Integer, ByRef upper As Integer)
      Dim stopnow As Integer

      ' This routine was writen by Ryan Wellman.
      ' Copyright 1992, Ryan Wellman, all rights reserved.
      ' Released as Freeware October 22, 1992.
      ' You may freely use, copy & modify this code as you see
      ' fit. Under the condition that I am given credit for
      ' the original sort routine, and partial credit for modified
      ' versions of the routine.

      ' Thanks to Richard Vannoy who gave me the idea to compare
      ' entries further than 1 entry away.
      Dim Index As Integer
      Dim increment As Integer
      Dim cutpoint As Integer
      Dim i2 As Integer
      Dim l2 As Integer

      increment = (upper + lower)
      l2 = lower - 1

      Do
         increment = increment \ 2
         i2 = increment + l2
         For Index = lower To upper - increment
            If inarray(Index) > inarray(Index + increment) Then
               swap(inarray(Index), inarray(Index + increment))
               If Index > i2 Then
                  cutpoint = Index
                  stopnow = 0
                  Do
                     Index = Index - increment
                     If inarray(Index) > inarray(Index + increment) Then
                        swap(inarray(Index), inarray(Index + increment))
                     Else
                        stopnow = -1
                        Index = cutpoint
                     End If
                  Loop Until stopnow
               End If
            End If
         Next Index
      Loop Until increment <= 1
   End Sub

   Function go(ByRef count As Integer) As String
      Dim e2 As Double
      Dim Start As Double
      Dim s2 As Double

      Dim szResult As String
      Dim b As Integer
      Dim a As Integer

      'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1033"'
      Dim temp(count) As Integer
      ' Generate a random array to test the sort.


      'Make the original arrays identical (no cheating! ;^)

      Randomize(a)
      For b = 1 To count
         temp(b) = Rnd() * 32766 + 1
      Next b

      s2 = VB.Timer()
      Do
         Start = VB.Timer()
      Loop While s2 = Start

      '  QuickSort2 temp(), 1, count
      e2 = VB.Timer()

      ' A test to make sure it sorted it correctly.
      '
      'FOR chk = 1 TO Count
      '  PRINT Temp(chk);
      'NEXT chk
      'PRINT

      'Debug.Print "FastSort: took"; e1# - s1#;
      'Debug.Print Tab(30); "seconds to sort"; Count; "entries."
      'Debug.Print "MiscSort: took"; e2# - s2#;
      'Debug.Print Tab(30); "seconds to sort"; Count; "entries."
      szResult = "MiscSort: took " & e2 - s2 & " seconds to sort " & count & " entries." & vbCrLf

      go = szResult
   End Function

   Sub QuickSort2(ByRef sortarray() As Integer)
      'QuickSort iterative (rather than recursive) by Cornel Huth
      'UPGRADE_WARNING: Lower bound of array lstack was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1033"'
      Dim lstack(128) As stacktype 'our stack
      Dim sp As Integer 'out stack pointer
      Dim mid_Renamed As Integer
      Dim hi As Integer
      Dim low As Integer
      Dim i As Integer
      Dim j As Integer
      Dim lower As Integer
      Dim upper As Integer
      Dim compare As Integer

      lower = LBound(sortarray)
      upper = UBound(sortarray)

      sp = 1
      'maxsp = sp
      lstack(sp).low = lower
      lstack(sp).hi = upper
      sp = sp + 1
      Do
         sp = sp - 1
         low = lstack(sp).low
         hi = lstack(sp).hi
         Do
            i = low
            j = hi
            mid_Renamed = (low + hi) \ 2
            compare = sortarray(mid_Renamed)
            Do
               Do While sortarray(i) < compare
                  i = i + 1
               Loop
               Do While sortarray(j) > compare
                  j = j - 1
               Loop
               If i <= j Then
                  swap(sortarray(i), sortarray(j))
                  i = i + 1
                  j = j - 1
               End If
            Loop While i <= j
            If j - low < hi - i Then
               If i < hi Then
                  lstack(sp).low = i
                  lstack(sp).hi = hi
                  sp = sp + 1
               End If
               hi = j
            Else
               If low < j Then
                  lstack(sp).low = low
                  lstack(sp).hi = j
                  sp = sp + 1
               End If
               low = i
            End If
         Loop While low < hi
         'IF sp > maxsp THEN maxsp = sp
      Loop While sp <> 1
      'PRINT "MAX SP"; maxsp
   End Sub

   Public Sub swap(ByRef var1 As Object, ByRef var2 As Object)
      Dim temp As Object

      temp = var2
      var2 = var1
      var1 = temp
   End Sub
   Public Sub swapVariantList(ByRef lPos1 As Integer, ByRef lPos2 As Integer, ByRef arrVariant() As Object)

      Dim temp As Object
      Dim iRecordNumber As Integer

      temp = arrVariant(lPos2)
      arrVariant(lPos2) = arrVariant(lPos1)
      arrVariant(lPos1) = temp

      iRecordNumber = g_arrResult(lPos2)
      g_arrResult(lPos2) = g_arrResult(lPos1)
      g_arrResult(lPos1) = iRecordNumber

   End Sub

   Sub QuickSortVariant(ByRef sortarray() As Object)
      'QuickSort iterative (rather than recursive) by Cornel Huth
      'UPGRADE_WARNING: Lower bound of array lstack was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1033"'
      Dim lstack(128) As stacktype 'our stack
      Dim sp As Integer 'out stack pointer
      'UPGRADE_NOTE: mid was upgraded to mid_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
      Dim mid_Renamed As Integer
      Dim hi As Integer
      Dim low As Integer
      Dim i As Integer
      Dim j As Integer
      Dim lower As Integer
      Dim upper As Integer
      Dim compare As Object

      lower = LBound(sortarray)
      upper = UBound(sortarray)

      sp = 1
      'maxsp = sp
      lstack(sp).low = lower
      lstack(sp).hi = upper
      sp = sp + 1
      Do
         sp = sp - 1
         low = lstack(sp).low
         hi = lstack(sp).hi
         Do
            i = low
            j = hi
            mid_Renamed = (low + hi) \ 2
            'UPGRADE_WARNING: Couldn't resolve default property of object sortarray(mid_Renamed). Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            'UPGRADE_WARNING: Couldn't resolve default property of object compare. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            compare = sortarray(mid_Renamed)
            Do
               'UPGRADE_WARNING: Couldn't resolve default property of object compare. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               'UPGRADE_WARNING: Couldn't resolve default property of object sortarray(i). Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               Do While sortarray(i) < compare
                  i = i + 1
               Loop
               'UPGRADE_WARNING: Couldn't resolve default property of object compare. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               'UPGRADE_WARNING: Couldn't resolve default property of object sortarray(j). Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               Do While sortarray(j) > compare
                  j = j - 1
               Loop
               If i <= j Then
                  swapVariantList(i, j, sortarray)
                  i = i + 1
                  j = j - 1
               End If

               'Check for ESC
               System.Windows.Forms.Application.DoEvents()
               If g_bStopProcess Then Exit Sub
            Loop While i <= j
            If j - low < hi - i Then
               If i < hi Then
                  lstack(sp).low = i
                  lstack(sp).hi = hi
                  sp = sp + 1
               End If
               hi = j
            Else
               If low < j Then
                  lstack(sp).low = low
                  lstack(sp).hi = j
                  sp = sp + 1
               End If
               low = i
            End If
         Loop While low < hi
         'IF sp > maxsp THEN maxsp = sp
      Loop While sp <> 1
      'PRINT "MAX SP"; maxsp
   End Sub


   Sub QuickSortVariantDesc(ByRef sortarray() As Object)
      'QuickSort iterative (rather than recursive) by Cornel Huth
      'UPGRADE_WARNING: Lower bound of array lstack was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1033"'
      Dim lstack(128) As stacktype 'our stack
      Dim sp As Integer 'out stack pointer
      'UPGRADE_NOTE: mid was upgraded to mid_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
      Dim mid_Renamed As Integer
      Dim hi As Integer
      Dim low As Integer
      Dim i As Integer
      Dim j As Integer
      Dim lower As Integer
      Dim upper As Integer
      Dim compare As Object

      lower = LBound(sortarray)
      upper = UBound(sortarray)

      sp = 1
      'maxsp = sp
      lstack(sp).low = lower
      lstack(sp).hi = upper
      sp = sp + 1
      Do
         sp = sp - 1
         low = lstack(sp).low
         hi = lstack(sp).hi
         Do
            i = low
            j = hi
            mid_Renamed = (low + hi) \ 2
            'UPGRADE_WARNING: Couldn't resolve default property of object sortarray(mid_Renamed). Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            'UPGRADE_WARNING: Couldn't resolve default property of object compare. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            compare = sortarray(mid_Renamed)
            Do
               'UPGRADE_WARNING: Couldn't resolve default property of object compare. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               'UPGRADE_WARNING: Couldn't resolve default property of object sortarray(i). Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               Do While sortarray(i) > compare
                  i = i + 1
               Loop
               'UPGRADE_WARNING: Couldn't resolve default property of object compare. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               'UPGRADE_WARNING: Couldn't resolve default property of object sortarray(j). Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
               Do While sortarray(j) < compare
                  j = j - 1
               Loop
               If i <= j Then
                  swapVariantList(i, j, sortarray)
                  i = i + 1
                  j = j - 1
               End If

               'Check for ESC
               System.Windows.Forms.Application.DoEvents()
               If g_bStopProcess Then Exit Sub
            Loop While i <= j
            If j - low < hi - i Then
               If i < hi Then

                  lstack(sp).low = i
                  lstack(sp).hi = hi
                  sp = sp + 1
               End If
               hi = j
            Else
               If low < j Then
                  lstack(sp).low = low
                  lstack(sp).hi = j
                  sp = sp + 1
               End If
               low = i
            End If
         Loop While low < hi
         'IF sp > maxsp THEN maxsp = sp
      Loop While sp <> 1
      'PRINT "MAX SP"; maxsp
   End Sub


   Public Function union(ByRef arr1() As Integer, ByRef arr2() As Integer) As Integer

      Dim arrResult() As Integer
      Dim arr1Count As Integer
      Dim arr2Count As Integer
      Dim ResultCount As Integer
      Dim iUboundArr1 As Integer
      Dim iUboundArr2 As Integer
      Dim arr1Current As Integer
      Dim arr2Current As Integer
      Dim iCount As Integer

      On Error GoTo lblErr

      iUboundArr1 = UBound(arr1)
      iUboundArr2 = UBound(arr2)
      ReDim arrResult(iUboundArr1 + iUboundArr2 + 1)

      For arr1Count = 0 To iUboundArr1
         arr1Current = arr1(arr1Count)

         Do Until arr2(arr2Count) > arr1Current
            If arr2(arr2Count) <> arr1Current Then
               arrResult(ResultCount) = arr2(arr2Count)
               ResultCount = ResultCount + 1
            End If

            arr2Count = arr2Count + 1
            If arr2Count > iUboundArr2 Then
               For iCount = arr1Count To iUboundArr1
                  arrResult(ResultCount) = arr1(iCount)
                  ResultCount = ResultCount + 1
               Next
               GoTo lblFinished
            End If
         Loop
         arrResult(ResultCount) = arr1Current
         ResultCount = ResultCount + 1
      Next

      If arr2Count <= iUboundArr2 Then
         For iCount = arr2Count To iUboundArr2
            arrResult(ResultCount) = arr2(iCount)
            ResultCount = ResultCount + 1
         Next
      End If

lblFinished:
      union = ResultCount '- 1

      If ResultCount = 0 Then
         'ResultCount = 1
         ReDim Preserve arrResult(0)
      Else
         ReDim Preserve arrResult(ResultCount - 1)
      End If

      'Call copymemory(arr1(0), arrResult(0), ResultCount * 4)
      Call UTCopyMemory(arr1, arrResult)
      Exit Function

lblErr:
      'MsgBox Err.Description, vbExclamation, "quicksort.bas : union"
      LogMsg("quicksort.bas!union: " & Err.Description)
   End Function


   Public Function less(ByRef arr2() As Integer, ByRef arr1() As Integer) As Integer

      Dim arrResult() As Integer
      Dim arr1Count As Integer
      Dim arr2Count As Integer
      Dim ResultCount As Integer
      Dim iUboundArr1 As Integer
      Dim iUboundArr2 As Integer
      Dim arr1Current As Integer
      Dim arr2Current As Integer
      Dim iCount As Integer

      On Error GoTo lblErr

      iUboundArr1 = UBound(arr1)
      iUboundArr2 = UBound(arr2)
      ReDim arrResult(iUboundArr1 + iUboundArr2 + 1)

      For arr1Count = 0 To iUboundArr1
         arr1Current = arr1(arr1Count)

         Do Until arr2(arr2Count) > arr1Current
            If arr2(arr2Count) <> arr1Current Then
               arrResult(ResultCount) = arr2(arr2Count)
               ResultCount = ResultCount + 1
            End If

            arr2Count = arr2Count + 1
            If arr2Count > iUboundArr2 Then
               GoTo lblFinished
            End If
         Loop
      Next

      If arr2Count <= iUboundArr2 Then
         For iCount = arr2Count To iUboundArr2
            arrResult(ResultCount) = arr2(iCount)
            ResultCount = ResultCount + 1
         Next
      End If

lblFinished:
      less = ResultCount '- 1

      If ResultCount = 0 Then
         'ResultCount = 1
         ReDim Preserve arrResult(0)
      Else
         ReDim Preserve arrResult(ResultCount - 1)
      End If

      'Call copymemory(arr1(0), arrResult(0), ResultCount * 4)
      Call UTCopyMemory(arr2, arrResult)
      'For arr2Count = 0 To UBound(arr1)
      '    If arr2(arr2Count) = 103196 Then MsgBox "kk"
      'Next

      Exit Function

lblErr:
      'MsgBox Err.Description, vbExclamation, "quicksort.bas : less"
      LogMsg("quicksort.bas!less: " & Err.Description)
   End Function

   Public Function intersection(ByRef arr1() As Integer, ByRef arr2() As Integer) As Integer

      Dim arrResult() As Integer
      Dim arr1Count As Integer
      Dim arr2Count As Integer
      Dim ResultCount As Integer
      Dim iUboundArr1 As Integer
      Dim iUboundArr2 As Integer
      Dim arr1Current As Integer
      Dim arr2Current As Integer
      Dim iCount As Integer

      On Error GoTo lblErr

      iUboundArr1 = UBound(arr1)
      iUboundArr2 = UBound(arr2)
      ReDim arrResult(iUboundArr1 + iUboundArr2 + 1)

      For arr1Count = 0 To iUboundArr1
         arr1Current = arr1(arr1Count)

         Do Until arr2(arr2Count) > arr1Current
            If arr2(arr2Count) = arr1Current Then
               arrResult(ResultCount) = arr2(arr2Count)
               ResultCount = ResultCount + 1
            End If

            arr2Count = arr2Count + 1
            If arr2Count > iUboundArr2 Then
               GoTo lblFinished
            End If
         Loop
      Next

lblFinished:
      intersection = ResultCount '- 1

      If ResultCount = 0 Then
         'ResultCount = 1
         ReDim Preserve arrResult(0)
      Else
         ReDim Preserve arrResult(ResultCount - 1)
      End If

      'Call copymemory(arr1(0), arrResult(0), ResultCount * 4)
      Call UTCopyMemory(arr1, arrResult)
      Exit Function

lblErr:
      'MsgBox Err.Description, vbExclamation, "quicksort.bas : union"
      LogMsg("quicksort.bas!intersection: " & Err.Description)
   End Function
End Module